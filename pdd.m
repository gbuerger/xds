function pdd (L = true)
 
   ## usage: pdd (L)
   ## 
   ## process predictands (stations)

   global PAR PRJ MC ids vars names lons lats alts undefs clim v

   ldata = fullfile("data", PRJ, PAR.pdd) ;
   METAFILE = fullfile(ldata, "meta.txt") ;
   ofile = sprintf("%s/%s.ob", ldata, PAR.pdd) ;
   cfile = fullfile(ldata, "clim.ob") ;

   if ~isempty(f = dbstack)
      printf("\n\t###   %s   ###\n\n", f(1).name) ;
   endif

   pkg load miscellaneous statistics

   printf("<-- %s\n", METAFILE) ;
   [ids, vars, names, lons, lats, alts, undefs, J] = meta(METAFILE) ;
   if (~PAR.bld && ~PAR.dbg && isnewer(cfile, ofile, METAFILE) && ~L), return ; endif

   if exist(ofile, "file") == 2
      load(ofile) ;
      switch PAR.output
	 case "csv"
	    mwrite_stat(sprintf("data/%s/%s.org", PRJ, PAR.pdd), pdd) ;
	 case "netcdf"
	    ncwrite_stat(ldata, pdd, PAR.ptr, PAR.ncdir) ;
      endswitch
   endif

   VAR = sunique(vars) ;

   pdd = [] ; ierr = false ;
   for j = 1:length(VAR)

      if isempty(J{j}), continue ; endif 

      v = VAR{j} ;

      [s ierr1] = do_read(J{j}) ;
      ierr = ierr | ierr1 ;
      if ierr1 continue ; endif
      
      s = fill_date(s) ;	#  fix broken observations
      s = normalize(s) ;

      pdd = join(pdd, s) ;

      ## output mean and quantiles
      if PAR.dbg
	 s.x = s.x(~isnan(s.x)) ;
	 if 0
	    source ~/oct/oc/ssn.m
	    I = ssn(s.id, 1:8) ;
	    s.x = s.x(I,:) ;
	 endif
	 xq = quantile(s.x, [0 0.1 0.5 0.9 1]) ;
	 xm = mean(s.x) ;
	 fid = fopen(fullfile(ldata, [v, ".stat.csv"]), "at") ;
	 fprintf(fid, ["M:\t%7.1f | Q:\t%7.1f\t%7.1f\t%7.1f\t%7.1f\t%7.1f\n"], xm, xq) ;
	 fclose(fid) ;
	 printf("out: --> %s\n", fullfile(ldata, [v, ".stat.csv"])) ;
      endif

   endfor

   if ierr
      fid = fopen("/tmp/meta.txt", "wt") ;
      for j=1:length(MC{1})
	 fprintf(fid, "%s,%s,%s,%s,%s,%s,%s\n", MC{1}{j}, MC{2}{j}, MC{3}{j}, MC{4}{j}, MC{5}{j}, MC{6}{j}, MC{7}{j}) ;
      endfor
      fclose(fid) ;
      error("xds:xds", "pdd> consider command:\ncp /tmp/meta.txt %s", fullfile(ldata, "meta.txt")) ;
   endif
   
   if isempty(pdd), error("xds:xds", "pdd> no data") ; endif
   
   [pdd.nr pdd.nc] = size(pdd.x) ;
   for k = {"vars" "ids" "names" "lons" "lats" "alts" "isP" "ispos" "udf"}
      clim.(k{:}) = pdd.(k{:}) ;
   endfor

   pdd.cov = do_cov(pdd) ;
   pdd.cal = "gregorian" ;

   save(ofile, "pdd") ;
   printf("pdd: --> %s\n", ofile) ;

   if strcmp(PAR.output, "netcdf") ncwrite_stat(ldata, pdd) ; endif

   save(cfile, "clim") ;
   printf("pdd: --> %s\n", cfile) ;

endfunction


function [s ierr] = do_read (J)

   ## usage: [s ierr] = do_read (J)
   ## 
   ## 

   global ids vars names lons lats alts undefs
   global PAR PRJ MC DRATE

   ldata = fullfile("data", PRJ, PAR.pdd) ;

   v = vars{J(1)} ; vn = tolower(names(J)) ;
   pfx = fullfile(ldata, v) ;
   ifile = [pfx ".csv"] ;
   
   if isnewer(ofile = [pfx ".ob"], ifile, fullfile(ldata, "meta.txt"))

      printf("pdd: <-- %s\n", ofile) ;
      load(ofile) ;

   else
      
      printf("pdd: <-- %s\n", ifile) ;
      s = read_stat(ifile) ;

      s = selper(s, PAR.cper(1,:), PAR.cper(2,:)) ;
      
      [~, ia, ib] = setxor(ids(J), s.ids) ;
      if (n = length(ia)) > 0
	 fmt = repmat(" %s", 1, n) ;
	 [~, f2, f3] = fileparts(ifile) ;
	 warning(["not in " f2 f3 ":\t" fmt "\n"], ids(J){ia}) ;
      endif
      if (n = length(ib)) > 0
	 fmt = repmat(" %s", 1, n) ;
	 warning(["not in meta:\t" fmt "\n"], s.ids{ib}) ;
      endif
      printf("pdd: --> %s\n", ofile) ;
      save(ofile, "s") ;
      
   endif
   
   ## select ids from meta
   [Jm Js Km Ks] = unify(ids(J)', s.ids) ;
   
   isP = any(cellfun(@(x) !isempty(x), strfind(vn(Jm), "precip"))) ;
   ispos = isP | any(cellfun(@(x) !isempty(x), regexp(vn(Jm), PAR.ispos))) ;
   s.isP = repmat(isP, 1, length(Ks)) ;
   s.ispos = repmat(ispos, 1, length(Ks)) ;

   s.udf = undefs(J)(Jm)' ;
   s.ids = s.ids(Ks) ;
   s.x = s.x(:,Ks) ; s.x = lim2nan(s.x, s.udf) ;

   s.vars = vars(J)(Jm)' ;
   s.names = names(J)(Jm)' ;
   s.lons = lons(J)(Jm)' ;
   s.lats = lats(J)(Jm)' ;
   s.alts = alts(J)(Jm)' ;

   ## check data availability and select accordingly   
   I = sdate(s.id, PAR.cper(1,:), PAR.cper(2,:)) ;
   Idfd = !isnan(s.x(I,:)) ;
   switch PAR.pddsel
      case "yearwise"
	 if any(~s.isP)
	    Jdfd = find_ctg(Idfd, PAR.qctg) ;
	 else
	    Jx = false(size(s.x)) ;
	    for y = PAR.cper(1,1):PAR.cper(2,1)
	       Iy = s.id(:,1) == y ;
	       Jx(Iy,:) = repmat(any(~isnan(s.x(Iy,:)), 1), sum(Iy), 1) ;
	    endfor
	    s.x(Jx & isnan(s.x)) = 0 ;
	    Idfd = !isnan(s.x(I,:)) ;
	    Jdfd = sum(Idfd)' / rows(Idfd) > PAR.npdd ;
	    if ~all(Jdfd)
	       s.vars = cellfun(@(c) strcat(c, "z"), s.vars, "UniformOutput", false) ;
	       MC{2}(1+J(Jm)) = s.vars ;
	       write_stat(ldata, s) ;
	    endif
	 endif
      case "single"
	 Jdfd = sum(Idfd)' / rows(Idfd) > PAR.npdd ;
      case "ctg"
	 ## select most contiguous data portion
	 if any(s.isP)
	    Jdfd = find_ctg(Idfd, PAR.qctg) ;
	 else
	    Jdfd = sum(Idfd)' / rows(Idfd) > PAR.npdd ;
	 endif
   endswitch
   if (ierr = ~all(Jdfd))
      cellfun(@(b, c, d) printf("pdd> %s(%s):\t%4.1f %%\n", b, c, 100*d), s.vars(~Jdfd), s.ids(~Jdfd), num2cell(sum(Idfd)(~Jdfd)/rows(Idfd)), "UniformOutput", false) ;
      JM = J(Jm(~Jdfd)) ;
      MC{1}(1+JM) = cellfun(@(c) ["#" c], MC{1}(1+JM), "UniformOutput", false) ;
      warning("xds:xds", "pdd> %s: %d/%d have too few data", v, sum(~Jdfd), length(Jdfd)) ;
   endif

   datagaps(s, Jdfd) ;

endfunction


function datagaps (s, Jdfd)

   ## usage:  res = datagaps (s, Jdfd)
   ##
   ## check overlap of data

   global BATCH PAR PRJ

   ldata = fullfile("data", PRJ, PAR.pdd) ;

   s.ids = s.ids(Jdfd) ;
   s.x = s.x(:,Jdfd) ;
   
   sw = selper(s, PAR.cper(1,:), PAR.cper(2,:)) ;

   Idfd = !isnan(sw.x) ;
   I = Idfd'* Idfd ;

   if exist(pfile = fullfile(ldata, [s.vars{1} ".dfd.svg"]), "file") ~= 2
      h = disp_dfd(s) ;
      printf("datagaps: --> %s\n", pfile) ;
      print(pfile) ;
      close(h) ;
   endif

   II = (I < PAR.Ccases) ;
   if any(II(:))
      for j = find(all(II))
	 warning("pdd: not enough overlapping calibration data for %s:\t(%d < %d)", s.ids{j}, I(j,j), PAR.Ccases)
      endfor
   endif

endfunction


function v = normalize (u)

   ## usage: v = normalize (u)
   ## 
   ## 

   global PAR clim

   v = u ;

   vn = v.vars{1} ;

   v = pre_pare(v) ;

   vw = selper(v, PAR.xdscal(1,:), PAR.xdscal(2,:)) ;

   if isempty(PAR.anc)
      v.a = v.x ;
      vw.a = vw.x ;
      [vw.z, clim.(vn).sm, clim.(vn).ss] = anom(vw.a) ;
      v.z = anom(v.a, clim.(vn).sm, clim.(vn).ss) ;
   else
      if all(v.ispos)
	 if PAR.dbg, printf(["pdd: " vn " treated as positive.\n"]) ; endif
	 if isnan(PAR.ssnwin)
	    [clim.(vn).ps clim.(vn).pp vw] = anncyclog(vw) ;
	    v = anncyclog(v, clim.(vn).ps, clim.(vn).pp) ;
	    v = rmfield(v, "a") ;
	 else
	    clim.(vn).pp = ssnprb(vw.id, vw.x) ;
	    v.z = ssnprb(v.id, v.x, clim.(vn).pp) ;
	 endif
	 ##v.z = pfy(v.z, [clim.(vn).pp.q]) ;
      else
	 [clim.(vn).ps vw] = anncyc(vw) ;
	 [clim.(vn).pp clim.(vn).ppinv] = prbfit(vw.a) ;
	 v = anncyc(v, clim.(vn).ps) ;
	 if PAR.dbg
	    [v.a I] = parfun(@(j) outlier(v.a(:,j), thd = 4), 1:columns(v.x), "UniformOutput", false) ;
	    if any(cellfun(@(i) ~isempty(i), I))
	       printf("warning: possible outlier %s:\n", vn) ;
	       for j = 1:length(I)
		  if ~any(I{j}) continue ; endif
		  for i = find(I{j})'
		     printf("%4d-%02d-%02d\t%d\t%10.4g\n", v.id(i,:), j, u.x(i,j)) ;
		  endfor
	       endfor
	       v.a = cell2mat(v.a) ;
	    endif
	 endif
	 v.z = prbfit(v.a, clim.(vn).pp) ;
	 v = rmfield(v, "a") ;
      endif 
   endif

   v.x = u.x ;			# restore x

endfunction


function C = do_cov (u)

   ## usage: C = do_cov (u)
   ## 
   ## calculate (seasonal) covariance of u.z

   global PAR

   for is = 1:length(PAR.ssn)

      I = sdate(u.id, PAR.cper(1,:), PAR.cper(2,:)) ;
      Iu = selssn(PAR.ssn{is}, u.id) ;

      if sum(~isnan(u.z(:))) / numel(u.z) < PAR.npdd
	 
	 [~, c] = ecmnmle(u.z(I&Iu,:), "nanskip", 500, 1e-6) ;

	 ##c = nancov(u.z(I&Iu,:)) ;
	 ##if any(isnan(c(:)))
	 ##   warning("xds:xds", "pdd: non-overlapping predictands. Guessing covariance.\n") ;
	 ##   [i j] = find(isnan(c)) ;
	 ##   n = length(i) ;
	 ##   for k=1:n
	 ##      I = 1:n != k ;
	 ##      c(i(k),j(k)) = (mean(c(I,j(k)), 1) + mean(c(i(k),I), 2)) / 2 ;
	 ##   endfor 
	 ##   c = bend(c) ;
	 ##endif

      else
	 
	 c = nancov(u.z(I&Iu,:)) ;

      endif
      
      C(:,:,is) = c ;

   endfor

endfunction
