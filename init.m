function init (varargin)

   ## usage: init (varargin)
   ## 
   ## initialize variables and create directories

   persistent CFG INIT = false

   if !INIT
      f = dbstack ; printf("\n\t###   %s   ###\n\n", f(1).name) ;
   endif

   clear -g AVAR SVAR GVAR
   global lim=-999 verbose=0 RND GVAR isANA isCUR ADIR SDIR CDIR
   global PAR PRJ PRSV NCPU BATCH ;

   for i = 1:nargin
      eval(varargin{i}) ;
   endfor

   ## system related \\
   if isguirunning, more off ; endif
   warning("off", "Octave:possible-matlab-short-circuit-operator") ;
   if !isunix
      error("init: No unix system. Exiting.\n") ;
   endif
   if isempty(BATCH), BATCH = isempty(strfind(program_invocation_name, "octave")) ; endif
   PAR.HAS64 = __have_feature__("USE_64_BIT_IDX_T") ;
   ## [status PAR.mem] = system("cat /proc/meminfo | head -1 | awk '{print $2}'") ;
   ## PAR.mem = str2num(strtrim(PAR.mem)) * 1e3 / sizeof(0) ;
   [~, PAR.mem] = computer ; PAR.mem /= 10 ;
   init_rand ;
   warning("off", "Octave:assign-as-truth-value") ;
   warning("off", "Octave:shadowed-function") ;
   warning("off", "Octave:broadcast") ;
   warning("off", "backtrace") ;		    
   pkg_get() ;	    # install required packages
   ## system related //

   ### defaults \\
   warning("off", "Octave:possible-matlab-short-circuit-operator") ;
   warning("off", "Octave:assign-as-truth-value") ;
   source("xds.cfg") ;
   if exist("PDD", "var"), PAR.pdd = PDD ; endif
   if exist("RES", "var"), PAR.res = RES ; endif
   if exist("ANA", "var"), PAR.ana = ANA ; endif 
   if (isCFG = ischar(CFG)) && exist(CFG, "file"), source(CFG) ; endif
   if exist("SIM", "var") && !isempty(SIM)
      PAR.sim = strsplit(SIM, " ") ;
   endif
   if exist(file = fullfile("data", PAR.ana, "res.cfg"), "file") ; source(file) ; endif
   if exist(file = [PAR.res ".cfg"], "file") ; source(file) ; endif
   PAR.waitbar = have_window_system && PAR.waitbar ;
   ### defaults //

   ##if !INIT && PAR.bld, bld(PAR.bld) ; endif

   if !exist("TOP", "var")
      TOP = fullfile(pwd, "data") ;
   endif
   pmkdir(TOP) ;
   if ((isANA = (!isfield(PAR, "sim")) || (exist("SIM", "var") && isempty(SIM))))
      printf("init: no simulations defined, using %s.\n", PAR.ana) ;
      PAR.sim = {PAR.ana, PAR.ana} ;
      SVAR = AVAR ;
   endif
   if length(PAR.sim) == 1
      PAR.sim = [PAR.sim PAR.sim] ;
   endif
   MDL = strsplit(PAR.sim{1}, "/"){1} ;
   if exist(file = ["data/" MDL ".cfg"], "file")
      source(file) ;
   endif

   if ~isfield(PAR, "sper") PAR.sper = PAR.cper ; endif
   
   PAR.lon(PAR.lon > 180) = PAR.lon(PAR.lon > 180) - 360 ;

   if !exist("SVAR", "var")
      SVAR = AVAR ;
   endif

   Gpfx = "" ;

   gdata = fullfile("data", PRJ, PAR.ptr) ;
   ldata = fullfile("data", PRJ, PAR.pdd) ;

   if PAR.udf
      MODSFX = [MODSFX "_udf"] ; eofdir = "eof_udf" ;
   else
      eofdir = "eof" ;
   endif

   ANASFX = PAR.res ; # ugly hack for resolution settings
   wANA = strrep(PAR.ana, "/", "_") ;

   PAR.mfile = fullfile(ldata, [ANASFX MODSFX], [wANA ".mod.ob"]) ;
   PAR.plist = fullfile(ldata, [ANASFX MODSFX], [wANA ".ptr.lst"]) ;

   wRES = strrep(PAR.res, ".", "") ;
   if isempty(isCUR)
      isCUR = strcmp(PAR.sim{:}) ;
   endif
   if isANA
      PAR.ptradj = "" ;
      SVAR = AVAR ;
   else
      PAR.res = "" ; # at this point, GCM resolution is a given
      pmkdir(fullfile(gdata, PAR.sim{2}, PAR.res, "clim")) ;
   endif 

   ADIR = fullfile(gdata, PAR.ana, ANASFX) ;
   CDIR = fullfile(gdata, PAR.sim{2}, PAR.res) ;
   SDIR = fullfile(gdata, PAR.sim{1}, PAR.res) ;
   if ~isempty(PAR.curhour) && ~isANA
      CDIR = fullfile(gdata, PAR.sim{2}, PAR.res, sprintf("%02dh", PAR.curhour)) ;
      SDIR = fullfile(gdata, PAR.sim{1}, PAR.res, sprintf("%02dh", PAR.curhour)) ;
   endif
   pmkdir(fullfile(ldata, [ANASFX MODSFX]), fullfile(ADIR, PAR.pdd)) ;
   pmkdir(fullfile(ADIR, "clim"), fullfile(ADIR, eofdir)) ;
   pmkdir(fullfile(CDIR, "clim")) ;
   pmkdir(ldata, fullfile(SDIR, PAR.pdd)) ;

   if isempty(GVAR), GVAR = [AVAR; SVAR] ; endif

   if PAR.dbg, PAR.ncpu = 0 ; endif

   ## annual cycle
   if (PAR.numhar == 3)
      PAR.acyc = @(t) [ones(rows(t),1), cos(t), sin(t)] ;
   elseif (PAR.numhar == 5)
      PAR.acyc = @(t) [ones(rows(t),1) cos(t) sin(t) cos(2*t) sin(2*t)] ;
   endif

   if !INIT
      printf("init: PRJ = %s\n", PRJ) ;
      printf("init: PTR = %s\n", PAR.ptr) ;
      printf("init: PDD = %s\n", PAR.pdd) ;
      printf("init: MDL = %s\n", MDL) ;
      printf("init: ANA = %s\n", PAR.ana) ;
      printf("init: SIM = %s %s\n", PAR.sim{:}) ;
      printf("init: RES = %s\n", wRES) ;
      INIT = true ;
   endif

   PAR = orderfields(PAR) ;
   dp_upd(PAR) ;

   if isANA
      f = [fileparts(PAR.mfile) "/PAR.txt"] ;
      fprintf(fid = fopen(f, "wt"), "%s", disp(PAR)) ; fclose(fid) ;
   endif

endfunction
