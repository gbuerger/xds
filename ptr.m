function ptr (L = true)

   ## usage: ptr (L = true)
   ##
   ## process predictors (analyses and GCM simulations)

   global PAR PRJ GVAR ADIR SDIR CDIR isANA isCUR keof
   persistent warned_ana = false warned_cur = false ;

   dclim = fullfile(CDIR, "clim") ;

   cfile = fullfile(CDIR, [strrep(PAR.ana, "/", "_") ".pc.ob"]) ;

   if ~isempty(f = dbstack)
      printf("\n\t###   %s   ###\n\n", f(1).name) ;
   endif

   ## read ptr input
   gdir = sprintf("data/%s", PAR.sim{1}) ;
   if ~isnewer(file = sprintf("%s/ptr.ob", SDIR), gdir) || PAR.bld || L

      ptr = read_ptr(gdir) ;
      if isempty(ptr)
	 error("xds:xds", "%s> no files found in %s", f(1).name, gdir) ;
      endif
      ptr = add_tp(ptr) ;

      printf("ptr: --> %s\n", file) ; 
      save(file, "ptr") ;

   endif

   ## regrid and normalize
   prbwgt = PAR.prbwgt ; PAR.prbwgt = [1 0] ; ## UGLY
   if !isnewer(zfile=fullfile(SDIR, "ptrz.ob"), file, dclim) || length(dir(dclim)) == 2 || L || PAR.ptrout

      if PAR.ptrout
	 pmkdir(odir=sprintf("%s/%s.ptrout", SDIR, PAR.pdd)) ;
	 if exist(zfile, "file") == 2 && ~isnewer(zfile, odir) return ; endif
      endif
      
      printf("ptr: <-- %s\n", file) ; 
      load(file) ;
      ptr = SelFld(ptr, GVAR(2,:)) ;

      for iptr = 1:columns(GVAR)
	 k = GVAR{2,iptr} ; v = ptr.(k) ;
	 v.isP = !isempty(strfind(tolower(v.name), "precip")) ;
	 v = to_grid(v, k, iptr) ;

	 if PAR.ptrout
##	    lon = v.lon ; lat = v.lat ;
##	    save(pfile = fullfile(SDIR, [k ".geo"]), "-text", "lat", "lon") ;
##	    csvwrite(pfile = fullfile(SDIR, [k ".csv"]), [v.id(:,1:3) v.x]) ;
##	    printf("ptr: --> %s\n", pfile) ; 
	    ptrout(v, odir) ;
	 else
	    ## transform to N(0,1)
	    cfile = fullfile(dclim, [k ".ob"]) ;
	    v = do_norm(v, cfile) ;
	    ptr.(k) = v ;
	 endif
      endfor

      ofile = sprintf("%s/%s.pc.ob", SDIR, strrep(PAR.ana, "/", "_")) ;
      if PAR.ptrout && ~PAR.bld && isnewer(ofile, zfile) && ~L return ; endif

      save(zfile, "ptr") ;
      printf("ptr: --> %s\n", zfile) ; 

   endif
   PAR.prbwgt = prbwgt ; ## UGLY

   ## EOF reduction \\
   ofile = fullfile(SDIR, [strrep(PAR.ana, "/", "_") ".pc.ob"]) ;
   if ~PAR.bld && isnewer(ofile, zfile) && ~L
      return ;
   endif

   printf("ptr: <-- %s\n", zfile) ; 
   load(zfile)
   ptr = SelFld(ptr, GVAR(2,:)) ;

   ptr = common_def(ptr) ;

   PC.id = PC.x = PC.J = [] ; wptr = {} ; keof = 0 ;
   for iptr = 1:columns(GVAR)

      key = GVAR{2,iptr} ;
      if ~isANA
	 printf("ptr: pairing %s with %s\n", GVAR{[2 1],iptr}) ;
      endif
      v = ptr.(key) ; wptr = {wptr{:}, key} ;
      printf("%s: %f\n", key, max(zchk(v.z))) ;
      [pc v] = do_eof(v, GVAR{:,iptr}) ;
      v = rmfield(v, "z") ;

      PC = upd_PC(PC, v, pc, iptr) ;

   endfor
   PC.ptr = wptr ; [PC.nr PC.nc] = size(PC.x) ;

   if isANA
      Gfile = fullfile(ADIR, "G.ob") ;
      per = PAR.cper ;
   elseif isCUR
      Gfile = fullfile(CDIR, "G.ob") ;
      per = PAR.sper ;
   endif
   PCw = selper(PC, per(1,:), per(2,:)) ;
   G = covadj(PCw.x) ;
   printf("ptr: --> %s\n", Gfile) ; 
   save(Gfile, "G") ;

   if strfind(PAR.ptradj, "mlt")
      PC = mltadj(PC) ;
   endif

   save(ofile, "PC") ;
   printf("ptr: --> %s\n", ofile) ;
   ## EOF reduction  //
   
endfunction
