%% usage: a = betafit (x)
%%
%% fit one-parameter beta (MOM)
function a = betafit (x)

   v = var(x) ;
   a = 1 ./ (8*v) - 1/2 ;
   
end
