function varargout = disagg (P, T, lres, hres, par)

   ## usage: varargout = disagg (P, T, lres, hres, par)
   ##
   ## calibrate and/or disaggegate using P, T, with temporal resolution hres (string)

   init_rand ;
   addpath ~/xds/extra/rcm ;
   global TMC MERGE ;

   nT = length(TMC) ;
   if nargin < 3, hres = 15 ; endif

   T.dfd = sum(!isnan(T.x)) / rows(T.x) ;
   for j = 1:columns(P.ll)
      P.JT(j) = findll(P.ll(:,j), T.ll, T.dfd) ;
   endfor

   if nargin < 5
      
      [nr nc] = size(P.x) ;
      if MERGE XX = [] ; endif

      Tm = nanmean(T.x, 2) ;
      for j = 1:nc
	 
	 ## fill NaN of T with Tm
	 Inan = isnan(T.x(:,P.JT(j))) ;
	 T.x(Inan,P.JT(j)) = Tm(Inan) ;

         X = [T.x(P.IT,P.JT(j)) P.x(:,j)] ;

	 if MERGE
	    XX = cat(1, XX, X) ;
	    continue ;
	 endif

	 apar = parfun(@(jT) go_RCMc(jT, X, lres, hres), 1:nT) ;

	 if nT > 1
	    par = parT(apar, TMC) ;
	 else
	    par = apar ;
	 endif
	 wpar(j) = par ;

      endfor

      if MERGE
	 wpar2 = parfun(@(jT) go_RCMc(jT, XX, lres, hres), 1:nT) ;
	 if nT > 1
	    wpar = parT(wpar2, TMC) ;
	 endif
      endif
      
      varargout{1} = wpar ;
      varargout{2} = wpar2 ;
	 
   else

      res = go_RCMd(T, P, par, lres, hres) ;

      ns = 2^round(log2(lres/hres)) ;
      t = datenum(P.id(1,:)) : 1/ns : datenum(P.id(end,:)) + 1 - 1/ns ;
      varargout{1} = datevec(t)(:,1:5) ;
      varargout{2} = cell2mat(res) ;

   endif

endfunction


function res = cellmean (s)

   ## usage: res = cellmean (s)
   ## 
   ## calculate the mean over all elements of s

   i = 0 ;
   for j=1:length(s)
      if !isstruct(s{j}), continue ; endif
      i++ ;
      if i == 1
	 for [v k] = s{j}
	    res.(k) = v ;
	 endfor
      else
	 for [v k] = s{j}
	    res.(k) += v ;
	 endfor
      endif
   endfor

   if i > 0
      for [v k] = res
	 res.(k) = res.(k) / i ;
      endfor
   else
      res = NaN ;
   endif

endfunction


## usage: par = parT (apar, T)
##
## par dependence on T
function par = parT (apar, T)
   nT = length(T) ;
   for k = fieldnames(apar)'
      k = k{:} ;
      v = arrayfun(@(c) c.(k), apar', "UniformOutput", false) ;
      v = cell2mat(v) ;
      N = size(apar(1).(k)) ;
      v = reshape(v, [N(1) nT N(2:end)]) ; 
      K = setdiff(1:ndims(v), 2) ;
      v = permute(v, [2 K]) ;
      v = reshape(v, nT, []) ; 
      p = arrayfun(@(j) fitp(T, v(:,j)'), 1:prod(N), "UniformOutput", false)' ;
      kpol = length(p{1}) ;
      p = cell2mat(p) ;
      p = reshape(p, [N kpol]) ;
      par.(k) = squeeze(mat2cell(p, num2cell(N){:}, ones(1, kpol))) ;
   endfor
endfunction


## usage: p = fitp (t, x)
##
## fit p
function p = fitp (t, x)

   global KPOL
   N = size(x) ;
   I = ~isnan(x) ;
   if sum(I) / length(x) < 0.5
      p = nan(1, KPOL+1) ;
      return ;
   endif
   
   p = polyfit(t(I), x(I), KPOL) ;

endfunction
