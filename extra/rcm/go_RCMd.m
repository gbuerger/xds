## usage: res = go_RCMd (T, P, par, lres, hres)
##
##
function res = go_RCMd (T, P, par, lres, hres)

   global ML MERGE

   if ML
      fRCMd = "RCMd_ML" ;
   else
      fRCMd = "RCMd" ;
   endif

   [nr nc] = size(P.x) ;
   cP = mat2cell(P.x, nr, ones(1,nc)) ;

   if MERGE
      par = repmat(par, 1, nc) ;
   endif
   cpar = num2cell(par) ;

   if iscell(par(1).slom)
      cT = mat2cell(T.x(:,P.JT), nr, ones(1,nc)) ;
      res = parfun(@(vP,vT,vpar) feval(fRCMd, vP, vpar, lres, hres, vT), cP, cT, cpar, "UniformOutput", false) ;
   else
      res = parfun(@(vP,vpar) feval(fRCMd, vP, vpar, lres, hres), cP, cpar, "UniformOutput", false) ;
   endif

endfunction
