%% usage: res = am (pc, T, par, scale)
%%
%%
function res = am (pc, T, par, scale)

   KPOL = length(par.slom) - 1 ;

   if iscell(par.slom)
      for k = fieldnames(par)'
	 N = size(par.(k{:}){1}) ;
	 for i = 1:N(1)
	    for j = 1:N(2)
	       p = arrayfun(@(kpol) par.(k{:}){kpol}(i,j), 1:KPOL+1) ;
	       eval(sprintf('%s(i,j) = polyval(p, T);', k{:})) ;
	    end
	 end
      end

   else

      for k = fieldnames(par)'
	 eval(sprintf('%s = par.%s ;', k{:}, k{:})) ;
      end

   end
   
   j = [1 2 3 2] ;
   j = j(pc) ;

   if scale > alim
      res = 2.^ac(j) ;
   else
      res = 2.^(c3(j) + c4(j) * scale) ;
   end
   
end
