%% usage: [pxxm p01m p10m opar] = pm (T, par, scale, f_gt_1)
%%
%%
function [pxxm p01m p10m opar] = pm (T, par, scale, f_gt_1)

   nvc = 3 ;

   KPOL = length(par.slom) - 1 ;

   if iscell(par.slom)

      for k = fieldnames(par)'

	 k = k{:} ; v = par.(k) ;
	 
	 N = size(v{1}) ;
	 v = cell2mat(v) ;
	 v = reshape(v, N(1), KPOL+1, prod(N(2:end))) ;
	 K = setdiff(1:ndims(v), 2) ;
	 v = permute(v, [2 K]) ;
	 p = reshape(v, KPOL+1, prod(N)) ;
	 w = arrayfun(@(j) polyval(p(:,j), T), 1:prod(N)) ;
	 w = reshape(w, N) ;
	 eval(sprintf('%s = reshape(w, N) ;', k)) ;
	 eval(sprintf('opar.%s = reshape(w, N) ;', k)) ;
      end
      
   else

      for k = fieldnames(par)'
	 eval(sprintf('%s = par.%s ;', k{:}, k{:})) ;
	 eval(sprintf('opar.%s = par.%s ;', k{:}, k{:})) ;
      end

   end

   J = [1 2 3 2] ;
   npc = length(J) ;

   pxxm = arrayfun(@(j) bsxfun(@plus, c1(j,3) + c2(j,3) * scale, slom(j,3) * (1:nvc)), J, 'UniformOutput', false) ;
   pxxm = reshape(cell2mat(pxxm), nvc, npc)' ;

   p01m(2,:) = intm21 + slom(2,1) * (1:nvc) ;
   p01m(4,:) = intm21 + slom(2,1) * (1:nvc) ;
					    
   p10m(2,:) = intm21 + slom(2,1) * (1:nvc) ;
   p10m(4,:) = intm21 + slom(2,1) * (1:nvc) ;

   pxxm = max(min(pxxm, 1), 0) ;
   p01m = max(min(p01m, 1), 0) ;
   p10m = max(min(p10m, 1), 0) ;

   %%case [1 3]
   p01m([1 3],:) = (1 - pxxm([1 3],:)) / 2 ;
   p10m([1 3],:) = (1 - pxxm([1 3],:)) / 2 ;

   %%case 2	% Position class 2 (starting)
   I01 = pxxm + p01m > 1 ;
   I01([1 3 4],:) = false ;
   p01m(I01) = 1 - pxxm(I01);
   p10m(I01) = 0;
   p10m(~I01) = 1 - (pxxm(~I01) + p01m(~I01));

   %%case 4	% Position class 4 (stopping)
   I10 = pxxm + p10m > 1 ;
   I10([1 2 3],:) = false ;
   p10m(I10) = 1 - pxxm(I10);
   p01m(I10) = 0;
   p01m(~I10) = 1 - (pxxm(~I10) + p10m(~I10));

   
 % if fgt1=0 there are only "one tip" values in the smallest volume class, then nothing needs to be done   
   I = (f_gt_1 > 0) ;
   pxxm(I,1) = pxxm(I,1) ./ f_gt_1(I); %updated pxxm, to take "one tip" values into account

   II = (pxxm(:,1) >= 1) ;
   pxxm(I & II,1) = 1;
   p01m(I & II,1) = 0;
   p10m(I & II,1) = 0;

   p01m(I & ~II,1) = p01m(I & ~II,1) / (p01m(I & ~II,1) + p10m(I & ~II,1)) * (1-pxxm(I & ~II,1)) ;
   p10m(I & ~II,1) = 1 - (p01m(I & ~II,1) + pxxm(I & ~II,1)) ;

end
