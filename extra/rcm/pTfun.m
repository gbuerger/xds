function res = pTfun (p, T)
   global pT
   q = pT.q ; Tm = pT.Tm ;
   pw = q * (1 - p) ;
   a = atanh((pw - p)/(pw + p)) ;
   res = (pw+p) * (1 + tanh(T - Tm - a)) / 2 ;
endfunction
