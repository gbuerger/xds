function res = uniscl (s)

   ## usage: res = uniscl (s)
   ## 
   ## transform to uniform scale (per time unit)

   global lres

   res = s ;

   t = datenum(s.id) ;
   td = diff(t) ;

   [td0 tu st i] = maxhit(td) ;
   td = [td0 ; td] ;

   for j = 1:columns(s.x)

      if any(~isnan(s.x(:,j))) && ~any(diff(s.x(:,j)) < 0)
	 res.x(:,j) = [NaN  ; diff(s.x(:,j)) ./ (td * lres)] ; # per minute
      else
	 res.x(:,j) = s.x(:,j) ./ (td * lres) ;
      endif

   endfor

endfunction
