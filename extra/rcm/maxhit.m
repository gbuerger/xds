function [td0, u, s, i] = maxhit (td)

   ## usage: [td0, u, s, i] = maxhit (td)
   ##
   ## select maximum hit

   [u I J] = unique(td) ;

   C = num2cell(1:length(I)) ;
   s = parfun(@(i) sum(J == i, 1), C)' ;

   [~, i] = max(s) ;
   td0 = u(i) ;

endfunction
