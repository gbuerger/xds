## usage: res = RCMc_ML (P, fRCMc, lres, hres)
##
## wrapper function for RCMc (using matlab)
function res = RCMc_ML (P, lres, hres)

   mkdir(tdir = tempname) ;
   save("-mat", sprintf("%s/RCMc_in.mat", tdir), "P", "lres", "hres") ;

   fid = fopen(cfile = sprintf("%s/RCMc_cmd.m", tdir), "w") ;
   fprintf(fid, "cd %s ;\n", tdir) ;
   fprintf(fid, "addpath ~/xds/extra/rcm ;\n") ;
   fprintf(fid, "load RCMc_in.mat ;\n") ;
   fprintf(fid, "res = RCMc(P, lres, hres) ;\n") ; 
   fprintf(fid, "save RCMc_out.mat res ;\n") ;
   fprintf(fid, "quit ;\n") ;
   fclose(fid);
   
   cmd = sprintf("matlab -nodisplay -nodesktop -nosplash -r \"run %s\"", cfile) ;

   [st out] = system(cmd) ;

   load(sprintf("%s/RCMc_out.mat", tdir)) ;

   rmdir(tdir, "s") ;

endfunction
