function par = RCMc (x, lres, hres)

   %% usage: ac = RCMc (data, lres, hres)
   %% 
   %% 

   global Tb WB
   if exist('pkg') == 2
      pkg load statistics
   end
%%   addpath ~/oct/wafo/statistics
%%   addpath ~/oct/wafo/numdifftools
%%   addpath ~/oct/wafo/misc

%%   warning('off', 'WAFO:LOGPS') ;

   Ndfd = 100000 ;
   slom = nan(3,3) ;
   c1 = nan(3,3) ;
   c2 = nan(3,3) ;
   c3 = nan(1,3) ;
   c4 = nan(1,3) ;
   ac = nan(1,3) ;
   intm21 = NaN ;
   alim = NaN ;
   par = struct('slom', slom, 'c1', c1, 'c2', c2, 'intm21', intm21, 'c3', c3, 'c4', c4, 'alim', alim, 'ac', ac) ;

   I = ~isnan(x) ;
   if sum(I) < Ndfd
      return ;
   end

   x = x(I) ;
   
   nstep = round(log2(lres/hres));
   boxs = 2.^(0:nstep)' ;
   boxt=boxs*hres;
   for cs=1:nstep
      cst(cs)=mean(boxt(cs:cs+1));
   end

   nvc=3; %number of volume classes assumed (note that three classes is hardcoded below on lines 80-83, another number requires adjustments there)

   [nr,nc] = size(x); % nc - columns (1), nr - rows
   if nc > 1
      error('x must be vector') ;
   end
   
   % make a matrix with successively 2-by-2 aggregated values
   % it has nr rows in the first column, then successively halved nr, and nstep+1 columns

%%   data = nan(nr,nstep+1) ;
   data{1} = x ; clear x ;
   for cs=1:nstep
      if (cs == 1)
         npar(1)=(nr-mod(nr,2))/2;
      else
         npar(cs)=(npar(cs-1)/2)-(mod((npar(cs-1)/2),2))/2;
      end
      I = (1:npar(cs))' ; 
      data{cs+1}(I,1) = data{cs}(2*I-1) + data{cs}(2*I) ;
%%      for ap=1:npar(cs)
%%         ao=ap*2; 
%%         data(ap,cs+1)=data(ao-1,cs)+data(ao,cs);
%%      end
   end
   npar = floor(npar) ;
   
   % create matrices with "parent information"

   if WB, hw = waitbar(0) ; end
   for cs=1:nstep %nstep

      if WB, waitbar(cs/nstep, hw, sprintf('cs=%d, nstep=%d', cs, nstep)) ; end
      
      parn(1:4)=0;
      clear parval parinfo ;
      J = (2:npar(cs)-1)' ;
      I = data{cs+1}(J) > 0 ;
      I1 = I & (data{cs+1}(J-1) == 0 & data{cs+1}(J+1) == 0) ;
      I2 = I & ~I1 & (data{cs+1}(J-1) == 0 & data{cs+1}(J+1)  > 0) ;
      I3 = I & ~I1 & ~I2 & (data{cs+1}(J-1)  > 0 & data{cs+1}(J+1)  > 0) ;
      I4 = I & ~I1 & ~I2 & ~I3 ;
      parn(1) = sum(I1) ;
      parn(2) = sum(I2) ;
      parn(3) = sum(I3) ;
      parn(4) = sum(I4) ;
      parval(1:parn(1),1) = data{cs+1}(J(I1)) ;
      parval(1:parn(2),2) = data{cs+1}(J(I2)) ;
      parval(1:parn(3),3) = data{cs+1}(J(I3)) ;
      parval(1:parn(4),4) = data{cs+1}(J(I4)) ;
      parinfo(J(I1),1) = 1 ;
      parinfo(J(I2),1) = 2 ;
      parinfo(J(I3),1) = 3 ;
      parinfo(J(I4),1) = 4 ;
      J1 = I & data{cs}(2*J-1) == 0 ;
      parinfo(J(J1),3) = 1 ;
      J2 = I & data{cs}(2*J) == 0 ;
      parinfo(J(J2),3) = 2 ;
      II = I & ~(J1|J2) ;
      parinfo(J(II),3) = 3 ;
      parinfo(J(II),4) = data{cs}(2*J(II)-1) ./ data{cs+1}(J(II)) ;
      j = find(any(parinfo ~= 0, 2)) ; j = j(end) ;
      parinfo = parinfo(1:j,:) ;

      % based on three volume classes, make volume limits
      
      lim(1,1:2)=prctile(parval(1:parn(1),1),[33 67]);  
      lim(2,1:2)=prctile(parval(1:parn(2),2),[33 67]);  
      lim(3,1:2)=prctile(parval(1:parn(3),3),[33 67]);   
      lim(4,1:2)=prctile(parval(1:parn(4),4),[33 67]);   

      % add volume class in parinfo, column 2: 1 - small volumes, 2 - intermediate, 3 - large 
      
      for pc=1:4
         parpos=find(parinfo(:,1)==pc); %positions of a certain parent type        
         if (lim(pc,1)==lim(pc,2)) %special case when identical limits, because all small values are identical
            for pp=1:parn(pc)
               if (data{cs+1}(parpos(pp)) <= lim(pc,1)) %in this case, this means belonging to either volume class 1 or 2
                  vcproxy=mod(pp,2)+1; %this gives 2 if pp is odd and 1 if even
                  parinfo(parpos(pp),2)=vcproxy; %this assigns half of the parents to vc 1 and the other half to vc 2
               else
                  parinfo(parpos(pp),2)=3;                 
               end
            end     
         else
            for pp=1:parn(pc)
               if (data{cs+1}(parpos(pp)) <= lim(pc,1))
                  parinfo(parpos(pp),2)=1;
               elseif (data{cs+1}(parpos(pp)) <= lim(pc,2))
                  parinfo(parpos(pp),2)=2;                
               else
                  parinfo(parpos(pp),2)=3;                 
               end
            end
         end
      end
      
      % calculate p-statistics

      for pc=1:4
         ppos=find(parinfo(:,1)==pc); %positions of a certain parent class        
         pinfo=parinfo(ppos,:);   
         
	 % in the evaluation of probabilities, all volume classes are used
	 % results in matrix prob        
         
         for vc=1:nvc 
            vpos=find(pinfo(:,2)==vc); %positions of a certain volume class  
            pvinfo=pinfo(vpos,:);   
            pvn=size(pvinfo,1); %number of parents in this pos/vol class
            for dt=1:3
               n(cs,pc,vc,dt)=size(find(pvinfo(:,3)==dt),1); %number of parents in this pos/vol/dt class
               prob(cs,pc,vc,dt)=n(cs,pc,vc,dt)/pvn;
            end
         end

	 % in the evaluation of x/x-histograms, all volume classes are pooled
	 % results in matrix hist

         xpos=find(pinfo(:,3)==3); %positions of x/x-divisions
         xn=size(xpos,1);        
         histo(cs,pc,1:xn)=pinfo(xpos,4);  
         
      end

   end
   if WB, close(hw) ; end
   % save observed probabilities for comparison with model probabilities in
   % RCD_... (note that scale is reversed in observed and modeled arrays)

   pxxo=prob(:,:,:,3);
   p01o=prob(:,:,:,1);
   p10o=prob(:,:,:,2);

   % probabilities - general results

   x1v=[ones(nvc,1) (1:nvc)']; %vol class numbers = x in regression (+ a first row of ones, needed for regress in model 1)

   i=0;
   for pc=1:4
      for dt=1:3
         i=i+1;
         pcc=num2str(pc);
         dtc=num2str(dt);   
         for vc=1:nvc %all probabilities are plotted
            pplot(vc,:)=prob(:,pc,vc,dt);
         end
         nsum=sum(n(:,pc,:,dt)); %number of parents in this pos/dt class
         nsumc=num2str(sum(nsum));
         y=mean(pplot,2); %mean probability for each vol in this pos/dt class = y in regression
         mod1=regress(y,x1v); %model 1 is the linear model of mean probabilities
         for vc=1:nvc
            mod1res(vc,1)=mod1(1)+mod1(2)*vc; 
         end
         m1slo(pc,dt)=mod1(2); %slope of mean regression, save for model 2 below
      end
   end

   % probabilities - parameter estimation and plotting

   i=0;
   for pc=1:4
      for dt=1:3
         i=i+1;
         pcc=num2str(pc);
         dtc=num2str(dt);   
         for vc=1:nvc %all probabilities are plotted
            pplot(vc,:)=prob(:,pc,vc,dt);
         end
         nsum=sum(n(:,pc,:,dt)); %number of parents in this pos/dt class
         nsumc=num2str(sum(nsum));
         m1sloc=num2str(m1slo(pc,dt)); %mean regression slope as string
         mod2c=['a+' m1sloc '*x']; %string expression of model 2, i.e. linear regression with fixed mean slope
         %% mod2=fittype(mod2c); %type of general model to be fitted
         x2v=x1v(:,2); %only the vol class numbers
         for cs=1:nstep
            %% m2res=fit(x2v,pplot(:,cs),mod2,'Startpoint',0); %linear fit with mean slope to each cascade step 
            %% m2int(cs,pc,dt)=coeffvalues(m2res); %m2int contains the resulting intercept
            m2int(cs,pc,dt) = mean(pplot(:,cs) - m1slo(pc,dt)*x2v); %m2int contains the resulting intercept
         end
         if (dt==2)
            d01nsum=sum(n(:,pc,:,1),3); %number of "0/1-parents" of this pc, all vcs, for each cs 
            d10nsum=sum(n(:,pc,:,2),3); %number of "1/0-parents" of this pc, all vcs, for each cs 
            d01frac=d01nsum./(d01nsum+d10nsum); %fraction of 0/1-parents of all "non-x/x-parents"
            i=i+1;
         end
      end
   end

   % probabilities - simplified model information 

   dum1=cst';
   cstlog2=log2(dum1);
   if exist('~/rfm/data/EGLV/cstlog2.mat', 'file') ~= 2
      save ~/rfm/data/EGLV/cstlog2.mat cstlog2
   end
   x1p=[ones(nstep,1) cstlog2];

   % Position class 1 (isolated) 
   slom(1,3)=m1slo(1,3); %mean slope of pxx
   y=m2int(:,1,3); %associated intercept
   mdl=regress(y,x1p);
   c1(1,3)=mdl(1);
   c2(1,3)=mdl(2);

   % Position class 3 (enclosed)
   slom(3,3)=m1slo(3,3); %mean slope of pxx
   y=m2int(:,3,3); %associated intercept
   mdl=regress(y,x1p);
   c1(3,3)=mdl(1);
   c2(3,3)=mdl(2);

   % Position classes 2 and 4 (edge) 
   dum4=[m1slo(2,3) m1slo(4,3)]; %pool pxx slopes from 2 and 4
   slom(2,3)=mean(dum4); %totalt mean slope of pxx
   dum5=[m1slo(2,1) m1slo(4,2)]; %pool p01 and p01 slopes from 2 and 4
   slom(2,1)=mean(dum5); %totalt mean slope of p01 and p10
   dum2=[m2int(:,2,3) m2int(:,4,3)]; %pool pxx intercepts
   y=mean(dum2,2); %mean pxx intercept
   mdl=regress(y,x1p);
   c1(2,3)=mdl(1);
   c2(2,3)=mdl(2);
   dum3=[m2int(:,2,1);m2int(:,4,2)]; %pool p01 and p10 intercepts
   intm21=mean(dum3); %mean p01 and p10 intercept (constant in the model)

   % histograms - shape

   i=0;
   for pc=1:4
      for cs=1:nstep
         i=i+1;
         hplotp=find(squeeze(histo(cs,pc,:)>0));
         xn=size(hplotp,1);
         clear hplot
         hplot(1:xn)=histo(cs,pc,hplotp);
         for j=1:xn
            hplot(xn+j)=1-hplot(j); %in histo only one weight is given, here the complementary one is added to get symmetry
         end
         if (xn>4) %if less than 5 "unique" weights (i.e. 10 with the complementary), nothing is done (OK?)
            bins=round(1+3.3*log10(2*xn)); %number of bins according to recommended formula
            [nn,bc]=hist(hplot,bins); %create histogram
            afit=betafit(hplot); %fit beta distribution
%%	    options.method = "mps" ;
%%            phat=fitbeta(hplot, options); %fit beta distribution
%%	    afit = phat.params ;
            ao(cs,pc)=afit(1); %save the a parameter
            bp=betapdf(bc,afit(1),afit(1)); %generate the fitted distribution
         end
         pcc=num2str(pc);
         csc=num2str(cs);
         xnc=num2str(xn);
      end
   end

   % histograms - a-parameter

   i=0;
   for pc=1:4
      i=i+1;
      pcc=num2str(pc);
      i=i+1;
      balog2(:,pc)=log2(ao(:,pc));
   end
   % histograms - simplified model information 

   alcs=nstep-2; %aggregation step below which c3 and c4 is used / above which constant ac is used (because too few values for relaiable estimation of a at the last aggregation steps)
   alim=x1p(alcs,2); %scale below which c3 and c4 is used / above which constant ac is used

   % Position class 1 (isolated) 
   y = balog2(1:alcs,1) ; x = x1p(1:alcs,:) ;
   mdl=regress(balog2(1:alcs,1),x1p(1:alcs,:));
   c3(1)=mdl(1);
   c4(1)=mdl(2);
   ac(1)=mean(balog2(alcs:nstep,1));
   
		    % Position class 3 (enclosed)
   I = isfinite(balog2(1:alcs,3)) ;
   if ~any(I)
      I = isfinite(balog2(:,3)) ;
   end
   mdl=regress(balog2(I,3),x1p(I,:));
%%   mdl=regress(balog2(1:alcs,3),x1p(1:alcs,:));
   c3(3)=mdl(1);
   c4(3)=mdl(2);
   I = isfinite(balog2(alcs:nstep,3)) ;
   if ~any(I)
      I = isfinite(balog2(:,3)) ;
   end
   ac(3)=mean(balog2(I,3));
%%   ac(3)=mean(balog2(alcs:nstep,3));

   % Position classes 2 and 4 (edge) 
   dum6=[balog2(1:alcs,2) balog2(1:alcs,4)];
   y=mean(dum6,2);
   I = isfinite(y) ;
   mdl=regress(y(I),x1p(I,:));
%%   mdl=regress(y,x1p(1:alcs,:));
   c3(2)=mdl(1);
   c4(2)=mdl(2);
   dum7=[balog2(alcs:nstep,2);balog2(alcs:nstep,4)];
   I = isfinite(dum7) ;
   ac(2)=mean(dum7(I));

   par = struct('slom', slom, 'c1', c1, 'c2', c2, 'intm21', intm21, 'c3', c3, 'c4', c4, 'alim', alim, 'ac', ac, 'pxxo', pxxo, 'p01o', p01o, 'p10o', p10o) ;

end
