function out = RCMd (x, par, lres, hres, y)

   %% usage: out = RCMd (x, par, lres, hres, y)
   %% 
   %% 

   global vres

   if exist ('OCTAVE_VERSION', 'builtin') > 0
      pkg load statistics
   end
   
   nfun = 0 ;
   if 0
      eval('persistent nfun ;') ;
   end
   nfun = nfun + 1 ;

   hasT = ~isempty(y) ;

   x = round(x / vres) ;

   nstep = round(log2(lres/hres)) ;
   boxs = 2.^(nstep:-1:0)' ;

   nrea = 1 ; %number of desired stochastic realizations

   nr = size(x, 1);
   data = nan(nr*2^nstep,nstep+1) ; data(1:nr,1) = x ;

   out = nan(nr*2^nstep,nrea) ;

   if all(isnan(x)), return ; end

   if all(data(:) == 0)
      out = zeros(nr*(2^nstep),nrea) ;
      return ;
   end

   boxt=boxs*hres;		% minutes
   for cs=1:nstep
      cst(cs)=mean(boxt(cs:cs+1));
   end
   dum1=cst';
   cstlog2=log2(dum1);

   if hasT
      T0 = nanmean(y) ;
   else
      T0 = [] ;
   end

		    % Disaggregation model

   for cs=1:nstep

      scale=cstlog2(cs);
      
			  % start by creating matrices with "parent information"
      npar=nr*(2^(cs-1)); %number of "parents"
      nkid=nr*(2^cs); %number of "kids"
      parn(1:4)=0;
      clear parinfo;
      for ap=1:npar
         if (ap==1) %start of series, assume 0 before
            lef=0;
         else
            lef=data(ap-1,cs);
         end
         if (ap==npar) %end of series, assume 0 after
            rig=0;
         else
            rig=data(ap+1,cs);
         end
         if (data(ap,cs)>0)            
            if ((lef==0) && (rig==0))
	       parn(1)=parn(1)+1;
	       parval(parn(1),1)=data(ap,cs); %matrix with "parent values"
	       parinfo(ap,1)=1; %column 1 "parent types": 1 - isolated, 2 - starting, 3 - enclosed, 4 - stopping
            elseif ((lef==0) && (rig>0))
	       parn(2)=parn(2)+1; 
	       parval(parn(2),2)=data(ap,cs);    
	       parinfo(ap,1)=2; %column 1 "parent types": 1 - isolated, 2 - starting, 3 - enclosed, 4 - stopping               
            elseif ((lef>0) && (rig>0))
	       parn(3)=parn(3)+1;
	       parval(parn(3),3)=data(ap,cs);    
	       parinfo(ap,1)=3; %column 1 "parent types": 1 - isolated, 2 - starting, 3 - enclosed, 4 - stopping                   
            else
	       parn(4)=parn(4)+1;
	       parval(parn(4),4)=data(ap,cs);    
	       parinfo(ap,1)=4; %column 1 "parent types": 1 - isolated, 2 - starting, 3 - enclosed, 4 - stopping                   
            end
         end
      end

      parn;

				% based on three volume classes, make volume limits
      for pt=1:4
	 if parn(pt) > 10
	    vlim(pt,1:2)=prctile(parval(1:parn(pt),pt),[33 67]);
	 else
	    vlim(pt,1:2) = Inf ;
	 end
      end
      vlim;

  % add volume class in parinfo, column 2: 1 - small volumes, 2 - intermediate, 3 - large 
      
      for pc=1:4
         parpos=find(parinfo(:,1)==pc); %positions of a certain parent type        
         nps=0;
         np1=0;
         for pp=1:parn(pc)
            if (data(parpos(pp),cs) <= vlim(pc,1))
	       parinfo(parpos(pp),2)=1;
	       nps=nps+1; %total number of parents in the smallest volume class             
	       if (data(parpos(pp),cs)==1)
                  np1=np1+1; %number of parents with only "one tip"  in the smallest volume class (this type can not be x/x-divided and the probabilities need to be adjusted for this, below) 
	       end
            elseif (data(parpos(pp),cs) <= vlim(pc,2))
	       parinfo(parpos(pp),2)=2;                
            else
	       parinfo(parpos(pp),2)=3;                 
            end
         end
	 if nps > 0
            f_gt_1(pc,1)=(nps-np1)/nps; %fraction of values larger than "one tip" in the smallest volume class
	 else
	    f_gt_1(pc,1) = NaN ;
	 end
      end

		    % Disaggregation start    
%%      hw = waitbar(0) ;
      for ap=1:npar

%%	 waitbar(ap/npar, hw) ;
	 if hasT
	    id = ceil(ap / 2^(cs-1)) ;
	    T = y(id) ;
	    if isnan(T)
	       T = T0 ;
	    end
	 else
	    T = [] ;
	 end

	 %% T dependence here
	 [pxxm p01m p10m] = pm(T, par, scale, f_gt_1) ;

         pos1=(2*ap)-1;
         pos2=2*ap;
	 data([pos1 pos2],end) = data(ap,end) ;
         if (data(ap,cs)==0)
            data(pos1,cs+1)=0;
            data(pos2,cs+1)=0;
         elseif isnan(data(ap,cs))
            data(pos1,cs+1)=NaN;
            data(pos2,cs+1)=NaN;
         else
            pc=parinfo(ap,1);
            vc=parinfo(ap,2);    
            if (data(ap,cs)==1) %if data is one tip, x/x-div is not possible, therefore rescale p01 and p10 to sum one          
	       p01ms=p01m(pc,vc)/(p01m(pc,vc)+p10m(pc,vc));
	       p10ms=p10m(pc,vc)/(p01m(pc,vc)+p10m(pc,vc));
	       ran=rand;
	       if (ran<p01ms)
                  data(pos1,cs+1)=0;
                  data(pos2,cs+1)=1;
	       else
                  data(pos1,cs+1)=1;
                  data(pos2,cs+1)=0;
	       end       
            else
	       ran=rand;
	       if ran < p01m(pc,vc)
                  data(pos1,cs+1)=0;
                  data(pos2,cs+1)=data(ap,cs);
	       elseif ran < p01m(pc,vc) + p10m(pc,vc)
                  data(pos1,cs+1)=data(ap,cs);
                  data(pos2,cs+1)=0;
	       else
		  am_ = am(pc, T, par, scale);
                  ran2 = gamrnd_(am_, 1, 2, 1) ;
                  ran3=ran2/sum(ran2);
                  data(pos1,cs+1)=round(ran3(1)*data(ap,cs));
                  data(pos2,cs+1)=round(ran3(2)*data(ap,cs));
                  if (data(pos1,cs+1)==0) % to avoid that an x/x-div results in one zero-values
		     data(pos1,cs+1)=1;
		     data(pos2,cs+1)=data(pos2,cs+1)-1;
                  elseif (data(pos2,cs+1)==0) 
		     data(pos2,cs+1)=1;
		     data(pos1,cs+1)=data(pos1,cs+1)-1;
                  end
	       end
            end
         end
      end
   end

   out = vres * data(:,nstep+1) ;

end


function res = gamrnd_ (varargin)

   %% usage: res = gamrnd_ (varargin)
   %% 
   %% 
 
   res = gamrnd(varargin{:}) ;

   if varargin{1} <= 0, res = 0 ; end
   
end
