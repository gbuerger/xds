## usage: res = MCbc (f, o, pdf)
##
## MC bias correction
function res = MCbc (f, o, pdf="wbl")

   N = size(o) ;

   if N(2) > 1

      Cf = mat2cell(f, rows(f), ones(1, N(2))) ;
      Co = mat2cell(o, rows(o), ones(1, N(2))) ;
      
      res = parfun(@(u,v) MCbc(u, v), Cf, Co, "UniformOutput", false) ;
      res = cell2mat(res) ;
      return ;

   endif

   if isstruct(o)

      I = (f > 0) ;
      res(~I,1) = 0 ;

      par = o ;

##      Ff = @(x) feval([pdf "cdf"], x, par.pf.scale, par.pf.shape) ;
##      IFo = @(x) feval([pdf "inv"], x, par.po.scale, par.po.shape) ;
##      res(I,1) = IFo(Ff(f(I))) ;

      res(I,1) = ppval(o.pp, f(I)) ;
      
##      ## deal with Infs
##      II = isinf(res) ;
##      x0 = max(f(~II)) ;
##      slope = (par.po.mx - x0) / (max(f(II)) - x0) ;
##      res(II,1) = x0 + (f(II) - x0) * slope ;
      
   else

      f = f(f>0) ;
      o = o(o>0) ;
      [Q S] = qqplot(o, f) ;
      par.pp = splinefit(Q, S, 5) ;
      
##      par.pf = feval([pdf "fit"], f(f>0)) ;
##      par.po = feval([pdf "fit"], o(o>0)) ;

      par.po.mx = max(o(isfinite(o))) ;
      
      res = par ;

   endif
   
endfunction
