## usage: par = go_RCMc (jT, X, lres, hres)
##
## wrapper for RCM
function par = go_RCMc (jT, X, lres, hres)

   global ML TMC

   if ML
      fRCMc = "RCMc_ML" ;
   else
      fRCMc = "RCMc" ;
   endif

   wTMC = [TMC Inf] ;
   
   lX = wTMC(jT) ;
   uX = wTMC(jT+1) ;

   I = ~isnan(X(:,2)) & lX <= X(:,1) & X(:,1) <= uX ;

   par = feval(fRCMc, X(I,2), lres, hres) ;

endfunction
