## usage: res = RCMd_ML (P, par, lres, hres, T)
##
## wrapper function for RCMd (using matlab)
function res = RCMd_ML (P, par, lres, hres, T = [])

   global vres

   mkdir(tdir = tempname) ;
   save("-mat", sprintf("%s/RCMd_in.mat", tdir), "P", "par", "lres", "hres", "T", "vres") ;

   fid = fopen(cfile = sprintf("%s/RCMd_cmd.m", tdir), "w") ;
   fprintf(fid, "cd %s ;\n", tdir) ;
   fprintf(fid, "addpath ~/xds/extra/rcm ;\n") ;
   fprintf(fid, "load RCMd_in.mat ;\n") ;
   fprintf(fid, "res = RCMd(P, par, lres, hres, T) ;\n") ; 
   fprintf(fid, "save RCMd_out.mat res ;\n") ;
   fprintf(fid, "quit ;\n") ;
   fclose(fid);

   cmd = sprintf("matlab -nodisplay -nodesktop -nosplash -r \"run %s\"", cfile) ;

   [st out] = system(cmd) ;

   load(sprintf("%s/RCMd_out.mat", tdir)) ;

   rmdir(tdir, "s") ;
   
endfunction
