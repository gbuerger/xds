
cd ~/xds/data/SaWaM/brazil/pdd

addpath ~/xds/fun
pkg load netcdf
global lim = -99.9 ;
##f = glob("*.xlsx"){1} ;
##[numarr, txtarr, rawarr, limits] = xlsread(f, "Sheet1", "A1:A3") ;

G = dlmread("SFextended_watersheds.csv", ",")(:,1:2) ;
q = 1.0 ;
G0 = mean(G) ; Ge = G0 + q*(G - G0) ;

if 0
   clf ; hold on
   plot(G(:,1), G(:,2), ".", "color", 0*[1 1 1]) ;
   plot(Ge(:,1), Ge(:,2), ".", "color", 1*[1 0 0]) ;
   xlabel("lon") ; ylabel("lat") ;
   print SaoFran.svg

   ncf = "1961/pr_daily_inmet_ana_sinda_19610101_19611231.nc"
   lat = ncread(ncf, "latitude") ;
   lon = ncread(ncf, "longitude") ;   

   J = [1 find(abs(diff(G(:,1)))>0.06 & abs(diff(G(:,2)))>0.06)' rows(G)] ;
   IN = false(rows(lat), 1) ; clf ; hold on
   for j = 2:length(J)
      I = J(j-1):J(j) ;
      plot(G(I,1), G(I,2), ".", "color", 0*[1 1 1]) ;
      [in on] = inpolygon(lon, lat, Ge(I,1), Ge(I,2)) ;
      IN = (IN | in | on) ;
   endfor
   lon(~IN) = nan ; lat(~IN) = nan ;
   plot(lon(:), lat(:), "+", "color", 0.7*[1 1 1]) ;
endif

for v = {"t2m" "pr"}

   v = v{:} ;
   if exist(file = [v ".ob"], "file") == 2

      load(file) ;

   else

      F = glob(sprintf("*/%s_*.nc", v))' ; nF = length(F) ; ncf = F{1} ;
      lat = ncread(ncf, "latitude") ;
      lon = ncread(ncf, "longitude") ;
      code = ncread(ncf, "code") ;
      nc = ncinfo(ncf) ;
      [~,jv] = keyval(nc.Variables, v) ;
      lim = keyval(nc.Variables(jv).Attributes, "missing_value") ;
      
      IN = false(rows(lat), 1) ;
      for j = 2:length(J)
	 I = J(j-1):J(j) ;
	 [in on] = inpolygon(lon, lat, Ge(I,1), Ge(I,2)) ;
	 IN = (IN | in | on) ;
      endfor

      eval(sprintf("%s.id = %s.x = [] ;", v, v)) ;
      for ncf = F
	 ncf = ncf{:} ;
	 t0 = datenum(str2num(ncf(1:4)), 1, 1) ;
	 id = datevec(t0 + ncread(ncf, "time"))(:,1:3) ;
	 x = ncread(ncf, v)(IN,:)' ;
	 x = lim2nan(x) ;
	 eval(sprintf("%s.id = cat(1, %s.id, id) ;", v, v)) ;
	 eval(sprintf("%s.x = cat(1, %s.x, x) ;", v, v)) ;
      endfor
      eval(sprintf("%s.ids = code ;", v)) ;
      save(file, v, "IN") ;
   endif

   
endfor

subplot(2,1,1) ;
cla ; hold on ;
plot(dateax(t2m.id), nanmean(t2m.x, 2)) ;
plot(dateax(pr.id), nanmean(pr.x, 2)) ;
datetick
subplot(2,1,2) ;
cla ; hold on ;
plot(dateax(t2m.id), 100*sum(~isnan(t2m.x), 2) ./ rows(t2m.x)) ;
plot(dateax(pr.id), 100*sum(~isnan(pr.x), 2) ./ rows(pr.x)) ;
datetick

t2m.Ndfd = sum(~isnan(t2m.x), 1) ;
pr.Ndfd = sum(~isnan(pr.x), 1) ;
[~,t2m.Js] = sort(t2m.Ndfd) ;
[~,pr.Js] = sort(pr.Ndfd) ;

clf ; hold on ;
plot(100*t2m.Ndfd(t2m.Js) / rows(t2m.x))
plot(100*pr.Ndfd(pr.Js) / rows(pr.x))

t2m.x = t2m.x(:,t2m.Js) ;
t2m.ids = t2m.ids(find(IN)(t2m.Js)) ;
pr.x = pr.x(:,pr.Js) ;
pr.ids = pr.ids(find(IN)(pr.Js)) ;

disp_dfd(t2m, 1)
xlabel("year") ; ylabel("stations") ;
title("stations vs. time with data, t2m") ;
hgsave("~/octmp/t2m.og") ;
disp_dfd(pr, 1)
xlabel("year") ; ylabel("stations") ;
title("stations vs. time with data, pr") ;
hgsave("~/octmp/pr.og") ;

hgload("/home/gerd/octmp/t2m.og") ;

Idfd = ~isnan(t2m.x) ;      
i = find(all(t2m.id == [1980 1 1], 2))
j = find(Idfd(i,:))(1)
Idfd = ~isnan(pr.x) ;      
i = find(all(pr.id == [2000 1 1], 2))
j = find(Idfd(i,:))(1)

V = {"Tn" "T" "Tx"} ;
pfx = {"Mahanadi_MinT" "Mahanadi_MeanT" "Mahanadi_MaxT"}
for jv = 1:length(V)
   v = V{jv} ;
   if exist(file = [v ".ob"], "file") == 2
      load(file) ;
   else
      t = datenum(1971, 1, 1) - 1 ;
      s.id = s.x = [] ;
      for f = glob([pfx{jv} "*.xlsx"])'(3:7)
	 xls = xlsopen(f{:}, 0, "oct") ;
	 x = xls2oct(xls) ;
	 x = cell2mat(xls2oct(xls)) ;
	 id = datevec(t + (1:rows(x)-2)) ;
	 LAT = x(1,:) ; LON = x(2,:) ;
	 [IN ON] = inpolygon(LON(:), LAT(:), Ge(:,1), Ge(:,2)) ;
	 IN = (IN | ON) ;
	 s.id = cat(1, s.id, id(:,1:3)) ;
	 s.x = cat(1, s.x, x(3:end,IN)) ;
	 t += rows(x)-2 ;
      endfor
      s.lon = LON(IN) ; s.lat = LAT(IN) ; s.alt = lim + 0*LON(IN) ;
      s.ids = strsplit(sprintf("%.2f_%.2f\n", [LON(IN)' LAT(IN)']'), "\n")(1:end-1) ;
      eval([v " = s ;"]) ;
      eval(["save " v ".ob " v]) ;
   endif
endfor

## write xds input
V = {"Tn" "T" "Tx" "P"} ;
VN = {"min temperature" "mean temparature" "max temperature" "precipitation"} ;
XDS = "~/xds/data/SHIVA/pdd/" ;
## meta.txt
mid = fopen(ofile = [XDS "meta.txt"], "wt") ;
fdisp(mid, "id,var,varname,lon,lat,alt,undef") ;
for jv = 1:length(V)
   v = V{jv} ; vn = VN{jv} ;

   ## write *.csv
   n = eval(["columns(" v ".x)"]) ;
   fid = fopen(ofile = [XDS v ".csv"], "wt") ;
   fmt = ["Y,M,D" repmat(",%s", 1, n) "\n"] ;
   eval(["fprintf(fid, fmt, " v ".ids{:}) ;"]) ;
   fmt = ["%d,%d,%d" repmat(",%.4g", 1, n) "\n"] ;
   eval(["fprintf(fid, fmt, [" v ".id nan2lim(" v ".x)]') ;"]) ;
   fclose(fid) ;

   ## meta.txt
   nr = eval(["columns(" v ".x) ;"]) ;
   for i=1:nr
      eval(["fprintf(mid, \"%s,%s,%s,%.2f,%.2f,%.1f,%.1f\\n\", " v ".ids{i}, v, vn, " v ".lon(i), " v ".lat(i), " v ".alt(i), lim) ;"]) ;
   endfor

endfor
fclose(mid) ;

## write ECMWF fields
addpath ~/xds/util
for d = glob("/home/gerd/xds/data/SHIVA/ei/[0-9][0-9][0-9][0-9]")'
   nc2csv(d{:}) ;
endfor

for d = glob("/home/gerd/xds/data/SHIVA/mmsf/[0-9][0-9][0-9][0-9]")'
   nc2csv(d{:}) ;
endfor
