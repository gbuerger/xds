## usage: s = sdly (Lsdly=0)
##
##
function s = sdly (Lsdly=0)

   if ~Lsdly, return ; endif
   
   global pT lres vres BLD ML MERGE
   addpath ~/xds/extra/rcm
   
   lres = 60 * 24 ; # original time resolution (in minutes)
   hres = 10 ; # intended time resolution
   vres = 0.00003 ; # for EGLV
   ML = true ;
   MERGE = true ;
   if isempty(BLD) BLD = false ; endif
   
   fitfun = @(t, q) expfit_(t, q) ;
   
   if ~isempty(f = dbstack)
      fname = f(1).name ;
      printf("\n\t###   %s   ###\n\n", fname) ;
   else
      fname = "sdly" ;
   endif

   global PAR PRJ GVAR ADIR SDIR CDIR BATCH Judf
   
   idx.type = "()" ;

   per = [2010 1 1 ; 2010 12 31] ;
   cur.per = [1970 2000] ; fut.per = [2070 2100] ;
   fut.per = [-Inf Inf] ;
   
   ldata = fullfile("data", PRJ, PAR.pdd) ;
   gdata = fullfile("data", PRJ, "ptr") ;
   pmkdir(pdir = fullfile(SDIR, PAR.pdd, "sdly")) ;
   if (stat(pdir).size ~= 4096 && !L), return ; endif

   METAFILE = fullfile(ldata, "meta.txt") ;

   [ids, vars, names, lons, lats, alts, undefs, J] = meta(METAFILE) ;
   VARS = sunique(vars) ;

   JV = [ find(strncmp(VARS, "T", 1))' find(strncmp(VARS, "P", 1))'] ;
   for jv=JV

      if !any(J{jv}), continue ; endif
      v = vars{J{jv}(1)} ; vn = names{J{jv}(1)} ; vv = strrep(v, ".", "_") ;

      printf("sdly: <-- %s\n", ifile = fullfile(ldata, [v ".csv"])) ;
      obs.(vn) = read_stat(ifile) ;

      ## select ids from meta
      [Jm Js Km Ks] = unify(ids(J{jv})', obs.(vn).ids) ;

      if 1
	 obs.(vn).ids = ids(J{jv})(Jm)' ;
	 obs.(vn).lons = lons(J{jv})(Jm)' ;
	 obs.(vn).lats = lats(J{jv})(Jm)' ;
	 obs.(vn).alts = alts(J{jv})(Jm)' ;
	 obs.(vn).udf = undefs(J{jv})(Jm)' ;
	 obs.(vn).x = obs.(vn).x(:,Ks) ; obs.(vn).x = lim2nan(obs.(vn).x, obs.(vn).udf) ;

	 printf("sdly: <-- %s\n", ifile = fullfile(ADIR, PAR.pdd, [vv ".csv"])) ;
	 ana.(vn) = read_stat(ifile) ;
	 ana.(vn).x = lim2nan(ana.(vn).x, obs.(vn).udf) ;
	 ana.(vn).lons = lons(J{jv})(Jm)' ;
	 ana.(vn).lats = lats(J{jv})(Jm)' ;

	 ifile = glob([fullfile(CDIR, PAR.pdd) "/" vv "*.csv"]){1} ;
	 printf("sdly: <-- %s\n", ifile) ;
	 sim2.(vn) = read_stat(ifile) ;
	 sim2.(vn).x = lim2nan(sim2.(vn).x, obs.(vn).udf) ;
	 sim2.(vn).lons = lons(J{jv})(Jm)' ;
	 sim2.(vn).lats = lats(J{jv})(Jm)' ;
      endif

      if ~isempty(ifile = glob([fullfile(SDIR, PAR.pdd) "/plt/" vv "*.csv"]))
	 ifile = ifile{1} ;
      elseif ~isempty(ifile = glob([fullfile(SDIR, PAR.pdd) "/" vv "*.csv"]))
	 ifile = ifile{1} ;
      else
	 ifile = glob([fullfile(CDIR, PAR.pdd) "/" vv "*.csv"]){1} ;
      endif
      printf("sdly: <-- %s\n", ifile) ;
      sim1.(vn) = read_stat(ifile) ;
      sim1.(vn).x = lim2nan(sim1.(vn).x, undefs(J{jv})(Jm)') ;
      sim1.(vn).lons = lons(J{jv})(Jm)' ;
      sim1.(vn).lats = lats(J{jv})(Jm)' ;

      if 0 && PAR.mskobs
	 idx.subs = repmat({":"}, 1, columns(obs.(vn).x)) ;
	 [obs.(vn).I ana.(vn).I sim2.(vn).I] = common(obs.(vn).id, ana.(vn).id, sim2.(vn).id) ;
	 I = isnan(obs.(vn).x(obs.(vn).I,:)) ;
	 w = (ana.(vn).x(ana.(vn).I,:)) ; w(I) = NaN ; ana.(vn).x(ana.(vn).I,:) = w ;
	 w = (sim2.(vn).x(sim2.(vn).I,:)) ; w(I) = NaN ; sim2.(vn).x(sim2.(vn).I,:) = w ;
      endif

   endfor

   load("~/xds/rand_init.ob") ; rand("state", v) ;
   
   for S = {"obs" "ana" "sim1" "sim2"}

      S = S{:} ;
      eval(sprintf("T = %s.temperature ;", S)) ;
      eval(sprintf("wP = %s.precipitation ;", S)) ;
      T.ll = cell2mat(arrayfun(@(x,y) [x ; y], T.lons, T.lats, "UniformOutput", false)) ;
      wP.ll = cell2mat(arrayfun(@(x,y) [x ; y], wP.lons, wP.lats, "UniformOutput", false)) ;

      [IT IP] = common(T.id, wP.id) ;
      T = selper(T, IT) ;
      wP = selper(wP, IP) ;
      T.dfd = sum(!isnan(T.x)) / rows(T.x) ;
      for j = 1:length(wP.lons)
      	 wP.T(j) = findll(wP.ll(:,j), T.ll, T.dfd) ;
      endfor

      for MC = {"MC" "MCp"}

	 MC = MC{:} ;
	 if strcmp(MC, "MC")
	    sfx = "" ;
	 else
	    sfx = ".2" ;
	 endif

	 if exist(ofile = sprintf("%s/%s.%s%s.T-P.ob", pdir, S, MC, sfx), "file") == 2 && ~BLD
	    continue ;
	 endif

	 pfile = sprintf("data/%s/%s.dapar%s.ot", PRJ, MC, sfx) ;
	 printf("<-- %s\n", pfile) ;
	 load(pfile) ;

	 [P.id P.x] = disagg(wP, T, lres, hres, dapar) ;
	 P.xc = MCbc(P.x, bcp) ;

	 P.ll = wP.ll ;
	 P.ids = ids(J{JV(2)}) ;
	 P.vars = vars(J{JV(2)}) ;

	 printf("sdly: --> %s\n", ofile) ;
	 save(ofile, "T", "P") ;

      endfor

   endfor

endfunction
