function pmkdir (varargin)

   ## usage:  pmkdir (D)
   ##
   ## make directory with parents

   for v=varargin
      D = v{:} ;
      sd = "" ;
      for d=strsplit(D, "/")
	 sd = [sd d{:} "/"] ;
	 mkdir(sd) ;
      endfor 
   endfor 

endfunction
