function y = prb (fun, x, p, q, Pp)

   ## usage:  y = prb (fun, x, p, q, Pp)
   ##
   ## probit transform using fun to fit cdf

   if (strcmp(fun, "gam") && any(p<=0))
      y = inf(size(x)) ;
      return ;
   endif

   ## FIXME introduced with CS 24011abcd6a30b14249b5eaac6ddcc6167eec64c
   if false
      p = [p polyval(Pp, p)] ;
   else
      x0 = Pp ;
      x = (x - x0) / p(1) ;
      p = p(2:end) ;
   endif
   p = mat2cell(p(:)', 1, ones(1, length(p))) ;

   y = feval([fun "cdf"], x, p{:}) ;
   y = (1-q) * y + q ;
   y = norminv(y) ;
   
endfunction
