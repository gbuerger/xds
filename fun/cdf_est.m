function varargout = cdf_est (x)

   ## usage:  varargout = cdf_est (x)
   ##
   ## estimate cdf of x, possibly returning the sorting

   [n m] = size(x) ;
   [s, I] = sort(x) ;
#   F = (0:n-1)' / n ;
#   F = (1:n)' / n ;
   # f = 1/n + ((1:n)'-1)*(n-2)/n/(n-1) ;
   f = (1:n)' / (n+1) ;
   F = repmat(f, 1, m) ;
   F(I) = F ;

   if (nargout < 1)
      plot(x(I), F(I)) ;
   else
      varargout{1} = F ;
      if nargout > 1
	 varargout{2} = I ;
      endif
   endif

endfunction
