function varargout = linear (x, par)

   ## usage: varargout = linear (x, par)
   ## 
   ## adjust mean and variance of x

   if nargin > 1
      m = par{1} ; s = par{2} ;

      y = bsxfun(@minus, x, m) ;
      y = bsxfun(@rdivide, y, s) ;

      varargout{1} = y ;
   else
      [w m s] = anom(x) ;
      par = {m, s} ;

      y = bsxfun(@minus, x, m) ;
      y = bsxfun(@rdivide, y, s) ;

      varargout{1} = par ;
      varargout{2} = y ;
   endif
	 
endfunction
