function [r0, r1] = rotfind (ll0, ll1, lon, lat)

   ## usage: [r0, r1] = rotfind (ll0, ll1, lon, lat)
   ## 
   ## find rotated grid section

   I = lon >= ll0(1) & lon <= ll1(1) & lat >= ll0(2) & lat <= ll1(2) ;
   [i j] = find(I) ;
   [r0(1) r0(2)] = deal(min(i), min(j)) ;
   [r1(1) r1(2)] = deal(max(i), max(j)) ;
	 
endfunction
