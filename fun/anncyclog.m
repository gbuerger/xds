function varargout = anncyclog (s, ps, pp)

   ## usage:  varargout = anncyclog (s, ps, pp)
   ##
   ## remove annual cycle from log of data, and normalize

   global PAR

   if !isfield(s, "cal"), s.cal = "gregorian" ; endif 
   t = date2toy(s.id, s.cal) ;

   r = s ;

   r.x(abs(r.x) < 1e-6 | r.x < 0) = 0 ; # some stupid nc values

   if 0
      ## remove annual cycle (multiplicative way)
      r = mcyc(r) ;
   else
      if PAR.Plog
	 r.x = log(r.x) ;
      endif
      r.x(I = isinf(r.x)) = NaN ;
      if (nargin < 2)
	 ps = anncyc(r) ;
      endif

      r.a = r.x - PAR.acyc(t) * ps(1:PAR.numhar,:) ;
      if regexp(PAR.anc, "m")
	 r.a = r.a ./ PAR.scyc.fun(t,ps(PAR.numhar+1:end,:)) ;
      endif
      r.a = r.a + ps(1,:) ;

      if PAR.Plog
	 r.a = exp(r.a) ;
      endif
      r.a(I) = 0 ;
   endif
   
   if nargin > 2
      r.z = normprc(r.a, pp) ;
   else
      [pp, r.z] = normprc(r.a) ;
   endif

   if (nargin == 1)
      if (nargout > 1)
	 varargout{1} = ps ;
	 varargout{2} = pp ;
	 if (nargout > 2)
	    varargout{3} = r ;
	 endif 
      endif 
   else
      varargout{1} = r ;
   endif 

endfunction
