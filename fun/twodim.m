function [N, y] = twodim (x)

   ## usage:  [N, y] = twodim (x)
   ##
   ## two-dimensinalize x

   N = size(x) ;
   y = reshape(x, N(1), prod(N(2:end))) ;

endfunction
