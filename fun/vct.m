## usage: y = vct (x, nvd, srt)
##
## vectorize lon-lat grid
function y = vct (x, nvd, srt)

   if nargin > 2
      nvd = nvd(srt) ;
   endif

   for v = {"tim" "dat" "lon", "lat" "ref"}
      j.(v{:}) = find(strncmp({nvd.Name}, v{:}, 3)) ;
   endfor

   switch [~isempty(j.dat) ~isempty(j.tim) ~isempty(j.ref)]
      case [1 1 0]
	 p(1:4) = [j.dat j.tim j.lon j.lat] ;
	 ns = 2 ;
      case [0 1 1]
	 p(1:4) = [j.tim j.ref j.lon j.lat] ;
	 ns = 0 ;   # CFSR: don't do anything
      case [1 0 0]
	 p(1:3) = [j.dat j.lon.j.lat] ;
	 ns = 1 ;
      case [0 1 0]
	 p(1:3) = [j.tim j.lon j.lat] ;
	 ns = 1 ;
      otherwise
	 ns = 0 ;
   endswitch
   
   p = [p setdiff(1:length(nvd), p)] ;
   x = permute(x, p) ;
   N = size(x) ;

##   J = find(p == j.lon | p == j.lat) ;
##   N = [N(1) prod(N(J)) N(J(2)+1:end)] ;
   
   if ns > 0
      y = reshape(x, [prod(N(1:ns)) prod(N(ns+1:ns+2)) N(ns+3:end)]) ;
   else
      y = reshape(x, [N(1:2) prod(N(3:end))]) ;
   endif

   y = squeeze(y) ;
   
endfunction
