function y=gevinv(x,xi,mu,sigma),
%inverse CDF for GEV
%
%     USAGE: y = gevinv(x,xi,mu,beta)
%
%         x: Quantile
%xi,mu,beta: Parameters
%         y: Cumulative probability

   y = mu + sigma/xi * (log(1./x).^(-xi) - 1) ;

endfunction
