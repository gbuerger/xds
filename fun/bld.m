function bld (BLD)

   ## usage: bld (BLD)
   ## FIXME: decide whether to use PAR.bld or BLD
   ## rebuild (binary) project files

   global BATCH PAR PRJ

   gdata = fullfile("data", PRJ, PAR.ptr) ;

   switch BLD
      case "PRJ"
	 printf("init: Rebuilding entire project %s.\n", PRJ) ;
	 ## cellfun(@(f) unlink(f), rglob(fullfile("data", PRJ, PAR.pdd, "**/*.ob"))) ;
      case {"ALL" true 1}
	 printf("init: Rebuilding\n%s\n%s\n%s\n", PAR.ana, PAR.sim{2}, PAR.sim{1}) ;
	 ## cellfun(@(f) unlink(f), glob(fullfile(gdata, PAR.ana, PAR.res, "*.ob"))) ;
	 cellfun(@(f) unlink(f), glob(fullfile(gdata, PAR.ana, "*.ob"))) ;
	 cellfun(@(f) unlink(f), glob(fullfile(gdata, PAR.sim{1}, "*.ob"))) ;
	 cellfun(@(f) unlink(f), glob(fullfile(gdata, PAR.sim{2}, "*.ob"))) ;
	 cellfun(@(f) unlink(f), glob(fullfile(gdata, PAR.sim{2}, "clim/*.ob"))) ;
	 cellfun(@(f) unlink(f), glob(fullfile(gdata, PAR.sim{2}, "eof/*.ob"))) ;
	 cellfun(@(f) unlink(f), glob(fullfile(gdata, PAR.sim{2}, PAR.pdd, "/*.ob"))) ;
      case "SIM"
	 printf("init: Rebuilding\n%s\n%s\n", PAR.sim{2}, PAR.sim{1}) ;
	 cellfun(@(f) unlink(f), glob(fullfile(gdata, PAR.sim{1}, "*.ob"))) ;
	 cellfun(@(f) unlink(f), glob(fullfile(gdata, PAR.sim{2}, "*.ob"))) ;
	 cellfun(@(f) unlink(f), glob(fullfile(gdata, PAR.sim{2}, "clim/*.ob"))) ;
	 cellfun(@(f) unlink(f), glob(fullfile(gdata, PAR.sim{2}, "eof/*.ob"))) ;
	 cellfun(@(f) unlink(f), glob(fullfile(gdata, PAR.sim{2}, PAR.pdd, "/*.ob"))) ;
   endswitch

   if BATCH
      pause(10) ;
   else
      a = tolower(input("Continue? [y/N]> ", "s")) ;
      if !strcmp(a, "y"), exit ; endif
   endif

endfunction
