function y = EXP_inv (x, p)

   ## usage:  y = EXP_inv (x, p)
   ##
   ## inverse of probit fit function

   y = p(1) + (exp(x/p(3)) - 1) * p(2) ;
   y = max(0, y) ;

endfunction
