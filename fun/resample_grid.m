## usage: zi = resample_grid (x, y, z, xi, yi, isP = false)
##
## resample grid
function zi = resample_grid (x, y, z, xi, yi, isP = false)

   global PAR

   [y x] = meshgrid(y, x) ;
   [yi xi] = meshgrid(yi, xi) ;
   Ni = size(xi) ;
   
   if ndims(z) < 3
      z = adddim(z) ;
   endif

   d = sqrt((x(:)-xi(:)').^2 + (y(:)-yi(:)').^2) ;
   d = d ./ mean(d(:)) ;  # do it scale free
   
   if isP	    # nearest neighbor
      dm = min(d) ;
      w = (d == dm) ;
   else
      w = 1 ./ d.^3 ;
   endif
   w = w ./ sum(w) ;
   
   N = size(z) ;
   z = reshape(z, N(1), N(2)*N(3), N(4:end)) ;

   z(isnan(z)) = 0 ;

   zi = mult(z, w, 2, 1) ;

   if ndims(zi) > 2
      w = p = 1:ndims(zi) ; p(2) = w(3) ; p(3) = w(2) ;
      zi = permute(zi, p) ;
   endif
   zi = reshape(zi, N(1), Ni(1), Ni(2), N(4:end)) ;
   
endfunction


## usage: y = adddim (x, d)
##
##
function y = adddim (x, d)
   if nargin < 2
      d = 1 ;
   endif

   n = ndims(x) ;
   p = 1:n ;
   p = [p(1:d-1) n+1 p(d:n)] ;
   y = permute(x, p) ;

endfunction
