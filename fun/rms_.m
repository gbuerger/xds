## usage: y = rms_ (x)
##
## calculate rms
function y = rms_ (x)
   I = ~isnan(x) ;
   x = x(I) ;
   n = length(x) ;
   y = sqrt(sumsq(x) / n) ;
endfunction
