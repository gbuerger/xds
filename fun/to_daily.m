function v = to_daily (u, fun=@nanmean)

   ## usage: v = to_daily (u, fun=@nanmean)
   ## 
   ## transform to daily

   if !isfield(u, "id")
      for [vu ku] = u
	  v.(ku) = to_daily (vu, fun) ;
      endfor
      return ;
   endif

   v = u ;
   if !all(diff(datenum(v.id)) == 1) && length(unique(v.id(:,4))) > 1
      if !isempty(strfind(v.name, "precip"))
	 [v.id, v.x] = dayavg(v.id, v.x, 24, fun) ;
      else
	 [v.id, v.x] = dayavg(v.id, v.x, 0, fun) ;
      endif
   endif
   v = fill_date(v) ; # fix broken scenarios

endfunction
