## usage: id_ref = id_extr (ncfile)
##
## extract reftime from filename
function id_ref = id_extr (ncfile)

   F = strsplit(ncfile, "/") ;
   ncfile = fullfile(F{end-1:end}) ;
   tstr = ncfile(regexp(ncfile, "\\d")) ;
   n = length(tstr) ;

   switch n
      case 6
	 tstr = [tstr "01"] ;
      case num2cell(12:30)
	 tstr = [ncfile(end-6:end-3) "0101"] ;
      otherwise
	 error("xds:xds", "app: cannot read nc filename: %s", ncfile) ;
   endswitch

   id_ref = cell(1,3) ;
   [id_ref{:}] = sscanf(tstr, "%4d%2d%2d", "POSIX") ;
   ##id_ref{4} = 12 ;
   
   id_ref = cell2mat(id_ref) ;
   
endfunction
