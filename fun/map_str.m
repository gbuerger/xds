## usage: J = map_str (s1, s2)
##
##
function [J J2] = map_str (s1, s2)

   [~, j1, j2] = setxor(s1, s2) ;

   if (n = length(j2)) > 0
      fmt = repmat(" %s", 1, n) ;
      error(["map_str: not in s1:\t" fmt "\n"], s2{j2}) ;
   endif

   ss1 = [s1 s2(j2)] ;

   [~, J1] = sort(ss1) ;
   [~, J2] = sort(s2) ;
   
   J2(J2) = 1:length(J2) ;
   J = J1(J2) ;

   if ~isempty(j2)
      T = arrayfun(@(j) find(strcmp(s2{j}, ss1(J))), j2) ;
      JJ = and(arrayfun(@(k) 1:length(J) ~= k, T, "UniformOutput", false){:}) ;   
      J = J(JJ) ;

      n2 = length(s2) ;
      J2 = and(arrayfun(@(k) 1:n2 ~= k, j2, "UniformOutput", false){:}) ;
   else
      J2 = true(1, length(s2)) ;
   endif

endfunction
