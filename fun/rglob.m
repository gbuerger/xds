function list = rglob (pat)

   ## usage: list = rglob (pat)
   ## 
   ## list glob pattern recursivley

   g = "/**/" ; n = length(g) ;
 
   if isempty(i = strfind(pat, g))
      list = glob(pat) ;
      return ;
   endif

   pfx = pat(1:i+1) ; sfx = pat(i+n:end) ;

   list = glob([pfx(1:end-1) sfx]) ;

   for d = glob(pfx)'
       d = d{:} ;
       if isdir(d)
	  list = union(list, rglob([d g sfx])) ;
       endif
   endfor

endfunction
