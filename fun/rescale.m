function S = rescale (s, z)

   ## usage:  S = rescale (s, z)
   ##
   ## rescale N(0,1) var. s.z to phys. units x, using PAR

   global PRJ PAR clim

   if isempty(clim)
      load(fullfile("data", PRJ, PAR.pdd, "clim.ob")) ;
   endif
   ssnrad = 2*pi*PAR.ssnwin/365 ;

   S = s ;

   id = s.id ; z = s.(z) ;

   N = size(z) ;
   x = nan(N) ;

   if ~isempty(PAR.anc)
      [t It Jt] = date2toy(id, s.cal) ;
      x_h = [ones(rows(t),1) cos(t) sin(t) cos(2*t) sin(2*t)] ;
   endif

   VAR = {} ; for i=1:N(2), VAR = union(VAR, clim.vars(i)) ; endfor 
   for v = VAR

      v = v{:} ;
      J = strcmp(clim.vars, v) ;

      if isempty(PAR.anc)
	 eval(["x(:,J) = anom_inv(z(:,J), clim." v ".sm, clim." v ".ss) ;"]) ;
	 if all(JJ = clim.ispos(J))
	    x(:,J) = max(0, x(:,J)) ;
	 endif
	 continue
      endif

      if all(JJ = clim.ispos(J))

	 ## rescale via probit
	 if exist([PAR.cdffit "cdf"]) == 2 && exist([PAR.cdffit "inv"]) == 2
	    if PAR.waitbar, hw = waitbar(0, "looping through P") ; endif
	    for j=find(JJ)
	       jj = find(J)(j) ;
	       if ssnrad < 2*pi
		  q = ppval(clim.(v).pp(j).q, t) ;
		  p1 = ppval(clim.(v).pp(j).p(1), t) ;
		  p2 = ppval(clim.(v).pp(j).p(2), t) ;
		  x0 = ppval(clim.(v).pp(j).x0, t) ;
	       else
		  q = clim.(v).pp(j).q ;
		  p1 = clim.(v).pp(j).p(1) ;
		  p2 = clim.(v).pp(j).p(2) ;
		  x0 = clim.(v).pp(j).x0 ;
	       endif
	       x(:,jj) = parfun(@(_x,_p1,_p2,_q,_x0) prb_inv(PAR.cdffit, _x, [_p1 _p2], _q, _x0), z(:,jj), p1, p2, q, x0) ;
	       if ~isnan(PAR.ssnwin) & ~isempty(clim.(v).pp(j).IZ)
		  x(~clim.(v).pp(j).IZ(Jt),jj) = 0 ;
	       endif
	       if PAR.waitbar, waitbar(j/sum(JJ), hw) ; endif
	    endfor
	    if PAR.waitbar, close(hw) ; endif
	 else
	    F = [PAR.cdffit "_inv"] ;
	    for j=find(JJ)
	       jj = find(J)(j) ;
	       q = clim.(v).pp(j).q ;
	       x0 = clim.(v).pp(j).x0 ;
	       p = clim.(v).pp(j).p ;
	       x(:,jj) = feval(F, z(:,jj), p, q, x0) ;
	    endfor
	 endif
	 ww = x(:,clim.ispos) ;
	 ww(ww<0) = 0 ;
	 x(:,clim.ispos) = ww ;

	 if isnan(PAR.ssnwin)
	    if PAR.Plog
               x(:,J) = log(x(:,J)) ;
            endif
            x(:,J) = x(:,J) - repmat(clim.(v).ps(1,:), rows(x), 1) ;
	    if regexp(PAR.anc, "m")
               ps2 = clim.(v).ps(PAR.numhar+1:end,:) ;
               x(:,J) = x(:,J) .* PAR.scyc.fun(t,ps2) ;
	    endif
            x(:,J) = x(:,J) + x_h(:,1:PAR.numhar) * clim.(v).ps(1:PAR.numhar,:) ;
            if PAR.Plog
               x(:,J) = exp(x(:,J)) ;
            endif
	 endif
	 
      else

	 x(:,J) = prbfit(z(:,J), clim.(v).ppinv) ;
	 if regexp(PAR.anc, "m")
	    ps2 = clim.(v).ps(PAR.numhar+1:end,:) ;
	    x(:,J) = x(:,J) .* PAR.scyc.fun(t,ps2) ;
	 endif
	 x(:,J) = x(:,J) + x_h * clim.(v).ps(1:PAR.numhar,:) ;

      endif

   endfor

   x(isinf(x)) = nan ;

   S.x = x ;
   for k = {"vars" "ids" "names" "lons" "lats" "alts" "udf"}
      if ~isfield(clim, k{:}) continue ; endif
      S.(k{:}) = clim.(k{:}) ;
   endfor

endfunction


function y = anom_inv (x, xm, xs)

   ## usage:  y = anom_inv(x, xm, xs)
   ## inverse of anom

   N = size(x) ;
   x = reshape(x, N(1), prod(N(2:end))) ;
   [nr nc] = size(x) ;

   y = x ;

   if (nargin > 2)
      y = y .* repmat(xs, nr, 1) ;
   endif 

   y = y + repmat(xm, nr, 1) ;

   y = reshape(y, N) ;

endfunction
