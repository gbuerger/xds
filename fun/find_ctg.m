## usage: J = find_ctg (L, qctg = 0.9)
##
## find most contiguous chunk of data
function J = find_ctg (L, qctg = 0.9)

   [nr nc] = size(L) ;

   J = false(1, nc) ;
   I = wI = true(1, nr) ;
   p = [] ;
   s = [] ;
   while ~all(J) && sum(I)/sum(wI) > qctg
      wI = I ;
      [~, jj] = max(sum(L(I,~J), 1)) ;
      j = find(~J)(jj) ;
      J(j) = true ;
      I = all(L(:,J), 2) ;
      p = [p j] ;
      s = [s sum(I)] ;
   endwhile

   J = ind2log(p, nc) ;
   
endfunction
