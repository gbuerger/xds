function res = ssnprb (id, x, pp)

   ## usage:  res = ssnprb (id, x, pp)
   ##
   ## seasonal probit

   pkg load signal statistics
   persistent N0 nfun hw ;
   global PAR

   init_rand ;

   ssnrad = 2*pi*PAR.ssnwin/365 ;
   Qzero = 0.1:0.01:0.9 ;
   
   if isempty(nfun)
      pkg load splines signal
      N0 = prod(size(x)(2:end)) ;
   endif

   f = dbstack ;
   x = squeeze(x) ;
   N = size(x) ;

   if N(2) > 1
      
      nfun = 0 ;
      I{1} = N(1) ;
      for j=2:length(N)
	 I{j} = ones(N(j),1) ;
      endfor
      C = mat2cell(x, I{:}) ;
      if nargin < 3
	 if 0
	    if PAR.waitbar, hw = waitbar(nfun/N0) ; endif
	    for j = 1:length(C)
	       disp(j) ;
	       res{j} = ssnprb(id, C{j}) ;
	    endfor
	    if PAR.waitbar, close(hw) ; endif
	 else
	    res = parfun(@(x) ssnprb(id,x), C, "UniformOutput", false, "ErrorHandler", @ssnprb_err) ;
	 endif
      else
	 PP = num2cell(pp) ;
	 if length(N) > 2
	    PP = repmat(PP, 1, 1, N(3)) ;
	 endif
	 res = parfun(@(x,pp) ssnprb(id,x,pp), C, PP, "UniformOutput", false, "ErrorHandler", @ssnprb_err) ;
      endif
      res = cell2mat(res) ;
      if isnumeric(res) && sum(isnan(res(:))) > sum(isnan(x(:)))
	 warning("xds:xds", ["ssnprb: %7.1f%% NaNs created\n"], 100*(sum(isnan(res(:)))/sum(isnan(x(:))) - 1)) ;
      endif
      nfun = 0 ;
      return ;
      
   endif

   if isfigure(hw)
      waitbar(++nfun/N0, hw, sprintf("%d --> %d", nfun, N0)) ;
   endif
   z = nan(N) ;
   
   Idfd = !isnan(x) ;
   if !any(Idfd)
      warning("ssnprb: all values undefined.\n") ;
      if nargin < 3
	 pp.p = [NaN NaN] ; pp.q = pp.qz = pp.IZ = pp.x0 = NaN ;
	 res = pp ;
      else
	 res = nan(size(x)) ;
      endif
      return ;
   endif

   ## from here per t
   [t,I,J] = date2toy(id) ;
   nr = rows(x) ;

   if nargin < 3

      ## find zero (uff!!)
      [~,j] = max(diff(log(quantile(x, Qzero)))) ;
      pp.qz = max(0, quantile(x, Qzero(j))) ;
      
      ssnwin = @(x,y) abs(x-y) < ssnrad ;
      if ssnrad < 2*pi
	 X = arrayfun(@(i) x(ssnwin(t,t(i))), I, "UniformOutput", false) ;
      else
	 X = arrayfun(@(i) x(ssnwin(t,t(i))), I(1), "UniformOutput", false) ;
      endif
      II = cellfun(@(x) x > pp.qz, X, "UniformOutput", false) ;
      X = cellfun(@(x,i) x(i), X, II, "UniformOutput", false) ;
      Q = cellfun(@(i) sum(~i)/length(i), II, "UniformOutput", false) ;
      X0 = cellfun(@(x) min(x), X, "UniformOutput", false) ;
      ## handle days with no rain at all
      Iany = cellfun(@(i) any(i), II) ;
      X0(~Iany) = min(x(x > pp.qz)) ;
      
      ## generate reasonable random values for empty cases
      if ssnrad < 2*pi
	 IZ = cell2mat(cellfun(@(x) numel(x) > PAR.ssnNX, X, "UniformOutput", false)) ;
	 [bw.b bw.a] = butter(3, 1/30) ;
	 qf = filtfilt(bw.b, bw.a, wq=cell2mat(Q)) ;
	 qf(qf>=1) = max(wq(wq<1)) ;
	 x0f = filtfilt(bw.b, bw.a, wq=cell2mat(X0)) ;
	 xf(IZ) = cellfun(@(x) mean(x), X(IZ), "UniformOutput", false) ;
	 xf(~IZ) = mean(x(x>pp.qz)) ;
	 xf = filtfilt(bw.b, bw.a, cell2mat(xf))' ;
	 X(~IZ) = arrayfun(@(x) wblgen(x), xf(~IZ), "UniformOutput", false, "ErrorHandler", @ErrFun) ;
	 Q(~IZ) = arrayfun(@(q) q, qf(~IZ), "UniformOutput", false, "ErrorHandler", @ErrFun) ;
	 X0(~IZ) = arrayfun(@(x0) x0, x0f(~IZ), "UniformOutput", false, "ErrorHandler", @ErrFun) ;
      endif
      
      [F, IF] = cellfun(@(x) cdf_est(x), X, "UniformOutput", false) ;
      X = cellfun(@(x,If) x(If), X, IF, "UniformOutput", false) ;
      F = cellfun(@(f,If) f(If), F, IF, "UniformOutput", false) ;

      Z = cellfun(@(f,q) abs((1-q) * f + q), F, Q, "UniformOutput", false) ;
      Z = cellfun(@(f) norminv(f), Z, "UniformOutput", false) ;
      XZ = cellfun(@(x,z) [x z], X, Z, "UniformOutput", false) ;

      ## initial guess P0 for P
      switch PAR.cdffit
	 case "wbl"
	    P0 = cellfun(@(x, F_) fitwbl(x, F_), X, F, "UniformOutput", false) ;
	 case "0wbl"
	    Xm = cellfun(@(x) median(x(:,1)), Z, "UniformOutput", false) ;
	    Xl = cellfun(@(x) max(x(:,1)), Z, "UniformOutput", false) ;
	    K = cellfun(@(xm,xl) log(log(2))/(log(xm)-log(xl)), Xm, Xl, "UniformOutput", false) ;
	    P0 = cellfun(@(xl,k) [xl k], Xl, K, "UniformOutput", false) ;
	 case "1wbl"
	    options.method = "mps" ;
	    P0 = parfun(@(x) fwbl(x, options), X, "UniformOutput", false, "ErrorHandler", @fwblErr) ;
	 otherwise
	    P0 = cellfun(@(x) [0.01 mean(x(:,1)) std(x(:,1))], Z, "UniformOutput", false) ;
      endswitch

      ## fit P dependence on ssn
      fitprb = "fmin" ;
      switch fitprb
	 case "xy"
	    [~, P, CVG] = parfun(@(x,y,p,q) xyfit(x, y, p, q), X, Z, P0, Q, "UniformOutput", false, "ErrorHandler", @xyfitErr) ;
	    if any(IV = (cell2mat(CVG) == 0))
	       pp.p = pp.q = pp.qz = pp.IZ = NaN ;
	       warning("xds:xds", "ssnprb: no convergence at %d", find(IV)(1)) ;
	    else
	       P = cell2mat(P) ; Q = cell2mat(Q) ;
	       pp.p = csaps(t(I), P, PAR.ssnP) ;
	       pp.q = csaps(t(I), Q, PAR.ssnP) ;
	       pp.IZ = IZ ;
	    endif
	 case "fmin"
	    opt = optimset("TolX", 1e-10, "TolFun", 1e-10, "MaxFunEvals", 1000, "AutoScaling", "on") ;
	    [P V] = parfun(@(x,q,p0,x0) fmin_ (@(p) cost(p, x, q, x0), p0, opt), XZ, Q, P0, X0, "UniformOutput", false) ;
	    if any(IV = isinf(cell2mat(V))) && PAR.dbg
	       warning("xds:xds", "ssnprb: no convergence in %d case(s)", sum(IV)) ;
	    endif
	    P = cell2mat(P) ; Q = cell2mat(Q) ; X0 = cell2mat(X0) ;
	    P(IV,:) = NaN ; Q(IV) = NaN ;
	    if ssnrad < 2*pi
	       pp.p(1) = csaps(t(I)(~IV), P(~IV,1), PAR.ssnP) ;
	       pp.p(2) = csaps(t(I)(~IV), P(~IV,2), PAR.ssnP) ;
	       pp.q = csaps(t(I)(~IV), Q(~IV), PAR.ssnP) ;
	       pp.x0 = csaps(t(I)(~IV), X0(~IV), PAR.ssnP) ;
	       pp.IZ = IZ ;
	    else
	       pp.p = P ;
	       pp.q = Q ;
	       pp.x0 = X0 ;
	       pp.IZ = [] ;
	    endif
      endswitch
      
      res = pp ;
      return ;

   endif

   if ssnrad < 2*pi
      q.pp = ppval(pp.q, t(I)) ;
      q.pp = max(q.pp, 0) ;
      z = trunc_rnd("norm", length(J), -Inf, norminv(q.pp(J))) ;
   else
      q.pp = pp.q ;
      z = trunc_rnd("norm", length(J), -Inf, norminv(q.pp)) ;
   endif

   Ix = (x > pp.qz) ;
   if PAR.prbraw

      z(Ix) = F ;

   else

      if ssnrad < 2*pi
	 p1.pp = ppval(pp.p(1), t(Ix)) ;
	 p2.pp = ppval(pp.p(2), t(Ix)) ;
	 q.pp = ppval(pp.q, t(Ix)) ;
	 x0.pp = ppval(pp.x0, t(Ix)) ;
      else
	 p1.pp = pp.p(:,1) ;
	 p2.pp = pp.p(:,2) ;
	 q.pp = pp.q ;
	 x0.pp = pp.x0 ;
      endif
      z(Ix) = arrayfun(@(_x,_p1,_p2,_q,_x0) prb(PAR.cdffit, _x, [_p1 _p2], _q, _x0), x(Ix), p1.pp, p2.pp, q.pp, x0.pp) ;

   endif

   if ~isreal(z(Ix))
      error("ssnprb: complex zscores") ;
   endif
   if any(isinf(z))
      Iz = isinf(z) ;
      if length(f) < 3
	 warning("xds:xds", ["ssnprb: %d infinite values\n"], sum(Iz(:))) ;
      endif
      z(Iz) = NaN ;
   endif

   res = z ;

endfunction


## usage: p = fwbl (X, options)
##
## fit Weibull parameters
function p = fwbl (X, options)
   phat = fitweib(X, options) ;
   p = phat.params(1:2) ;
endfunction

function p = fwblErr (s, X, options)
   y = NaN ;
   disp(s) ;
endfunction

function y = wblgen (x)
   n = 100 ;
   y = wblrnd(x, 1, n, 1) ;
endfunction

function y = ErrFun (s, q, x)
   y = NaN ;
   disp(s) ;
endfunction

function y = ssnprb_err (s, id, x, pp)
   y = NaN ;
   disp(s) ;
endfunction

function y = xyfitErr (s, x, f, p, q)
   y = NaN ;
   disp(s) ;
endfunction

## usage: [P, V] = fmin_ (fun, p0, opt)
##
## wrapper for fminunc or fminsearch
function [P, V] = fmin_ (fun, p0, opt)
   
   try
      [P V] = fminunc (fun, p0, opt) ;
   catch
      [P V] = fminsearch (fun, p0, opt) ;
   end_try_catch

endfunction
