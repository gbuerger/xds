function v = common_def (u)

   ## usage: v = common_def (u)
   ## 
   ## 

   global PAR
   
   v = u ;
   clear u ;
   
   I = ID = cell(1, numfields(v)) ;
   j = 0 ;
   for [s k] = v
      if isfield(s, "id_ref")
	 ID{++j} = s.id_ref ;
      else
	 ID{++j} = s.id ;
      endif
   endfor

   [I{:}] = common(ID{:}) ;
   if all(cellfun(@(i) all(i), I)), return ; endif
   
   j = 0 ;
   for [s k] = v
      j++ ;
      for f = {"id" "id_ref" "x" "z" "a"}
	 if isfield(s, f{:})
	    PAR.idx.subs = repmat({":"}, 1, ndims(s.(f{:}))) ;
	    PAR.idx.subs{1} = I{j} ;
	    v.(k).(f{:}) = subsref(s.(f{:}), PAR.idx) ;
	 endif
      endfor
   endfor

endfunction
