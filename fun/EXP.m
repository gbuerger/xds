function y = EXP (x, p)

   ## usage:  y = EXP (x, p)
   ##
   ## probit fit function

   y = p(3) * log((x-p(1))/p(2)+1) ;

endfunction
