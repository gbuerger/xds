function y = frechetinv (x, m, s, a)

   ## usage:  y = frechetinv (x, m, s, a)
   ##
   ## Frechet inv cum distr.
   
   z = exp(-x.^(-a)) ;
   y = anom_inv(z, m, s) ;

endfunction