function res = upd_PC (PC, v, x, iptr)

   ## usage:  res = upd_PC (PC, v, x, iptr)
   ##
   ## update PC from ptr

   global PAR
   PAR.idx.subs = repmat({":"}, 1, ndims(x)) ;

   if isempty(PC.id)
      res = v ;
      res.x = x ;
   else
      [IPC Iv] = common(PC.id, v.id) ;
      if isfield(v, "id_ref")
	 res.id_ref = v.id_ref(Iv,:) ;
      endif
      res.id = v.id(Iv,:) ;
      PAR.idx.subs{1} = IPC ;
      wPC = subsref(PC.x, PAR.idx) ;
      PAR.idx.subs{1} = Iv ;
      wx = subsref(x, PAR.idx) ;
      
      res.x = cat(2, wPC, wx) ;
   endif

   res.J = [PC.J repmat(iptr, 1, columns(x))] ;
   res.cal = v.cal ;

endfunction
