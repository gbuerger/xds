function res = cellwr (fun, x)

   ## usage: res = cellwr (fun, x)
   ##
   ## wrapper function for cellfun

   if !iscell(x), error("cellwr: x not cell.\n") ; endif 

   res = feval(fun, x{:}) ;

endfunction
