function aout

   ## usage: aout
   ## 
   ## output areal average

   pkg load statistics
   
   global PAR PRJ GVAR ADIR SDIR CDIR BATCH Lavg per

   t0 = max(cellfun(@(f) stat(f).mtime, glob({sprintf("%s/%s/*.csv", SDIR, PAR.pdd) sprintf("%s/%s/*/*.csv", SDIR, PAR.pdd)}))) ;
   t1 = min(cellfun(@(f) stat(f).mtime, glob({sprintf("%s/%s.aout/*.csv", SDIR, PAR.pdd) sprintf("%s/%s.aout/*/*.csv", SDIR, PAR.pdd)}))) ;
   if t1 > t0 return ; endif
   
   odir = sprintf("%s/%s.aout", SDIR, PAR.pdd) ;

   if ~isempty(f = dbstack)
      fname = f(1).name ;
      printf("\n\t###   %s   ###\n\n", fname) ;
   else
      fname = "aout" ;
   endif

   idx.type = "()" ;
   Lptr = ~true ; wLptr = false ;
   mth = {"J" "F" "M" "A" "M" "J" "J" "A" "S" "O" "N" "D"} ;
   P0 = 86400 ; T0 = 273.16 ;
   LEG = {"OBS", "ANA", "CUR", "FUT"} ;
   per = [2010 1 1 ; 2010 12 31] ;
   cur.per = PAR.cper(:,1)' ; fut.per = [2070 2100] ;
   fut.per = [-Inf Inf] ;
   Lavg = ~true ;
   
   ldata = fullfile("data", PRJ, PAR.pdd) ;
   gdata = fullfile("data", PRJ, "ptr") ;

   METAFILE = fullfile(ldata, "meta.txt") ;

   annarg = {} ; Sfun = {"mean" "max"} ;

   [ids, vars, names, lons, lats, alts, undefs, J] = meta(METAFILE) ;
   VAR = sunique(vars) ; VARNAME = sunique(names) ;

   for jv=1:length(VAR)

      if !any(J{jv}), continue ; endif
      v = vars{J{jv}(1)} ; vn = names{J{jv}(1)} ; vv = strrep(v, ".", "_") ;

      if (exist("V", "var") && !any(strcmp(v, V))), continue ; endif
      vu = punit(v) ;

      printf("%s: <-- %s\n", fname, ifile = fullfile(ldata, [v ".csv"])) ;
      obs = read_stat(ifile) ;
      obs.ifile = ifile ;
      
      ## select ids from meta
      [Jm Js Km Ks] = unify(ids(J{jv})', obs.ids) ;

      obs.ids = ids(J{jv})(Jm)' ;
      obs.lons = lons(J{jv})(Jm)' ;
      obs.lats = lats(J{jv})(Jm)' ;
      obs.alts = alts(J{jv})(Jm)' ;
      obs.udf = undefs(J{jv})(Jm)' ;
      obs.vars = vars(J{jv})(Jm)' ;
      obs.x = obs.x(:,Ks) ; obs.x = lim2nan(obs.x, obs.udf) ;
      TP.obs.(v).id = obs.id ; TP.obs.(v).x = obs.x ; TP.obs.(v).ll = [obs.lons ; obs.lats] ;

      printf("%s: <-- %s\n", fname, ifile = fullfile(ADIR, PAR.pdd, [vv ".csv"])) ;
      ana = read_stat(ifile) ;
      ana.ifile = ifile ;
      ana.x = lim2nan(ana.x, obs.udf) ;
      TP.ana.(v).id = ana.id ; TP.ana.(v).x = ana.x ; TP.ana.(v).ll = TP.obs.(v).ll ;

      ifile = glob([fullfile(CDIR, PAR.pdd) "/" vv "*.csv"]){1} ;
      printf("%s: <-- %s\n", fname, ifile) ;
      sim2 = read_stat(ifile) ;
      sim2.ifile = ifile ;
      sim2.x = lim2nan(sim2.x, obs.udf) ;
      TP.sim2.(v).id = sim2.id ; TP.sim2.(v).x = sim2.x ; TP.sim2.(v).ll = TP.obs.(v).ll ;

      if PAR.mskobs
	 idx.subs = repmat({":"}, 1, columns(obs.x)) ;
	 [obs.I ana.I] = common(obs.id, ana.id) ;
	 I = isnan(obs.x(obs.I,:)) ;
	 w = ana.x(ana.I,:) ; w(I) = NaN ; ana.x(ana.I,:) = w ;
	 [obs.I sim2.I] = common(obs.id, sim2.id) ;
	 I = isnan(obs.x(obs.I,:)) ;
	 w = sim2.x(sim2.I,:) ; w(I) = NaN ; sim2.x(sim2.I,:) = w ;
      endif

      for s = {"obs" "ana" "sim2"}
	 s = s{:} ;
	 if exist(eval(sprintf("[fileparts(%s.ifile) \".aout\"] ;", s), "file")) == 7
	    continue ;
	 endif
	 eval(sprintf("pmkdir(%s.odir = [fileparts(%s.ifile) \".aout\"]) ;", s, s)) ;
	 eval(sprintf("%s.ids = {\"aout\"} ;", s)) ;
	 eval(sprintf("%s.vars = obs.vars(1) ;", s)) ;
	 eval(sprintf("%s.udf = mean(obs.udf) ;", s)) ;
	 eval(sprintf("%s.x = nanmean(%s.x, 2) ;", s, s)) ;
	 eval(sprintf("mwrite_stat([fileparts(%s.ifile) \".aout\"], %s) ;", s, s)) ;
      endfor

      F = glob({sprintf("%s/%s/[0-9][0-9]/%s.*-01.csv", SDIR, PAR.pdd, vv)}) ;
      if isempty(F)
	 error("xds:xds", "no station data, please rerun with PAR.aout = false") ;
      endif
      ne = numel(glob({sprintf("%s/%s/[0-9][0-9]", SDIR, PAR.pdd)})) ;
      F = reshape(F, [], ne) ;
      
      parfun(@(m) aout_(F(m,:), odir, m, obs, ne), 1:rows(F)) ;

   endfor

endfunction


## usage: aout_ (ifiles, odir, m, obs, ne)
##
## write single files
function aout_ (ifiles, odir, m, obs, ne)

   sim1 = [] ;
##   printf("%s: <-- %s\n", fname, F{m,1}) ;
##   printf("%s: ...\n", fname) ;
   for ifile = ifiles
      ifile = ifile{:} ;
      s = read_stat(ifile) ;
      s.x = lim2nan(s.x, obs.udf) ;
      sim1 = join(sim1, s) ;
   endfor
##   printf("%s: <-- %s\n", fname, F{m,end}) ;
   sim1.x = reshape(sim1.x, rows(sim1.x), [], ne) ;
   sim1.x = squeeze(nanmean(sim1.x, 2)) ;
   sim1.ids = arrayfun(@(i) sprintf("e%02d", i), 1:ne, "UniformOutput", false) ;
   sim1.vars = repmat(obs.vars(1), 1, ne) ;
   sim1.udf = repmat(mean(obs.udf), 1, ne) ;
   mwrite_stat(odir, sim1, sprintf("%02d.", m)) ;
   
endfunction


function vu = punit (v)

   ## usage: vu = punit (v)
   ## 
   ## physical units

   switch v
      case "T"
	 vu = " [{\\deg}C]" ;
      case "P"
	 vu = " [mm/d]" ;
      otherwise
	 vu = "" ;
   endswitch

endfunction
