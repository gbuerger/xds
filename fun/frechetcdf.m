function y = frechetcdf (x, m, s, a)

   ## usage:  y = frechetcdf (x, m, s, a)
   ##
   ## Frechet cum distr.
   
   z = anom(x, m, s) ;
   y = exp(-z.^(-a)) ;

endfunction