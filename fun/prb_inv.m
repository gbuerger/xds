function y = prb_inv (fun, x, p, q, Pp)

   ## usage:  y = prb_inv (fun, x, p, q, Pp)
   ##
   ## inv. probit transform using fun to fit cdf

   y = x ;
   I = isnan(x) ;

   w = normcdf(x(~I)) ;
   w = (w-q) ./ (1-q) ;

   ## FIXME introduced with CS 24011abcd6a30b14249b5eaac6ddcc6167eec64c
   if false
      p = [p polyval(Pp, p)] ;
   endif
   x0 = Pp ;
   
   p = mat2cell(p(:)', 1, ones(1, length(p))) ;

   w = feval([fun "inv"], w, p{2:end}) ;
   II = !isnan(w) ;
   w(II) = max(x0, w(II)*p{1} + x0) ;
   w(~II) = 0 ;

   y(!I) = w ; y(I) = nan ;

endfunction
