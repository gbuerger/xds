function z = mult (x, y, d1, d2)

   ## usage:  z = mult (x, y, d1, d2)
   ##
   ## nd-array multipication, along dims d1, d2

   dx = size(x) ; dy = size(y) ;

   if (nargin < 3)
      d1 = length(dx) ;
      d2 = 1 ;
   endif

   px = [1:d1-1 d1+1:length(dx) d1] ;
   py = [d2 1:d2-1 d2+1:length(dy)] ;

   x = permute(x, px) ;
   x = reshape(x, prod(dx(px)(1:end-1)), dx(px)(end)) ;
   y = permute(y, py) ;
   y = reshape(y, dy(py)(1), prod(dy(py)(2:end))) ;

   z = nanmult(x, y) ;
   z = reshape(z, [dx(px)(1:end-1) dy(py)(2:end)]) ;

endfunction
