function Y = bend (X, W)

   ## usage:  Y = bend (X, W)
   ##
   ## bend X to become pd

   if (nargin < 2), W = ones(size(X)) ; endif

   Y = X ;
   if any(isnan(Y(:))), return ; endif

   [V D] = eig(Y) ;

   while any(J = !((d = diag(D)) > 0))
      q = min(d(!J)) * 0.9 ;
      D(find(J)(1),find(J)(1)) = q ;
      Y = V * D * V' ;
      [c p] = chol(Y) ;
      if (p == 0), return ; endif
      [V D] = eig(Y) ;
   endwhile 

endfunction
