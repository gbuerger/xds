## usage: ncatt (ncf, attr)
##
## edit time attribute for forecasts
function ncatt (ncf, attr)

   if nargin < 2
      attr = "reftime" ;
   endif
   
   nc = ncinfo(ncf) ;

   jt = find(strcmp({nc.Variables.Name}, "time")) ;

   if any(strcmp({nc.Variables(jt).Attributes.Name}, attr))
      return ;
   endif

   t0 = ncread(ncf, "time")(1) ;
   ncwriteatt(ncf, "time", attr, t0) ;

endfunction
