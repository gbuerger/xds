function varargout = prbfit (x, pp)

   ## usage:  varargout = prbfit (x, pp)
   ##
   ## estimate probit using bspline with k knots, extrap. with rate q

   pkg load splines statistics
   
   global PAR

   if isempty(PAR)
      PAR.ncpu = 1 ;
      PAR.par_opt = {"UniformOutput", false, "VerboseLevel", 0} ;
   endif

   P = 1e-10 ; # strong spline smoothing

   N = size(x) ;
   x = reshape(x, [N(1:2), prod(N(3:end))]) ;
   PAR.idx.subs = repmat({":"}, 1, ndims(x)) ;

   if ndims(x) < 3
      CJ = cell(size(x,2), 1) ;
   else
      CJ = cell(size(x)(2:end)) ;
   endif
   if nargin > 1
      for i=1:size(x,2)
	 PAR.idx.subs{2} = i ;
	 wx = subsref(x, PAR.idx) ;
	 for j=1:size(wx,3)
	    CJ{i,j} = {wx(:,j), pp(:,i)} ;
	 endfor
      endfor
      w{1} = parfun(@(x) _prbfit(x), CJ, PAR.par_opt{:}) ;
   else
      for i=1:size(x,2)
	 PAR.idx.subs{2} = i ;
	 wx = subsref(x, PAR.idx) ;
	 for j=1:size(wx,3)
	    CJ{i,j} = {wx(:,j)} ;
	 endfor
      endfor
      w = cell(1, nargout) ;
      [w{:}] = parfun(@(x) _prbfit(x), CJ, PAR.par_opt{:}) ;
   endif
   for j=1:nargout
      varargout{j} = reshape(cell2mat(w{j}), [], num2cell(N){2:end}) ;
   endfor

endfunction


function varargout = _prbfit (x)

   ## usage:  varargout = _prbfit (x)
   ##
   ## wrapper for prbfit

   global PAR PRSV ;

   if Lin = (length(x) > 1), pp = x{2} ; endif
   ## transition to new pp.form element
   if Lin && !isfield(pp, "form")
      pp.form = "pp" ;
      pp.breaks = pp.x(:)' ;
      pp.coefs = pp.P ;
      pp.pieces = size(pp.coefs, 2) ;
      pp.order = size(pp.coefs, 3) ;
      # pp.orient = 1 ;
      pp.dim = pp.d ;
   endif
   ## transition to new pp.form element
   x = x{1} ;

   z = nan(rows(x), 1) ; 

   k = 5 ;

   if !any(I = !isnan(x)) && !Lin
      if ~PAR.udf
	 warning("xds:xds", "prbfit: no values") ;
      endif
      pp.breaks = nan(1,2*k-1) ;
      pp.dim = nan ;
      pp.coefs = nan(length(pp.breaks)-1, 4) ;
      pp.pieces = size(pp.coefs, 2) ;
      pp.order = size(pp.coefs, 3) ;
      pp.form = "pp" ;
      varargout{1} = pp ;
      if nargout == 2
	 varargout{2} = pp ;
      endif
      return ;
   endif

   if Lin
      if any(isnan(pp.breaks))
	 z(I) = nan ;
      else
	 z(I) = ppval(pp, x(I)) ;
      endif
      varargout{1} = z ;
      return ;
   endif

   u = x(I) ;

   [F, IF] = cdf_est(u) ;
   if PRSV
      [~, um, us] = anom(u) ;
      F = norminv(F, um, us) ;
      if PAR.dbg, printf("prbfit: %f %f\n", um, us) ; endif
   else
      F = norminv(F) ;
   endif
   if PAR.prbraw
      varargout = {nan, F} ;
      return ;
   endif 

   ## extrapolate and fit
   u = u(IF) ; F = F(IF) ;

   ub = getbreaks(u, k) ;
   Fb = getbreaks(F, k) ;

   uu = extrap(u) ;
   FF = extrap(F) ;

   if 0
      P = 1e-16 ; # strong spline smoothing
      P = 0 ; # straight line
      [wu wF] = dedup(uu, FF) ;
      pp = csaps(wu, wF, P) ;
      if nargout > 1
	 [wF wu] = dedup(FF, uu) ;
	 ppinv = csaps(wF, wu, P) ;
      endif
   else
      pp = splinefit(uu, FF, ub) ;
      ppinv = splinefit(FF, uu, Fb) ;
   endif

   if PAR.dbg
      pp_print(u, F, pp, ppinv) ;
   endif

   ## transition to new pp.form element
   if !isfield(pp, "form")
      pp.form = "pp" ;
      pp.breaks = pp.x ;
      pp.coefs = pp.P ;
      pp.pieces = size(pp.P, 2) ;
      pp.order = size(pp.P, 3) ;
      pp.dim = pp.d ;
   endif
   if !isfield(ppinv, "form")
      ppinv.form = "pp" ;
      ppinv.breaks = ppinv.x ;
      ppinv.coefs = ppinv.P ;
      ppinv.pieces = size(ppinv.P, 2) ;
      ppinv.order = size(ppinv.P, 3) ;
      ppinv.dim = ppinv.d ;
   endif
   ## transition to new pp.form element
   
   varargout{1} = pp ;
   if nargout > 1
      varargout{2} = ppinv ;
   endif

endfunction


function xb = getbreaks (x, k, d)

   ## usage: xb = getbreaks (x, k, d)
   ## 
   ## get breakpoints

   if nargin < 3, d = 3 ; endif

   xm = (x(1) + x(end)) / 2 ;
   xd = (x(end) - x(1)) / 2 ;

   xd = linspace(0, xd^d, k).^(1/d) ;

   xb(k+1:2*k) = xm + xd ;
   xb(1:k) = xm - fliplr(xd) ;

endfunction


function xx = extrap (x, q)

   ## usage: xx = extrap (x, q)
   ## 
   ## extrapolate x on both sides

   if nargin < 2, q = 0.3 ; endif

   m = rows(x) ; kq = min(m-1, floor(m*q)) ;
   xx = nan(m + 2*kq, 1) ;

   xx(1+kq:m+kq) = x ;
   xx(1:kq) = 2*x(1) - flipud(x(1+1:1+kq)) ;
   xx(m+kq+1:m+kq+kq) = 2*x(m) - flipud(x(m-kq:m-1)) ;
	 
endfunction


function pp_print (u, F, pp, ppinv, pfile)

   ## usage:  pp_print (u, F, pp, ppinv, pfile)
   ##
   ## print pp diagnostics

   persistent V = "" nprint = 0 ;
   global PAR PRJ BATCH v ;

   if ~strcmp(V, v)
      nprint = 0 ;
      V = v ;
   endif
   if nprint == 0 && ~isempty(Plots = glob(fullfile("data", PRJ, PAR.pdd, [v ".prb.*.png"])))
      delete(Plots{:}) ;
   endif
   nprint++ ;
   q = 2 ;

   if (nargin < 5)
      pfile = fullfile("data", PRJ, PAR.pdd, [v ".prb." num2str(nprint) ".png"]) ;
   endif
   if ~PAR.dbg && exist(pfile, "file"), return ; endif

   if BATCH
      figure(1, "visible", "off") ;
   endif

   ue = affin(u, q) ;
   Fe = affin(F, q) ;

   [~,Iu] = unique(ue) ;
   [~,IF] = unique(Fe) ;
   subplot(1,2,1) ;
   plot(ue(Iu), ppval(pp, ue(Iu)), u, F, ".") ;
   xlabel("phys.") ; ylabel("norm.") ;
   title(sprintf("probit fit %s", v)) ;
   subplot(1,2,2) ;
   plot(Fe(IF), ppval(ppinv, Fe(IF)), F, u, ".") ;
   xlabel("norm.") ; ylabel("phys.") ;
   title(sprintf("probit fit %s", v)) ;

   printf("pp_print: --> %s\n", pfile) ;
   print(pfile) ;
   pause(3) ;
   clf ;

endfunction
