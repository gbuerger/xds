## usage: par = wblfit (x)
##
## fit Weibull function
function par = wblfit (x)
   
   F = cdf_est(x) ;

   u = log(x) ;
   v = log(-log(1 - F)) ;

   p = polyfit(u, v, 1) ;

   par.shape = p(1) ;
   par.scale = exp(-p(2) / par.shape) ;
   
endfunction
