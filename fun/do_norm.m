function v = do_norm (v, cfile)

   ## usage: do_norm (v, cfile)
   ## 
   ## calculate climatologies

   global PAR PRJ isANA isCUR CDIR

   ##calc. annual cycle
   file2 = fullfile(CDIR, "ptr.ob") ;
   if isCUR && !isnewer(cfile, file2)

      if isANA
	 per = PAR.cper ;
      else
	 per = PAR.sper ;
      endif
      vw = selper(v, per(1,:), per(2,:)) ;
      H = unique(vw.id(:,4)) ;
      for jh = 1:numel(H) ; # hours defined
	 vh = selHour(vw, H(jh)) ;
	 if isempty(PAR.anc)
	    [~, clim(jh).xm, clim(jh).xs] = anom(vh.x) ;
	 else
	    if v.isP
	       if isnan(PAR.ssnwin)
		  [clim(jh).ps clim(jh).pp] = anncyclog(vh) ;
	       else
		  wrn = warning("query", "Octave:divide-by-zero") ;
		  warning("off", "Octave:divide-by-zero") ;
		  clim(jh).pp = ssnprb(vh.id, vh.x) ;
		  warning(wrn.state, "Octave:divide-by-zero") ;
	       endif
	    else
	       [clim(jh).ps vh] = anncyc(vh) ;
	       clim(jh).pp = prbfit(vh.a) ;
	    endif 
	 endif
	 clim(jh).h = H(jh) ;
      endfor
      printf("ptr: --> %s\n", cfile) ; 
      save(cfile, "clim") ;
   endif

   printf("ptr: <-- %s\n", cfile) ; 
   load(cfile)

   H = intersect([clim.h], unique(v.id(:,4))) ;
   if isANA && ~isempty(PAR.anahour)
      jh = find(H == PAR.anahour) ;
   elseif ~isempty(PAR.curhour)
      jh = find(H == PAR.curhour) ;
   elseif ~isempty(H)
      jh = find(H(1) == [clim.h]) ;
   else
      warning("xds:xds", "no hours found") ;
      [~, jh] = min(abs([clim.h] - unique(v.id(:,4))(1))) ;
      H = unique(v.id(:,4)) ;
   endif
   printf("do_norm> selecting hour: %3.0f\n", H(jh)) ;
   v = selHour(v, H(jh)) ;

   if isempty(PAR.anc)
      v.z = anom(v.x, clim(jh).xm, clim(jh).xs) ;
   else
      if v.isP
	    if isnan(PAR.ssnwin)
	       v = anncyclog(v, clim(jh).ps, clim(jh).pp) ;
	    else
	       wrn = warning("query", "Octave:divide-by-zero") ;
	       warning("off", "Octave:divide-by-zero") ;
	       v.z = ssnprb(v.id, v.x, clim(jh).pp) ;
	       warning(wrn.state, "Octave:divide-by-zero") ;
	    endif
	 if 0
	    N = size(v.z) ;
	    v.z = reshape(v.z, [N(1:2), prod(N(3:end))]) ;
	    C = squeeze(mat2cell(v.z, N(1), N(2), ones(prod(N(3:end)),1))) ;
	    w = cellfun(@(x) pfy(x, [clim(jh).pp.q]), C, PAR.par_opt{1:2}) ;
	    ##w = parfun(@(x) pfy(x, [clim(jh).pp.q]), C, PAR.par_opt{:}) ;
	    v.z = reshape(cell2mat(w'), N) ;
	 endif
      else
	 v = anncyc(v, clim(jh).ps) ;
	 v.z = prbfit(v.a, clim(jh).pp) ;
      endif
      v = rmfield(v, "x") ; 
   endif

endfunction
