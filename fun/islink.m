function res = islink (file)

   ## usage:  res = islink (file)
   ##
   ## check link status of file

   res = exist(file) && strcmp(lstat(file).modestr(1), "l") ;

endfunction
