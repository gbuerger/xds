## usage: [J y] = selJ (x, Ix, tol)
##
## select indices Ix(1) <= x <= Ix(2), with tolerance tol
function [J y] = selJ (x, Ix, tol=eps)

   J = x - Ix(1) + tol > 0 & Ix(2) - x + tol > 0 ;
   y = x(J) ;
   
endfunction
