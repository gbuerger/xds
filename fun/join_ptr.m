function res = join_ptr (u, v, dim)

   ## usage:  res = join_ptr (u, v, dim)
   ##
   ## join eps fields, poss. along dim

   if (nargin < 3), dim = 1 ; endif 

   if (isempty(u) && isempty(v))
      res = [] ;
      return ;
   endif
   if (isempty(u))
      res = v ;
      return ;
   endif
   if (isempty(v))
      res = u ;
      return ;
   endif

   res = u ;

   for [f kf] = v
      if !isstruct(f), continue, endif
      if isfield(res, kf)
	 for [g kg] = f
	    if strcmp(kg,"id") || strcmp(kg,"id_ref") || strcmp(kg,"x")
	       N1 = size(v.(kf).(kg)) ; N2 = size(res.(kf).(kg)) ;
	       try
		  res.(kf).(kg) = cat(dim, res.(kf).(kg), v.(kf).(kg)) ;
	       catch
		  [N1 ; N2]
		  error("xds:xds", "dimension mismatch for %s, %s", kf, kg) ;
	       end_try_catch
	    endif 
	 endfor 
      else
	 res.(kf) = f ;
      endif 
   endfor 

endfunction
