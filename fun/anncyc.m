function varargout = anncyc (s, ps)

   ## usage:  u = anncyc (s, ps)
   ##
   ## estimate and remove annual cycle, including scaling

   pkg load statistics
   persistent nprint = 0
   global PAR PRJ BATCH v

   mode = PAR.anc ;
   if islogical(mode), mode = "am" ; endif # backward compatibility

   u = s ;
   N = size(u.x) ;
   j0 = length(N)-1 ;
   K = [1 N(end-j0+1:end)] ;

   ##u.x = reshape(u.x, prod(N(1:end-j0)), prod(K)) ;
   if !isfield(s, "cal"), s.cal = "gregorian" ; endif 
   t = date2toy(u.id, s.cal) ;

   if (nargin < 2)
      nprint++ ;
      ps = harmest(t, u.x) ;
      u.a = u.x - PAR.acyc(t) * ps ;
      u.a(u.a == -inf) = nan ; # deal with ispos

      if regexp(mode, "m")
	 ps2 = harm2est(t, u.a) ;
	 ps = [ps ; ps2] ;
      endif
      
      if PAR.dbg
	 pfile = fullfile("data", PRJ, PAR.pdd, [v ".anc." num2str(nprint) ".png"]) ;
	 if BATCH
	    figure("visible", "off") ;
	 endif
	 subplot(5,1,1) ; plot(pltid(u.id), [u.x PAR.acyc(t) * ps(1:PAR.numhar,:)]) ;	    
	 set(gca, "colororder", jet(columns(ps))) ;
	 subplot(5,1,2) ; plot(pltid(u.id), u.a) ;
	 set(gca, "colororder", jet(columns(ps))) ;
	 if regexp(mode, "m")
	    subplot(5,1,3) ; plot(pltid(u.id), [abs(u.a) PAR.scyc.fun(t,ps2)]) ;
	    set(gca, "colororder", jet(columns(ps))) ;
	    subplot(5,1,4) ; plot(pltid(u.id), [abs(u.a)./PAR.scyc.fun(t,ps2)]) ;
	    set(gca, "colororder", jet(columns(ps))) ;
	    subplot(5,1,5) ; plot(pltid(u.id), [u.a ./ PAR.scyc.fun(t,ps2)]) ;
	    set(gca, "colororder", jet(columns(ps))) ;
	 endif
	 printf("anncyc: --> %s\n", pfile) ;
	 print(pfile) ; close ;
      endif

      if (nargout < 2)
	 ##varargout{1} = reshape(ps, [rows(ps) K(2:end)]) ;
	 varargout{1} = ps ;
	 return ;
      endif
   endif

   u.a = u.x - PAR.acyc(t) * ps(1:PAR.numhar,:) ;

   if regexp(mode, "m")
      ps2 = ps(PAR.numhar+1:end,:) ;
      u.a = u.a ./ PAR.scyc.fun(t,ps2) ;
   endif
   
   ##u.x = reshape(u.x, N) ;
   ##u.a = reshape(u.a, N) ;

   if (nargin > 1)
      varargout{1} = u ;
   else
      varargout{1} = ps ;
      varargout{2} = u ;
   endif 
      
endfunction


## usage: ps = harmest (t, x)
##
## estimate harmonic parameters
function ps = harmest (t, x)
   global PAR

   N = size(x) ;
   if length(N) > 2		# collect ensemble for clim
      X_h = repmat(PAR.acyc(t), N(3), 1) ;
      x = permute(x, [1 3 2]) ;
      x = reshape(x, N(1)*N(3), N(2)) ;
   else
      X_h = PAR.acyc(t) ;
   endif
   
   N = size(x) ;
   C = mat2cell(x, N(1), ones(N(2),1)) ;
   ##ps = parfun(@(y) _regress(y, X_h), C, PAR.par_opt{:}) ;
   ps = parfun(@(y) NaN_("regress", y, X_h), C, PAR.par_opt{:}) ;
   ps = cell2mat(ps) ;
endfunction


## usage: ps = harm2est (t, x)
##
##
function ps = harm2est (t, x)
   global PAR
   pkg load optim

   N = size(x) ;
   if length(N) > 2		# collect ensemble for clim
      t = repmat(t, size(x, 3), 1) ;
      x = permute(x, [1 3 2]) ;
      x = reshape(x, N(1)*N(3), N(2)) ;
   endif

   N = size(x) ;
   C = mat2cell(abs(x), N(1), ones(N(2),1)) ;
   Fx = @(x) NaN_("leasqr", t, x, PAR.scyc.init, PAR.scyc.fun, [], niter=1000) ;
   ps = parfun(Fx, C, PAR.par_opt{:}) ;
   ps = cell2mat(ps) ;
endfunction


## usage:  res = _regress (y, x)
##
## wrapper for regress
function res = _regress (y, x)

   global PAR ;

   I = !isinf(y) ;
   if (sum(!isnan(y)) < PAR.Ccases)
      res = nan(5,1) ;
   else
      res = regress(y(I), x(I,:)) ;
   endif
endfunction
