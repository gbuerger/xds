## usage: write_stat (odir, s, sfx)
##
## write structure s to output dir odir
function write_stat (odir, s, sfx="")

   global PAR

   defPAR("ofmt", "%.1f") ;	# output format
   if ~isfield(PAR, "idx")
      PAR.idx.type = "()" ;
   endif
   
   if isfield(s, "udf")
      s.x = nan2lim(s.x, s.udf) ;
   else
      s.x = nan2lim(s.x) ;
   endif
   
   for v = sunique(s.vars)

      v = v{:} ;
      J = strcmp(s.vars, v) ;
      PAR.idx.subs = repmat({":"}, 1, ndims(s.x)) ; PAR.idx.subs{2} = J ;
      w = subsref(s.x, PAR.idx) ; Nw = size(w) ;
      IDS = repmat(s.ids(:,J), 1, prod(Nw(3:end))) ;
      w = reshape(w, size(w, 1), []) ; Nw = size(w) ;

      fid = fopen(ofile = fullfile(odir, [sfx v ".csv"]), "wt") ;
      printf("out: --> %s\n", ofile) ;
      if isfield(s, "id_ref")
	 fmt = ["Y,M,D,Yr,Mr,Dr" repmat(",%s", 1, Nw(2)) "\n"] ;
	 fprintf(fid, fmt, IDS{:}) ;
	 fmt = ["%d,%d,%d,%d,%d,%d" repmat(["," PAR.ofmt], 1, Nw(2)) "\n"] ;
	 fprintf(fid, fmt, [s.id(:,1:3) s.id_ref(:,1:3) w]') ;
      else
	 fmt = ["Y,M,D" repmat(",%s", 1, sum(J)) "\n"] ;
	 fprintf(fid, fmt, IDS{:}) ;
	 fmt = ["%d,%d,%d" repmat(["," PAR.ofmt], 1, sum(J)) "\n"] ;
	 fprintf(fid, fmt, [s.id(:,1:3) w]') ;
      endif
      fclose(fid) ;

      if ndims(s.x) <= 2, continue ; endif

      fid = fopen(ofile = fullfile(odir, [sfx v ".em.csv"]), "wt") ;
      if isfield(s, "id_ref")
	 fmt = ["Y,M,D,Yr,Mr,Dr" repmat(",%s", 1, sum(J)) "\n"] ;
	 fprintf(fid, fmt, s.ids(:,J){:}) ;
	 fmt = ["%d,%d,%d,%d,%d,%d" repmat(["," PAR.ofmt], 1, sum(J)) "\n"] ;
	 fprintf(fid, fmt, [s.id(:,1:3) s.id_ref(:,1:3) s.em(:,J)]') ;
      else
	 fmt = ["Y,M,D" repmat(",%s", 1, sum(J)) "\n"] ;
	 fprintf(fid, fmt, s.ids(:,J){:}) ;
	 fmt = ["%d,%d,%d" repmat(["," PAR.ofmt], 1, sum(J)) "\n"] ;
	 fprintf(fid, fmt, [s.id(:,1:3) s.em(:,J)]') ;
      endif
      fclose(fid) ;

      printf("out: --> %s\n", ofile) ;

   endfor

endfunction
