## usage: [isOD ofile err] = chkOD (ifile)
##
## check OPeNDAP
function [isOD ofile err] = chkOD (ifile)

   ofile = tempname ;

   [err out] = system(sprintf("ncdump -h %s 2>&1", ifile)) ;

   if isOD = (err == 0)

      ofile = ifile ;

##   else

##      try

##	 urlwrite(ifile, ofile) ;

##      catch

####	 ifile = strrep(ifile, "dodsC", "fileServer") ;
####	 urlwrite(ifile, ofile) ;

####	 [err out] = system(sprintf("wgrib2 %s -netcdf %s.nc 2>&1", ofile, ofile)) ;
####	 if err == 0
####	    ofile = [ofile ".nc"] ;
####	 endif
	 
##      end_try_catch

   elseif err ~= 1
      
      error("ncdump is missing on your system") ;

   endif
   
endfunction
