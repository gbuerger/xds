function s = SelFld (r, fld)

   ## usage:  s = SelFld (r, fld)
   ##
   ## select fld from the fields in r

   s = [] ;
   
   if isfield(r, "cp") && isfield(r, "lsp")
      r.pr = r.prc = r.cp ;
      r.pr.x = r.cp.x + r.lsp.x ;
      r.prc.x = r.cp.x ;
      r = rmfield(r, "cp") ;
      r = rmfield(r, "lsp") ;
   endif

   for f=fld
      f = f{:} ;
      for g=fieldnames(r)'
	 g = g{:} ;
	 if strcmp(f, g)
	    s.(f) = r.(g) ;
	 endif 
      endfor
      if ~isfield(s, f)
	 s = [] ;
	 warning("xds:xds", "SelFld> field %s missing", f) ;
	 return ;
      endif
   endfor

   if isempty(s)
      error("SelFld: no fields selected.\n") ;
   endif 

endfunction
