## usage: ncwrite_stat (odir, s, ptr = [], ncdir)
##
## write netcdf output
function ncwrite_stat (odir, s, ptr = [], ncdir)

   global SNAME = "station"
   pkg load netcdf

   odir = strsplit(odir, filesep) ;
   if isempty(n = find(strcmp(odir, ptr)))
      pmkdir(odir = tilde_expand(fullfile(ncdir, odir(1+1:end){:}))) ;
   else
      pmkdir(odir = tilde_expand(fullfile(ncdir, odir(n+1:end){:}))) ;
   endif
   
   [~, I, J] = unique(s.vars) ; V = s.vars(sort(I)) ;
   [~, I, J] = unique(s.names) ; VN = s.names(sort(I)) ;

   Nx = size(s.x) ;
   
   I = repmat({":"}, 1, length(Nx)) ;
   
   for i = 1:length(V)

      I{2} = strcmp(s.vars, V{i}) ; ns = sum(I{2}) ;

      if length(Nx) < 3
	 DIMS = {SNAME, ns, "time", Inf} ;
	 x = permute(s.x(I{:}), [2 1]) ;
      else
	 DIMS = {SNAME, ns, "member", Nx(3), "time", Inf} ;
	 x = permute(s.x(I{:}), [2 3 1]) ;
      endif

      unlink(ofile = sprintf("%s/%s.nc", odir, V{i})) ;
      printf("--> %s\n", ofile) ;

      nccreate(ofile, V{i}, "Dimensions", DIMS) ;
      nccreate(ofile, "time", "Dimensions", {"time"}) ;
      wids = char(s.ids)(I{2},:) ;
      strlen = columns(wids) ;
      nccreate(ofile, "str", "Dimensions", {"str", strlen}) ;
      ncwriteatt(ofile, "time", "units", sprintf("days since %s", datestr(1, "yyyy-mm-dd"))) ;

      nccreate(ofile, SNAME, "Dimensions", {SNAME}) ;
      nccreate(ofile, "ids", "Dimensions", {"str", SNAME}, "Datatype", "char") ;
      ncwriteatt(ofile, "ids", "standard_name", "station ids") ;
      nccreate(ofile, "alt", "Dimensions", {SNAME}) ;
      ncwriteatt(ofile, "alt", "standard_name", "elevation") ;
      ncwriteatt(ofile, "alt", "units", "m") ;
      nccreate(ofile, "lon", "Dimensions", {SNAME}) ;
      ncwriteatt(ofile, "lon", "standard_name", "longitude") ;
      ncwriteatt(ofile, "lon", "units", "degrees_east") ;
      nccreate(ofile, "lat", "Dimensions", {SNAME}) ;
      ncwriteatt(ofile, "lat", "standard_name", "latitude") ;
      ncwriteatt(ofile, "lat", "units", "degrees_north") ;
      ncwriteatt(ofile, V{i}, "standard_name", VN{i}) ;
      if regexpi(VN{i}, "temp")
	 ncwriteatt(ofile, V{i}, "units", "degrees Celsius") ;
      else
	 ncwriteatt(ofile, V{i}, "units", "mm") ;
      endif
      ncwrite(ofile, "time", datenum(s.id)) ;
      ncwrite(ofile, SNAME, (1:ns)') ;
      ncwrite(ofile, "str", (1:strlen)') ;
      ncwrite(ofile, "ids", wids') ;
      ncwrite(ofile, "alt", s.alts(I{2})') ;
      ncwrite(ofile, "lon", s.lons(I{2})') ;
      ncwrite(ofile, "lat", s.lats(I{2})') ;
      ncwrite(ofile, V{i}, x) ;

      ncwriteatt(ofile, "/", "Conventions", "CF-1.6") ;
      ncwriteatt(ofile, "/", "featureType", "timeSeries") ;
      ncwriteatt(ofile, "/", "title", "XDS for SaWaM") ;
      ncwriteatt(ofile, "/", "Created", date) ;

   endfor

endfunction
