## usage: JEOF = mtl (x, y)
##
##
function JEOF = mtl (x, y)

   global PAR
   addpath ~/xds/extra/mtl

   lambdas_m2 = GDCV(@mtlasso2G, x, y, [1 1 1], PAR.kfold);
   Bhat_m2 = mtlasso2G(x(1:100,1:10), y(1:100,1:3), lambdas_m2);

##   Post-processing
   [N K] = size(x) ; J = columns(y) ;
   thres = 1 * sqrt(log(J * K) / (N * K));
   Bhat_m2(abs(Bhat_m2) < thres) = 0;

   JEOF = abs(Bhat_m2) < thres ;
   rmpath ~/oct/mtl

endfunction
