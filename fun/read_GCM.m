function s = read_GCM (ncfile)

   ## usage:  s = read_GCM (ncfile)
   ##
   ## read GCM files

   pkg load netcdf ;
   f = dbstack ;

   global PAR isANA isCUR GVAR
   persistent warned_ana = false warned_cur = false ;
   
   ncfile = tilde_expand(ncfile) ;

   ## check OPeNDAP
   [isOD ncfile1 err] = chkOD(ncfile) ;
   if err
      s = [] ;
      warning("xds:xds", "file not found: %s", ncfile) ;
      return ;
   else
      ncfile = ncfile1 ;
   endif

   nc = ncinfo(ncfile) ;
   
   STDGRD = hasTime = hasLev = false ; j = 0 ;
   for nv=nc.Variables
      name = nv.Name ;
      if ~ncisdim(nc, name) & isempty(regexp(name, "time")), continue ; endif
      switch name
	 case {"longitude" "lon"}
	    ws.lon = double(ncread(ncfile, name)) ;
	    J = ws.lon > 180 ;
	    ws.lon(J) = ws.lon(J) - 360 ;
	    ws.nlon = nv.Dimensions.Length ;
	    STDGRD = true ;
	 case {"latitude" "lat"}
	    ws.lat = double(ncread(ncfile, name)) ;
	    ws.nlat = nv.Dimensions.Length ;
	    STDGRD = true ;
	 case {"level" "lev" "plev" "levelist" "isobaric" "isobaric3"}
	    hasLev = true ;
	    ws.lev = double(ncread(ncfile, name)) ;
	    ws.nlev = nv.Dimensions.Length ;
	    j = find(strcmp({nv.Attributes.Name}, "units")) ;	    
	    if strcmp(keyval(nv.Attributes, "units"), "Pa")
	       ws.lev = ws.lev / 100 ;
	    endif
	 case {"time" "reftime" "leadtime" "date"}
	    hasTime = true ;
	    ws.(name) = double(ncread(ncfile, name)) ;
	    if ~isempty(nv.Dimensions)
	       ws.(["n" name]) = nv.Dimensions.Length ;
	    endif
	    if ~(wcal = keyval(nv.Attributes, "calendar"))
	       wcal = "gregorian" ;
	    endif
	 otherwise
	    continue ;
      endswitch
   endfor

   ## fix time coordinate, transform to unique single time coordinate
   DT = false ;
   if isfield(ws, "date")
      tm = "date" ;
      if isfield(ws, "time")
	 DT = true ;
	 dt = "time" ;
      else
	 dt = "" ;
      endif
   elseif isfield(ws, "time")
      tm = "time" ;
   else
      error("read_GCM> no time coordinate") ;
   endif
   
   ## prd: define valid time
   tref = false ;
   jt = find(strcmp({nc.Variables.Name}, tm)) ;
   jlt = find(strcmp({nc.Variables.Name}, "leadtime")) ;
   jrt = find(strcmp({nc.Variables.Name}, "reftime")) ;
   if isempty(jt) & ~isempty(jlt) & ~isempty(jrt)
      t = ws.reftime + ws.leadtime/24 ;
      v = keyval(nc.Variables(jrt).Attributes, "units") ;
      id = nctime(t, v, wcal) ;
   else
      if DT	    # UGLY
	 t = (ws.(dt)/24 + ws.(tm)')(:) ;
      else
	 t = ws.(tm) ;
      endif
      v = keyval(nc.Variables(jt).Attributes, "units") ;
      if isempty(dstr)
	 error("xds:xds", "ptr>read_GCM: could not determine time coordinate") ;
      endif
      if isANA && isempty(PAR.anahour) && min(diff(t)) < 24
	 PAR.anahour = nctime(0, v, wcal)(4) ;
      endif
      if isCUR && isempty(PAR.curhour) && min(diff(t)) < 24
	 PAR.curhour = nctime(0, v, wcal)(4) ;
      endif
      id = nctime(t, v, wcal) ;
      if tref = keyval(nc.Variables(jt).Attributes, "reftime")
	 id_ref = nctime(tref, v, wcal) ;
      elseif tref = (tref || (~isCUR & PAR.prd))
	 id_ref = id_extr(ncfile) ;
      else
	 id_ref = id(1,:) ;
      endif
   endif

   lon0 = PAR.lon(1) ; lon1 = PAR.lon(end) ;
   lat0 = PAR.lat(1) ; lat1 = PAR.lat(end) ;
   if hasLev
      lev0 = PAR.lev(1) ; lev1 = PAR.lev(end) ;
   endif
   
   ## widen selection area for common grids
   d = (lon1 - lon0) * PAR.qgrd ;
   lon0 = lon0 - d ; lon1 = lon1 + d ; lat0 = lat0 - d ; lat1 = lat1 + d ;

   s = [] ;
   for nv=nc.Variables
      nvName = strrep(nv.Name, "-", "_") ;
      nvn = nvName ;
      if ncisdim(nc, nvn), continue ; endif
      if ~any(cellfun(@(c) ~isempty(strfind(c, nvn)), GVAR(:))), continue ; endif
      ## if isempty(nvn._FillValue) && isempty(nvn.missing_value), continue ; endif # neglect non-fields
      if length(nv.Size) < 3, continue ; endif # neglect non-fields
      if (!STDGRD && (strcmp(nvn, "lon") || strcmp(nvn, "lat") || strcmp(nvn, "lev"))), continue ; endif

      if STDGRD
	 Jx = find(lon0 <= ws.lon & ws.lon <= lon1) ;
	 Jy = find(lat0 <= ws.lat & ws.lat <= lat1) ;
	 if hasLev
	    Jl = find(lev0 <= ws.lev & ws.lev <= lev1) ;
	 else
	    Jl = [] ;
	 endif
      else
	 [ll0, ll1] = rotfind([lon0  lat0], [lon1 lat1], ws.lon, ws.lat) ;
	 Jx = ll0(1):ll1(1) ;
	 Jy = ll0(2):ll1(2) ;	  
	 if PAR.dbg
	    c0 = min([ws.lon(Jx, Jy)(:), ws.lat(Jx, Jy)(:)]) ;
	    c1 = max([ws.lon(Jx, Jy)(:), ws.lat(Jx, Jy)(:)]) ;
	    printf("app: nonstandard grid with range (%4.1f,%4.1f) (%4.1f,%4.1f)\n", c0, c1) ;
	 endif
      endif

      if (j=find(diff(Jx) > 1)) ; # wrap longitudes
	 Jx = Jx([j+1:length(Jx) 1:j]) ;
      endif

      if isempty(Jx) || isempty(Jy)
	 error("xds:xds", "%s: no ptr values for %s, check lon, lat, lev", f(1).name, nv.Name) ;
      endif

      if STDGRD
	 wlon = ws.lon(Jx) ; wlat = ws.lat(Jy) ;
      else
	 wlon = ws.lon(Jx,Jy) ; wlat = ws.lat(Jx,Jy) ;
      endif

      x = ncloop(ncfile, nv, Jx, Jy, Jl) ;

      dlat = find(strncmp({nv.Dimensions.Name}, "lat", 3)) ;
      dlon = find(strncmp({nv.Dimensions.Name}, "lon", 3)) ;
      dlev = find(strncmp({nv.Dimensions.Name}, "lev", 3) | strncmp({nv.Dimensions.Name}, "plev", 3) | strncmp({nv.Dimensions.Name}, "isob", 4)) ;
      [x wlat] = orderlat(x, dlat, wlat) ; # latitudes ordered increasingly

      ## expand times, levels...
      if hasLev
	 wlev = ws.lev(Jl) ;
	 for j=1:length(wlev)
	    if isfield(PAR, "lev") && !isempty(PAR.lev) && isfield(ws, "lev") && !isempty(ws.lev) && !ismember(wlev(j), PAR.lev), continue, endif
	    if isfield(PAR, "plev") && !isempty(PAR.plev) && isfield(ws, "plev") && !isempty(ws.plev) && ws.plev(j) != PAR.plev, continue, endif
	    nvn = [nvName "_" strrep(num2str(wlev(j)), ".", "_")] ;
	    PAR.idx.subs = repmat({":"}, 1, ndims(x)) ; PAR.idx.subs{dlev} = j ;
	    s.(nvn).x = subsref(x, PAR.idx) ;
##	    s.(nvn).x = nsqueeze(x, dlev, j) ; # FIXME
##	    ## select level and vectorize lon-lat grid and reorder t, x, ...
##	    if any(strncmp({nv.Dimensions.Name}, "lev", 3))
##	       [~,iv] = setdiff(strtrunc({nv.Dimensions.Name}, 3), "lev") ;
##	    elseif any(strncmp({nv.Dimensions.Name}, "plev", 4))
##	       [~,iv] = setdiff(strtrunc({nv.Dimensions.Name}, 3), "ple") ;
##	    elseif any(strncmp({nv.Dimensions.Name}, "isob", 4))
##	       [~,iv] = setdiff(strtrunc({nv.Dimensions.Name}, 4), "isob") ;
##	    endif
##	    s.(nvn).x = vct(s.(nvn).x, nv.Dimensions, sort(iv)) ;
	    s.(nvn).x = vct(s.(nvn).x, nv.Dimensions) ;
	    s.(nvn).N = size(s.(nvn).x) ;

	    s.(nvn).x = outlier(s.(nvn).x) ;

	    s.(nvn).time = ws.(tm) ;
	    s.(nvn).ntime = ws.(["n" tm]) ;
	    s.(nvn).cal = wcal ;
	    s.(nvn).lon = wlon ; s.(nvn).lat = wlat ; s.(nvn).lev = wlev(j) ;
	    s.(nvn).name = keyval(nv.Attributes, "long_name") ;
	    s.(nvn).id = id ;
	    if tref
	       s.(nvn).id_ref = repmat(id_ref, s.(nvn).N(1), 1) ;
	    endif
	    s.(nvn).stdgrd = STDGRD ;
	 endfor
      else
	 s.(nvn).x = squeeze(x) ;
	 ## vectorize lon-lat grid and reorder t, x, ...
	 s.(nvn).x = vct(s.(nvn).x, nv.Dimensions) ;
	 s.(nvn).N = size(s.(nvn).x) ;

	 s.(nvn).id = id ;
	 s.(nvn).x = outlier(s.(nvn).x) ;

	 s.(nvn).time = ws.(tm) ;
	 s.(nvn).ntime = ws.(["n" tm]) ;
	 s.(nvn).cal = wcal ;
	 s.(nvn).lon = wlon ; s.(nvn).lat = wlat ;
	 s.(nvn).name = keyval(nv.Attributes, "long_name") ;
	 if regexp(s.(nvn).name, "precip", "ignorecase")
	    ## cases when transforming to diff
	    N = s.(nvn).N ; N(PAR.rec) = 1 ;
	    L{1} = tref ;
	    L{2} = ~isnan(PAR.prcacc) ;
	    L{3} = ~isempty(regexp(ncfile, "/SEAS./.*\\.sfc\\.nc")) ;
	    if or(L{:})
	       s.(nvn).x = diff(cat(PAR.rec, s.(nvn).x, nan(N)), 1, PAR.rec) ;
	    endif
##	    if PAR.prcacc == 0
####	       s.(nvn).x = cat(PAR.rec, nan(N), diff(s.(nvn).x, 1, PAR.rec)) ; # #don't know what's this
##	    else
##	       if regexp(ncfile, "/SEAS./.*\\.sfc\\.nc")
##		  s.(nvn).x = diff(cat(PAR.rec, s.(nvn).x, nan(N)), 1, PAR.rec) ;
##	       else
##		  s.(nvn).x = diff(cat(PAR.rec, zeros(N), s.(nvn).x), 1, PAR.rec) ;
##	       endif
##	    endif
	    if any(s.(nvn).x(:)) < 0
	       l = find(diff(s.(nvn).x)(:))(1) ;
	       error("negative tp at: %d, %s", l, ncfile) ;
	    endif
	 endif
	 if tref
	    s.(nvn).id_ref = repmat(id_ref, s.(nvn).N(1), 1) ;
	 endif
	 s.(nvn).stdgrd = STDGRD ;
      endif

      ## CFSR peculiarities
      if (N=size(ws.(tm)))(2) > 1
	 s.(nvn).id = squeeze(nctime(ws.reftime, v, wcal)) ;
	 s.(nvn).x = squeeze(mean(s.(nvn).x, 1)) ;
	 s.(nvn) = to_daily(s.(nvn)) ;
      endif
      
      ## ECMWF precip
      if regexp(ncfile, "/(ei|era5)/.*\\.sfc\\.nc") && regexp(s.(nvn).name, "precip", "ignorecase")
	 s.(nvn) = to_daily(s.(nvn), @nansum) ;
      endif

      if !STDGRD
	 s.(nvn).id(:,4) = 12 ;
	 # s.(nvn) = fill_date(s.(nvn)) ;
      endif

   endfor

   if ~isOD
      unlink(ncfile) ;
   endif
   
endfunction
