## usage: pkg_get ()
##
## get required packages
function pkg_get ()

   P = {"statistics" "miscellaneous" "io" ...
		  "econometrics" "optim" "netcdf" "mapping" ...
		  "parallel" "signal" "splines" "octproj"} ;

   for p = P
      if isempty(pkg("list", p{:}))
	 pkg("install", "-local", "-nodeps", "-forge", p{:}) ;
      endif
   endfor
   
endfunction
