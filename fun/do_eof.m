function [pc v] = do_eof (u, avar, svar)

   ## usage: [pc v] = do_eof (u, avar, svar)
   ## 
   ## 

   pkg load statistics
   
   global PAR isANA isCUR ADIR SDIR keof
   persistent nfun = 0 ; nfun++ ;
   cdata = strrep(SDIR, PAR.sim{:}) ;

   v = u ;

   Judf = all(isnan(v.z), 1) ;
   if !PAR.udf && any(Judf)
      warning("xds:xds", "app: found undefined grid points. Consider setting PAR.udf.") ;
   endif
   if PAR.udf
      efile = fullfile(ADIR, "eof_udf", [avar ".ob"]) ;
      gfile = fullfile(ADIR, "clim_udf/G.ob") ;
      pmkdir(fullfile(ADIR, "udf"), fullfile(ADIR, "eof_udf")) ;
      if !exist(ufile = fullfile(ADIR, "udf", [avar ".ot"]), "file")
	 printf("app: --> %s\n", ufile) ;
	 save(ufile, "-text", "Judf") ;
      else
	 ## loading udf mask
	 printf("app: <-- %s\n", ufile) ;
	 w = load(ufile) ;
	 Judf = Judf | w.Judf ;
	 printf("app: --> %s\n", ufile) ;
	 save(ufile, "-text", "Judf") ;
	 if !isCUR && nfun == 1
	    printf("app: ###############################################\n") ;
	    warning("xds:xds", "app: Using PAR.udf, so consider the need to re-calibrate!") ;
	    printf("app: ###############################################\n") ;
	 endif
      endif
      v.z(:,Judf) = nan ;
   else
      efile = fullfile(ADIR, "eof", [avar ".ob"]) ;
      gfile = fullfile(ADIR, "clim/G.ob") ;
   endif

   if isANA
      vw = selper(v, PAR.cper(1,:), PAR.cper(2,:)) ;
      eof.G = covadj(vw.z) ;
      eof.E = pca(vw.z, PAR.trunc) ;

      [eof.nr eof.nc] = size(eof.E) ; eof.lon = v.lon ; eof.lat = v.lat ;

      if PAR.rotEOF
	 [U, r, lrms] = Kabsch(eof.E(:,1), ones(rows(eof.E),1)) ;	 
	 eof.E = U * eof.E ;
      endif

      printf("app: --> %s\n", efile) ; 
      save(efile, "eof") ;
   else
      load(efile) ;
      printf("app: <-- %s\n", efile) ;
   endif

   if strfind(PAR.ptradj, "sgl")
      printf("app: adjusting covariance for %s\n", svar) ;
      mfile = fullfile(CDIR, "clim", [avar ".m.ob"]) ;
      if isCUR
	 vw = selper(v, PAR.cper(1,:), PAR.cper(2,:)) ;
	 wm = nanmean(vw.z) ;
	 save(mfile, "wm")
	 printf("app: --> %s\n", mfile) ; 
	 w = anom(v.z, wm) ;
      else
	 if strfind(PAR.ptradj, ".loc")
	    [w wm] = anom(v.z) ;
	 else
	    load(mfile) ;
	    printf("app: <-- %s\n", mfile) ; 
	    w = anom(v.z, wm) ;
	 endif
      endif 
      w = covadj(w, eof.G) ;
      v.z = repmat(wm, rows(w), 1) + w ;
   endif

   printf("app: EOFs: %s  %d  (%d to %d)\n", svar, eof.nc, keof+1, keof+=eof.nc) ;
   d1 = 2 ; d2 = ndims(eof.E) - 1 ;
   pc = mult(v.z, eof.E, d1, d2) ;
   if ndims(pc) > 2
      p = [1, ndims(v.z)-1 + (1:d2), 2:ndims(v.z)-1] ;
      pc = permute(pc, p) ;
   endif
endfunction
