function [t I J] = date2toy (id, cal="gregorian", opts=0)

   ## usage:  [t I J] = date2toy (id, cal, opts)
   ##
   ## calculate time of year in radians

   if columns(id) > 3
      h = id(:,4) ;
   else
      h = 24 * ones(rows(id), 1) ;
   endif 
   n = rows(id) ;

   if strcmp(cal, "360_day") || strfind(cal, "360")
      per = repmat(360, n, 1) ;
      cmon = [0 repmat(30, 1, 11)] ;
      cmon = repmat(cmon, n, 1) ; 
   else
      per = repmat(365, n, 1) ;
      cmon = [0 31 28 31 30 31 30 31 31 30 31 30] ;
      cmon = repmat(cmon, n, 1) ; 
      if false && (strcmp(cal, "gregorian"))
	 I = (yeardays(y) == 366) ;
	 cmon(I,3) = 29 ;
      endif 
   endif 

   if opts == 0

      y = id(:,1) ; m = id(:,2) ; d = id(:,3) ;

      cmon = cumsum(cmon, 2) ;

      I = repmat(1:12, n, 1) == repmat(m, 1, 12) ;
      t = cmon'(I') + d ; # day of year

   else
      
      id = [id zeros(rows(id), 6-columns(id))] ;
      t = mat2cell(id, ones(rows(id),1), columns(id)) ;

      t = cellfun(@(c) strptime(sprintf("%04d%02d%02dT%02d%02d%02d", c(1), c(2), c(3), c(4), c(5), c(6)), "%Y%m%dT%T").yday, t) + 1 ;

   endif

   t = 2 * pi * ((t - 1) * 24 + h) ./ (per * 24) ;
   [~,I,J] = unique(t) ;

endfunction
