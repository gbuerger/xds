function c=gevcdf(q,xi,mu,sigma),
%CDF for GEV
%
%     USAGE: c = gevcdf(q,xi,mu,beta)
%
%         q: Quantile
%xi,mu,beta: Parameters
%         c: Cumulative probability
   z = 1+(xi.*(q-mu))./sigma ;
   c=exp(-z.^(-1/xi));
   c(z<=0) = nan ;

endfunction
