## usage: res = fNaN (fun, varargin)
##
## evaluate fun(x(~nan), y(~nan))
function res = fNaN (fun, varargin)
   I = all(isfinite(varargin{1}), 2) ;
   for i=2:nargin
      I = I & all(isfinite(varargin{i}), 2) ;
   endfor

   if ~any(I)
      error("xds:xds", "no values for function %s", fun) ;
   else
      C = cellfun(@(x) x(I,:), varargin, "UniformOutput", false) ;
      res = cellfun(fun, C) ;
   endif
endfunction
