function L = chklonlat (lon1, lon2, lat1, lat2, tol)

   ## usage: L = chklonlat (lon1, lon2, lat1, lat2, tol)
   ## 
   ## check lon lat for equality

   L = numel(lon1) == numel(lon2) && numel(lat1) == numel(lat2) ;

   if !L, return, endif

   L = norm(lon1 - lon2) <= tol && norm(lat1 - lat2) <= tol ;

endfunction
