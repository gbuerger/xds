function res = Xjfun (J)

   ## usage:  res = Xjfun (J)
   ##
   ## ugly indexing function

   for j=1:length(J)
      res(j) = j - find(J == J(j))(1) + 1 ;
   endfor

endfunction
