function [v I] = sunique (u)

   ## usage:  [v I] = sunique (u)
   ##
   ## unsorted unique

   if !iscell(u)
      error("sunique: need cell input.\n") ;
   endif 

   [~,I] = unique(u) ;
   I = sort(I) ;
   v = u(I) ;

endfunction
