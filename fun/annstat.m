function [idy y] = annstat (idx, x, fun, varargin)

   ## usage:  [idy y] = annstat(idx, x, fun)
   ##

   pkg load statistics
   global PAR DRATE MONTHS = 1:12
   if isempty(DRATE) DRATE = 0.7 ; endif

   if ~isequal(MONTHS, 1:12)
      disp(MONTHS) ;
   endif
   mean = @nanmean ;

   if ~isfield(PAR, "rec")
      PAR.rec = 1 ;
   endif
   
   p = [PAR.rec 1:PAR.rec-1 PAR.rec+1:ndims(x)] ;
   x = permute(x, p) ;
   N = size(x) ;
   
   if L = (ndims(x) > 2)
      x = reshape(x, N(1), prod(N(2:end))) ;
   endif

   if DRATE <= 1
      if length(unique(idx(:,3))) > 15 # daily
	 nlen = DRATE * 365 ;
      else
	 nlen = DRATE * 12 ;
      endif
   else
      nlen = DRATE ;
   endif

   if (nargin < 3), fun = @nanmean ; endif 

   Im = isempty(MONTHS)(ones(N(1),1)) ;
   for m = MONTHS
      Im = Im | idx(:,2) == m ;
   endfor
      
   Y = idx(1,1):idx(end,1) ;
   y = repmat(NaN,columns(Y), columns(x)) ;
   i = 0 ;
   for iy=Y

      i++ ;
      idy(i,:) = [iy, 1, 1] ;
      if (length(find(I = idx(:,1) == iy)) < nlen)
	 continue
      endif
      w = x(I,:) ;
      I = sum(!isnan(w)) ./ rows(w) > DRATE ; I = repmat(I, rows(w), 1) ;
      w(!I) = nan ;
      J = any(!isnan(w)) ;
      y(i,:) = nan([1 size(w)(2:end)]) ;
      w = feval(fun, w(:,J), varargin{:}) ;
      y(i,J) = w ;

   endfor

   if L, y = reshape(y, [rows(y), N(2:end)]) ; endif

   y = permute(y, perminv(p)) ;
   
endfunction
