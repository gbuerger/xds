function id = dat(idat, j0)

%usage:  id = dat(idat)

   iw = idat;
   if (nargin < 2), j0 = 4; endif 
   ind=ones(rows(idat),j0);
   id=ones(rows(idat),3);

   for j=1:j0
      n = 10^(2*(j0-1-j));
      ind(:,j) = iw(:)./n;
      iw = rem(iw,n);
      id(:,j) = fix(ind(:,j)) ;
   endfor

   if (j0 == 4)
      id(:,j0) = id(:,j0)/100 * 24 ;
   endif 

endfunction
