function [j dst] = findll (ll, LL, dfd)

   ## usage: [j dst] = findll (ll, LL, dfd)
   ## 
   ## find LL coordinates nearest to ll

   pkg load mapping

   dfd0 = 0.25 ;
   n = columns(LL) ;

   if nargin < 3
      dfd = ones(1, n) ;
   endif

   d = cellfun(@(x) distance(flipud(x)', flipud(ll)'), mat2cell(LL, 2, ones(1, n))) ;
   d = d / 180 * 6371 ;
   ##d = sumsq(ll - LL) ;

   d(dfd < dfd0) = Inf ;

   [dst j] = min(d) ;

endfunction
