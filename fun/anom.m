function [y ym ys] = anom (x, xm=[], xs=[], rec=1)

   ## usage:  [y ym ys] = anom(x, xm, xs)
   ## anomalies of x rel. to xm, xs

   pkg load statistics
   
   global REC

   if isempty(REC), REC = rec ; endif

   if (nargin > 1)
      ym = xm ;
   else
      ym = nanmean(x, REC) ;
   endif 

   y = x - ym ;

   if (nargin > 2 || nargout > 2)
      if nargin > 2
	 ys = xs ;
      else
	 ys = nanstd(x, 0, REC) ;
      endif
      y = y ./ ys ;
   endif 

endfunction
