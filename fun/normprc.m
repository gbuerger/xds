function varargout = normprc (P, pin)

   ## usage:  varargout = normprc (P, pin)
   ##
   ## normalize precipitation series P

   global PAR
   pkg load optim statistics

   if ~isfield(PAR, "par_opt") PAR.par_opt = {} ; endif
   if ~isfield(PAR, "cdffit") PAR.cdffit = "wbl" ; endif
   if ~isfield(PAR, "prbraw") PAR.prbraw = false ; endif
   if ~isfield(PAR, "dbg") PAR.dbg = false ; endif
   
   Inanin = isnan(P) ;
   Iinfin = isinf(P) ;

### zscore via NQT ;; FIXME: check Bogner et al. (2012)
   N = size(P) ;
   P = reshape(P, [N(1:2), prod(N(3:end))]) ;
   NP = size(P) ;
   idx.subs = repmat({":"}, 1, ndims(P)) ;

   if (nargin < 2)

      if length(NP) > 2
	 P = permute(P, [1 3 2]) ;
	 P = reshape(P, NP(1)*NP(3), NP(2)) ;
	 CJ = mat2cell(P, NP(1)*NP(3), ones(1, NP(2))) ;
      else
	 CJ = mat2cell(P, NP(1), ones(1, NP(2))) ;	 
      endif
      wrn = warning("query", "Octave:divide-by-zero") ; warning("off", "Octave:divide-by-zero") ;
      [pout Z] = parfun(@(x,j) _normprc({x}), CJ, PAR.par_opt{:}) ;
      warning(wrn.state, "Octave:divide-by-zero") ;
      Z = cell2mat(Z) ;
      if length(N) > 2
	 Z = reshape(Z, NP([1 3 2])) ;
	 Z = permute(Z, [1 3 2]) ;
      endif
      Inanout = isnan(Z) ;
      Iinfout = isinf(Z) ;

      if any((I = (Inanin != Inanout))(:))
	 [i, j] = find(I) ;
	 error("normprc: NaNs created at i, j = %d, %d.\n", i(1), j(1)) ;
      endif
      if any((I = (Iinfin != Iinfout))(:))
      	 [i, j] = find(I) ;
      	 for l=1:length(i)
      	    warning("xds:xds", "normprc: Infs created at i, j = %d, %d.\n", i(l), j(l)) ;
      	 endfor 
      	 Z(I) = norminv(1 - 1/rows(Z)) ; # replace inf with maximum normal
      endif

      varargout{1} = reshape(cell2mat(pout), [], N(2:end)) ;
      varargout{2} = reshape(Z, [], N(2:end)) ;

   else

      if ndims(P) < 3
	 CJ = cell(size(P,2), 1) ;
      else
	 CJ = cell(size(P)(2:end)) ;
      endif
      for i=1:size(P,2)
	 idx.subs{2} = i ;
	 wP = subsref(P, idx) ;
%	 wP = squeeze(subsref(P, idx)) ;
	 for j=1:size(wP,3)
	    CJ{i,j} = {wP(:,j), pin(i)} ;
	 endfor
      endfor
      pout = parfun(@(x) _normprc(x), CJ, PAR.par_opt{:}) ;
      Z = reshape(cell2mat(pout), N) ;

      Inanout = isnan(Z) ;
      Iinfout = isinf(Z) ;

      if (any(any(I = (Inanin != Inanout))))
	 [i, j] = find(I) ;
	 error("normprc: NaNs created at i, j = %d, %d.\n", i(1), j(1)) ;
      endif
      I = (Iinfin != Iinfout) ;
      Ij = cell(1, ndims(I)) ;
      if any(I(:))
	 [Ij{:}] = find(I) ;
	 ##for l=1:length(i)
	 ##   warning("xds:xds", "normprc: Infs created at i, j = %d, %d.\n", i(l), j(l)) ;
	 ##endfor 
	 Z(I) = norminv(1 - 1/rows(Z)) ; # replace inf with maximum normal
      endif

      varargout{1} = Z ;

   endif 

endfunction


function [res z] = _normprc (C)

   ## usage:  [res z] = _normprc (C)
   ##
   ## wrapper for normprc

   persistent nfun = 0 ;
   global PAR
   Qzero = 0.1:0.01:0.9 ;

   f = dbstack ;
   nfun++ ;

   P = C{1} ;
   if length(C) > 1, pin = C{2} ; endif 
   nP = size(P) ; z = nan(nP) ;
   
   I = !isnan(P) ;
   P = P(I,:) ; rP = rows(P) ;

   if length(C) == 1
      if rows(P) > 0
	 w = num2cell(P, 1) ;
	 [~,jQ] = max(diff(log(quantile(P, Qzero)))) ;
	 qz = cell2mat(cellfun(@(c) quantile(c(c~=0), Qzero(jQ)), w, PAR.par_opt{1:2})) ;
      else
	 qz = NaN ;
      endif
   else
      qz = pin.qz ;
   endif
   Pm = repmat(qz, rP, 1) ;

   II = (P > Pm) ;
   if any(II)
      x = P(II) ;
      q = sum(!II) ./ rP ;
      x0 = min(x) ;
   else
      x = x0 = NaN ; q = 0 ;
   endif

   [F, IF] = cdf_est(x) ;
   F = (1-q) * F + q ;
   F = norminv(F) ;
   X = [x(IF), F(IF)] ;

   if length(C) == 1

      res.q = q ; res.x0 = x0 ; res.qz = qz ;

      if strcmp(PAR.cdffit, "wbl")
	 p0 = [mean(X(:,1)) 0.5 0.5] ;
      else
	 p0 = [0.01 mean(X(:,1)) std(X(:,1))] ;
      endif

      if any(II)

	 opt = optimset("TolX", 1e-10, "TolFun", 1e-10, "MaxFunEvals", 1000, "AutoScaling", "on") ;

	 [p v] = fminsearch (@(p) cost(p, X, q, x0), p0, opt) ;
	 [v s1] = cost(p, X, q, x0) ;

	 if (s1 > 0.3)
	    [p v] = fminsearch (@(p) cost(p, X, q, x0), p, opt) ;
	    [v s1] = cost(p, X, q, x0) ;
	    if (flag != 1 && s1 > 0.3) # try harder
	       pkg load optim
	       warning("xds:xds", "normprc: fminsearch: (flag, fmin, rms) = (%d, %f, %f)", flag, v, s1) ;
	       opls.ftol = optimget(opt, "TolFun") ;
	       opls.maxev = optimget(opt, "MaxFunEvals") ;
	       [pw v] = fminsearch(@(p) @cost(p, X, q, x0), p, opt) ;
	       [v s1w] = cost(pw, X, q, x0) ;
	       if s1w > 0.3
		  [pw v] = fminsearch(@(p) @cost(pw, X, q, x0), p, opt) ;
		  [v s1w] = cost(p, X, q, x0) ;
		  if s1w > 0.3
		     save -binary err.ob p X q x0 PAR opls PAR
		     prb_print(X, p, q, x0) ;
		     warning("normprc: minimize(%d): cost = %f,\nsaving to err.ob\n", nfun, s1) ;
		  endif
	       endif
	       if s1w < s1, p = pw ; endif
	    endif
	 endif

	 if PAR.dbg
	    prb_print(X, p, q, x0) ;
	    pause(1) ;
	 endif

      else

	 p = nan(size(p0)) ;

      endif

      res.p = p ;

   else

      q = pin.q ; x0 = pin.x0 ; p = pin.p ;

   endif

   if any(II)
      fI = find(I)(II) ;
      if PAR.prbraw
	 z(fI) = F ;
      else
	 z(fI) = prb(PAR.cdffit, x, p, q, x0) ;
      endif 
      if ~isreal(z(fI))
	 error("normprc: complex zscores.\n") ;
      endif 
   endif

   fI = find(I)(~II) ;
   z(fI) = trunc_rnd("norm", sum((!II)(:)), -Inf, norminv(q)) ;

   if (length(C) > 1)
      res = z ;
   endif 

endfunction


function prb_print (X, p, q, x0, pfile)

   ## usage:  prb_print (X, p, q, x0, pfile)
   ##
   ## print probit estimate

   persistent nprint = 0 ;
   global PAR PRJ BATCH ;
   nprint++ ;

   if (nargin < 5)
      pdir = fullfile("data", PRJ, PAR.pdd) ;
      pfile = fullfile(pdir, ["prc_" num2str(nprint) ".png"]) ;
   endif

   if BATCH
      if exist(pfile, "file")
	 return ;
      else
	 figure("visible", "off") ;
      endif
   endif
   subplot(2,1,1) ;
   line(X(:,1), X(:,2), "linestyle", "none", "marker", "o", "markersize", 4) ;
   line(X(:,1), feval("prb", PAR.cdffit, X(:,1), p, q, x0), "linestyle", "-", "marker", "none") ;
   xlabel("phys.") ; ylabel("norm.") ;
   subplot(2,1,2) ;
   line(X(:,2), X(:,1), "linestyle", "none", "marker", "o", "markersize", 4) ;
   line(X(:,2), feval("prb_inv", PAR.cdffit, X(:,2), p, q, x0), "linestyle", "-", "marker", "none") ;
   xlabel("norm.") ; ylabel("phys.") ;

   if exist(pdir, "dir")
      printf("prb_print: --> %s\n", pfile) ;
      print(pfile) ;
   endif

   clf ;
   
endfunction
