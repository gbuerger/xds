function [ I ] = valsel (x)

   n = length(x) ; I = nan(n,1) ;
   j = 1 ; I(j) = j ;
   for i=2:n
      if (x(i) != x(i-1))
	 I(++j) = i ;
      endif 
   endfor
   I = I(1:j) ;

endfunction
