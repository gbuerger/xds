function u = mltadj (v)

   ## usage: y = mltadj (v)
   ## 
   ## multivariate covariance adjustment

   global PAR isCUR ADIR CDIR SDIR

   u = v ;

   printf("app: adjusting multi-field covariance\n") ;
   mfile = fullfile(CDIR, "pc.m.ob") ;
   if isCUR
      uw = selper(u, PAR.cper(1,:), PAR.cper(2,:)) ;
      wm = mean(uw.x) ;
      save(mfile, "wm")
      printf("app: --> %s\n", mfile) ; 
      w = anom(u.x, wm) ;
   else
      if strfind(PAR.ptradj, ".loc")
	 [w wm] = anom(u.x) ;
      else
	 load(mfile) ;
	 printf("app: <-- %s\n", mfile) ; 
	 w = anom(u.x, wm) ;
      endif
   endif

   printf("app: <-- %s\n", gfile = fullfile(ADIR, "G.ob")) ;
   load(gfile) ; Ga = G ;
   printf("app: <-- %s\n", gfile = fullfile(CDIR, "G.ob")) ;
   load(gfile) ; Gc = G ;
   w = covadj(w, Ga, Gc) ;
   u.x = repmat(wm, rows(w), 1) + w ;

endfunction
