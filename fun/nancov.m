## usage: c = nancov (x)
##
## estimate covariance in the presence of NaNs
function c = nancov (x)

   ## usage: c = nancov (x)
   ##
   ##

   I = ~isnan(x) ;

   II = I' * I ;
   I = II > 0 ;

   c = nan(size(I)) ;
   c(I) = nanmult(x', x)(I) ./ (II)(I) ;
   c(~I) = NaN ;

endfunction
