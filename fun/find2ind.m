function I = find2ind (p, n)

   ## usage:  I = find2ind (p, n)
   ##
   ## characteristic function of {p}

   if (nargin < 2), n = length(p) ; endif 

   I = false(1, n) ;
   I(p) = true ;

endfunction
