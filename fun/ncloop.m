## usage: x = ncloop (ncfile, nv, Jx, Jy, Jl)
##
## split ncreads for large files
function x = ncloop (ncfile, nv, Jx, Jy, Jl)

   global idx PAR
   
   nvd = {nv.Dimensions.Name} ;
   dlon = find(strncmp(nvd, "lon", 3)) ;
   dlat = find(strncmp(nvd, "lat", 3)) ;
   dlev = find(strncmp(nvd, "lev", 3)) ;

   jd = find(~strncmp(nvd, "lon", 3) & ~strncmp(nvd, "lat", 3) & ~strncmp(nvd, "lev", 3)) ;

   d = count = nv.Size ;

   start = stride = ones(1,length(d)) ;
   for k = {"lon" "lat" "lev"}
      if isfield(PAR, "stride") && isfield(PAR.stride, k{:})
	 eval(sprintf("stride(d%s) = PAR.stride.%s ;", k{:}, k{:})) ;
	 eval(sprintf("count(d%s) /= PAR.stride.%s ;", k{:}, k{:})) ;
      endif
   endfor
   start(dlon) = Jx(1) ; count(dlon) = length(Jx) ;
   start(dlat) = Jy(1) ; count(dlat) = length(Jy) ;
   if ~isempty(dlev)
      start(dlev) = Jl(1) ; count(dlev) = length(Jl) ;
   endif

   if Llon = (Jx(1) > Jx(end))
      start1 = start2 = start ; count1 = count2 = count ;
      jlon = find(Jx == 1) ;
      start1(dlon) = Jx(1) ; count1(dlon) = jlon - 1 ;
      start2(dlon) = Jx(jlon) ; count2(dlon) = length(Jx) - jlon + 1 ;
   else
      start(dlon) = Jx(1) ; count(dlon) = length(Jx) ;
   endif
   start(dlat) = Jy(1) ; count(dlat) = length(Jy) ;
   if ~isempty(dlev)
      start(dlev) = Jl(1) ; count(dlev) = length(Jl) ;
   endif

   [~,k] = computer ;
   k /= sizeof(1.0) ;
   k /= 10 ;                    # unclear calc02 behavior
   n = floor(k*count(jd)/prod(count)) ;

   x = [] ; wc = count ;
   while start(jd) <= count(jd)

      wc(jd) = min(n, count(jd)-start(jd)+1) ;
      if Llon
	 wc1 = wc2 = wc ;
	 wc1(dlon) = count1(dlon) ;
	 wc2(dlon) = count2(dlon) ;
      endif
      
##      j = 0 ;
##      while j++ < 1
##	 try
	    if Llon
	       w1 = ncread(ncfile, nv.Name, start1, wc1, stride) ;
	       w2 = ncread(ncfile, nv.Name, start2, wc2, stride) ;
	       w = cat(dlon, w1, w2) ;
	    else
	       w = ncread(ncfile, nv.Name, start, wc, stride) ;
	    endif
##	    break ;
##	 catch
##	 end_try_catch
##      endwhile
##      if j > 1
##	 if L
##	    ncfile = strrep(ncfile, "dodsC", "fileServer") ;
##	    warning("xds:xds", "file not found: %s", ncfile) ;
##	    urlwrite(ncfile, ncfile = tmpnam) ;
##	    printf("ncloop> --> %s\n", ncfile) ;
##	    w = ncread(ncfile, nv.Name, start, wc, stride) ;
##	 else
##	    error("xds:xds", "file not found: %s", ncfile) ;
##	 endif
##      endif
      x = cat(jd, x, w) ;
      start(jd) += stride(jd) .* wc(jd) ;
   endwhile
   x = double(x) ;

   if ~isempty(dsys = find(strncmp(nvd, "sys", 3)))
      x = nsqueeze(x, dsys, 1) ;
   endif
   
endfunction
