function v = pre_pare (u, mode)

   ## usage: v = pre_norm (u, mode)
   ## 
   ## remove/add unit details from/to u.x (back and forth)
   ## FIXME: use beta distribution for most variables

   pkg load statistics optim

   global PRJ PAR clim
   
   cldcdf = @(p,x) betacdf(x, p(1), p(2)) ; p0 = [1 0.5]' ;

   if nargin < 2, mode = "dir" ; endif

   v = u ;

   if strcmp(mode, "dir")

      for j = 1:columns(u.x)

	 Idfd = find(!isnan(u.x(:,j))) ; v.x(:,j) = nan ;
	 y = u.x(Idfd,j) ; name = u.names{j} ; uv = u.vars{j} ;

	 if regexp(name, "cloud")
	    a = 0 ; b = 8 ;
	    [F I] = cdf_est(y=y/b) ;
	    [p, fy, cvg, outp] = nonlin_curvefit(cldcdf, p0, y, F) ; clim.(uv).p{j} = p ;
	    y = betacdf(y, p(1), p(2)) ; Ia = (0 < y) ; Ib = (y < 1) ;
	    y = norminv(y(Ia & Ib)) ; ya = min(y) ; yb = max(y) ;
	    v.x(Idfd(Ia & Ib),j) = y ; v.x(Idfd(!Ia),j) = ya ; v.x(Idfd(!Ib),j) = yb ;
	 elseif regexp(name, "humidity")
	    a = 0 ; b = 100 ;
	    I = (y < b) ;
	    sigma = mean(y=b-y(I)) * sqrt(2/pi) ; clim.(uv).sigma{j} = sigma ;
	    y = raylcdf(y, sigma) ;
	    q = norminv(max(y(y<1))) ;
	    y = norminv(y) ; y(isinf(y)) = nan ;
	    y = -pfy(-y, -q) ;
	    v.x(Idfd(I),j) = y ; v.x(Idfd(!I),j) = min(y) ;
	 elseif regexp(name, "(sun|insol|radiation)")
	    a = 0 ;
	    t = date2toy(u.id(Idfd,:), u.cal)*365 / (2*pi) + 10 ;
	    dl = day_length(t, u.lats(j)) ;
	    I = (y > a) ; q = sum(y<=a) ; q = clim.(uv).q{j} = q / (q + sum(I)) ;
	    y = y(I) ./ dl(I) ;
	    clim.(uv).yx{j} = max(y) ; y /= clim.(uv).yx{j} ;
	    [F II] = cdf_est(y) ;
	    [p, fy, cvg, outp] = nonlin_curvefit(cldcdf, p0, y, F) ; clim.(uv).p{j} = p ;
	    y = q + (1 - q) * cldcdf(p, y) ; Ib = (y < 1) ;
	    y(Ib) = norminv(y(Ib)) ; y(!Ib) = max(y(Ib)) ;
	    v.x(Idfd(!I),j) = trunc_rnd("norm", sum(!I), -Inf, norminv(q)) ;
	    v.x(Idfd(I),j) = y ;
	 elseif regexp(name, "wind")
	    a = 0 ; b = inf ;
	    sigma = mean(y) * sqrt(2/pi) ; clim.(uv).sigma{j} = sigma ;
	    I = (y > a) ;
	    y = raylcdf(y(I), sigma) ;
	    q = norminv(max(y(y<1))) ;
	    y = norminv(y) ; y(isinf(y)) = nan ;
	    y = -pfy(-y, -q) ;
	    clim.(uv).ym{j} = ym = min(y) ;
	    v.x(Idfd(I),j) = y ; v.x(Idfd(!I),j) = ym ;
	 else
	    v.x(:,j) = u.x(:,j) ;
	 endif

      endfor

   else
      for j = 1:columns(u.x)

	 Idfd = find(!isnan(u.x(:,j))) ; v.x(:,j) = nan ;
	 y = u.x(Idfd,j) ;
	 name = u.names{j} ;
	 uv = u.vars{j} ;

	 ## find index within var
	 jj = j - find(strcmp(u.vars, uv))(1) + 1 ;

	 if regexp(name, "cloud")
	    a = 0 ; b = 8 ;
	    p = clim.(uv).p{jj} ;
	    y = b * betainv(normcdf(y), p(1), p(2)) ;
	    v.x(Idfd,j) = max(a, y) ;
	 elseif regexp(name, "humidity")
	    a = 0 ; b = 100 ;
	    sigma = clim.(uv).sigma{jj} ;
	    y = raylinv(normcdf(y), sigma) ;
	    v.x(Idfd,j) = min(b, max(a, b - y)) ;
	 elseif regexp(name, "(sun|insol|radiation)")
	    a = 0 ;
	    t = date2toy(u.id(Idfd,:), u.cal)*365 / (2*pi) + 10 ;
	    dl = day_length(t, u.lats(j)) ;
	    p = clim.(uv).p{jj} ; q = clim.(uv).q{jj} ;
	    y = normcdf(y) ; Ia = (y > q) ;
	    y(Ia) = (y(Ia) - q) / (1-q) ;
	    y(Ia) = betainv(y(Ia), p(1), p(2)) ;
	    y(Ia) = dl(Ia) .* clim.(uv).yx{jj} .* y(Ia) ;
	    y(!Ia) = 0 ;
	    v.x(Idfd,j) = y ;
	 elseif regexp(name, "wind")
	    ym = clim.(uv).ym{jj} ; sigma = clim.(uv).sigma{jj} ;
	    I = (y >= ym) ;
	    y = raylinv(normcdf(y(I)), sigma) ;
	    v.x(Idfd(I),j) = y ; v.x(Idfd(!I),j) = 0 ;
	 else
	    v.x(:,j) = u.x(:,j) ;
	 endif

      endfor

   endif

endfunction
