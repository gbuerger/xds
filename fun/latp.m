function [res p map] = latp (E, elatns, latns)

   ## usage:  [res p map] = latp (E, elatns, latns)
   ##
   ## swap latitudes to north - south

   global PAR

   map = !all(elatns == latns) ;

   if !map
      res = E ; p = (1:length(elatns))' ;
      return ;
   endif

   elatns = (1:length(elatns))'(elatns) ;
   p = elatns(latns) ;

   ll = length(elatns) ;
   [n m] = size(E) ;

   if mod(n, ll)
      error("ptr: latp: incorrect latitude dimensions %d vs. %d.\n", n, elatns) ;
   endif 

   E = reshape(E, ll, fix(n/ll), m)(p,:,:) ;

   res = reshape(E, n, m) ;

endfunction
