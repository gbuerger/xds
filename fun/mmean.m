## usage: res = mmean (id, x, per)
##
## calculate monthly means
function res = mmean (id, x, per)

   global REC

   pkg load statistics
   
   if isempty(REC), REC = 1 ; endif
   if nargin < 3
      per = [-Inf Inf] ;
   endif
   
   idx.type = "()" ;
   idx.subs = repmat({:}, 1, ndims(x)) ;

   for m=1:12
      I = id(:,2) == m & per(1) <= id(:,1) & id(:,1) <= per(2) ;
      idx.subs{REC} = I ;
      res(m,:) = nanmean(subsref(x, idx)) ;
   endfor

endfunction
