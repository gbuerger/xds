function r = trunc_rnd (f, n, a, b)

   ## usage:  r = trunc_rnd (f, n, a, b)
   ##
   ## generate n f random numbers between a and b

   if isscalar(a)
      a = repmat(a, n, 1) ;
   endif
   if isscalar(b)
      b = repmat(b, n, 1) ;
   endif

   u = rand(n,1) ;
   FA = feval([f, "cdf"], a) ;
   FB = feval([f, "cdf"], b) ;
   FI = [f, "inv"] ;
   r = feval(FI, FA + u .* (FB - FA)) ;

endfunction
