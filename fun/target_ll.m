function [lon lat] = target_ll (nlon, nlat, gausslat)

   ## usage: [lon lat] = target_ll (nlon, nlat, gausslat)
   ## 
   ## calculate target grid

   global PAR

   lon = linspace(0, 360, nlon+1)'(1:end-1) ;
   if gausslat
      lat = gauss2lats(nlat)' ; lat = [-lat ; flipud(lat)] ;
   else
      ## d = (180 / nlat) / 2 ;
      d = 0 ;
      lat = linspace(-90+d, 90-d, nlat+1)' ;
   endif

   lon(lon > 180) = lon(lon > 180) - 360 ;

   [~,lon] = selJ(lon, PAR.lon, 1e-2) ;
   if ~isempty(j=find(diff(lon) < 0)) ; # wrap longitudes
      Jlon = [j+1:length(lon) 1:j] ;
      lon = lon(Jlon) ;
   endif
   [~,lat] = selJ(lat, PAR.lat, 1e-2) ;

endfunction
