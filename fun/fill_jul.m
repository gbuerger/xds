function I = fill_jul (varargin)

   ## usage: I = fill_jul (varargin)
   ## 
   ## fill Julian dates

   d = inf ; I0 = inf ; I1 = -inf ;
   for i = 1:nargin
      Ju = varargin{i} ;
      dJu = unique(diff(Ju)) ;
      c = arrayfun(@(x) sum(dJu==x), dJu) ;
      d = min(d, dJu(find(c == max(c))(1))) ;
      if d < 1, d = 1 / round(1/d) ; endif
      I0 = min(I0, Ju(1)) ;
      I1 = max(I1, Ju(end)) ;
   endfor

   if numel(unique(datevec(Ju)(:,2))) == 1 && numel(unique(datevec(Ju)(:,3))) == 1
      I = (datevec(I0)(1,1):datevec(I1)(1,1))' ;
      I = datenum([I ones(rows(I),2)]) ;
   else
      I = datenum(datevec(I0:d:I1)) ;
   endif

endfunction
