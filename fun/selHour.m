## usage: res = selHour (s, h)
##
## select specific hour h of day
function res = selHour (s, h)

   global PAR

   if ~isfield(s, "id") && ~isfield(s, "x")
      for [v k] = s
	 res.(k) = selHour(v) ;
      endfor
   endif

   res = s ;
   
   if columns(s.id) < 4 || ~any(I = s.id(:,4) == h)
      continue ;
   endif

   res.id = s.id(I,:) ;
   if isfield(s, "id_ref")
      res.id_ref = s.id_ref(I,:) ;
   endif

   Ix = repmat({:}, 1, ndims(s.x)) ;
   Ix{PAR.rec} = I ;
   res.x = s.x(Ix{:}) ;

endfunction
