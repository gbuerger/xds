## usage: mwrite_stat (odir, s, sfx)
##
## write structure s to output dir odir
function mwrite_stat (odir, s, sfx="")

   global PAR isCUR lim
   pkg load statistics
   
   if isfield(s, "udf")
      s.x = nan2lim(s.x, s.udf) ;
   else
      s.udf = repmat(lim, 1, length(s.vars)) ;
      s.x = nan2lim(s.x) ;
   endif

   nid = columns(s.id) ; fmtid = [repmat("%d,", 1, nid-1) "%d"] ;
   fmthdr = "Y,M,D,H,M,S"(1:2*nid-1) ;
   
   for v = sunique(s.vars)

      v = v{:} ;
      J = strcmp(s.vars, v) ;
      PAR.idx.subs = repmat({":"}, 1, ndims(s.x)) ;
      if 0
	 source ~/oct/oc/ssn.m
	 PAR.idx.subs{PAR.rec} = ssn(s.id, 1:8) ;
      endif
      PAR.idx.subs{2} = J ;
      w = subsref(s.x, PAR.idx) ; Nw = size(w) ;
      wlim = s.udf(J) ;

      if length(Nw) > 2

	 for em=1:Nw(3)

	    sem = sprintf("%02d", em) ;
	    pmkdir(edir=sprintf("%s/%s", odir, sem)) ;

	    if isfield(s, "id_ref")

	       s.rt = datenum(s.id_ref) ;
	       RT = unique(s.rt)' ;
	       for rt = RT
		  I = s.rt == rt ;
		  ofile = sprintf("%s/%s/%s.%s.csv", odir, sem, [sfx v], datestr(rt, 29)) ;
		  fid = fopen(ofile, "wt") ;
		  fmt = [fmthdr repmat(",%s", 1, Nw(2)) "\n"] ;
		  fprintf(fid, fmt, s.ids{J}) ;
		  fmt = [fmtid repmat(["," PAR.ofmt], 1, Nw(2)) "\n"] ;
		  ww = w(I,:,em) ;
		  fprintf(fid, fmt, [s.id(I,:) ww]') ;
		  fclose(fid) ;
	       endfor
	       
	    else

	       ofile = sprintf("%s/%s/%s.csv", odir, sem, [sfx v]) ;
	       fid = fopen(ofile, "wt") ;
	       fmt = [fmthdr repmat(",%s", 1, sum(J)) "\n"] ;
	       fprintf(fid, fmt, s.ids{J}) ;
	       fmt = [fmtid repmat(["," PAR.ofmt], 1, sum(J)) "\n"] ;
	       ww = w(:,:,em) ;
	       I = PAR.idx.subs{PAR.rec} ;
	       fprintf(fid, fmt, [s.id(I,:) ww]') ;
	       fclose(fid) ;

	    endif

	 endfor

	 if 0
	    for q = [0.05 0.5 0.95]

	       sem = sprintf("q%02d", 100*q) ;
	       pmkdir(edir=sprintf("%s/%s", odir, sem)) ;

	       if isfield(s, "id_ref")

		  s.rt = datenum(s.id_ref) ;
		  RT = unique(s.rt)' ;
		  for rt = RT
		     I = s.rt == rt ;
		     ofile = sprintf("%s/%s/%s.%s.csv", odir, sem, [sfx v], datestr(rt, 29)) ;
		     fid = fopen(ofile, "wt") ;
		     fmt = [fmthdr repmat(",%s", 1, Nw(2)) "\n"] ;
		     fprintf(fid, fmt, s.ids{J}) ;
		     fmt = [fmtid repmat(["," PAR.ofmt], 1, Nw(2)) "\n"] ;
		     printf("out: --> %s\n", ofile) ;
		     ww = quantile(w(I,:,:), q, 3) ;
		     fprintf(fid, fmt, [s.id(I,:) ww]') ;
		     fclose(fid) ;
		  endfor
		  
	       else

		  ofile = sprintf("%s/%s/%s.csv", odir, sem, [sfx v]) ;
		  fid = fopen(ofile, "wt") ;
		  fmt = [fmthdr repmat(",%s", 1, sum(J)) "\n"] ;
		  fprintf(fid, fmt, s.ids{J}) ;
		  fmt = [fmtid repmat(["," PAR.ofmt], 1, sum(J)) "\n"] ;
		  printf("out: --> %s\n", ofile) ;
		  ww = quantile(w, q, 3) ;
		  I = PAR.idx.subs{PAR.rec} ;
		  fprintf(fid, fmt, [s.id(I,:) ww]') ;
		  fclose(fid) ;

	       endif

	    endfor
	 endif

      else

	 pmkdir(odir) ;
	 if ~isCUR && isfield(s, "id_ref")

	    s.rt = datenum(s.id_ref) ;
	    RT = unique(s.rt)' ;
	    for rt = RT
	       I = s.rt == rt ;
	       ofile = sprintf("%s/%s.%s.csv", odir, [sfx v], datestr(rt, 29)) ;
	       printf("out: --> %s\n", ofile) ;
	       fid = fopen(ofile, "wt") ;
	       fmt = [fmthdr repmat(",%s", 1, Nw(2)) "\n"] ;
	       fprintf(fid, fmt, s.ids{J}) ;
	       fmt = [fmtid repmat(["," PAR.ofmt], 1, Nw(2)) "\n"] ;
	       fprintf(fid, fmt, [s.id(I,:) w(I,:)]') ;
	       fclose(fid) ;
	    endfor
	    
	 else

	    ofile = sprintf("%s/%s.csv", odir, [sfx v]) ;
	    printf("out: --> %s\n", ofile) ;
	    fid = fopen(ofile, "wt") ;
	    fmt = [fmthdr repmat(",%s", 1, sum(J)) "\n"] ;
	    fprintf(fid, fmt, s.ids{J}) ;
	    fmt = [fmtid repmat(["," PAR.ofmt], 1, sum(J)) "\n"] ;
	    I = PAR.idx.subs{PAR.rec} ;
	    fprintf(fid, fmt, [s.id(I,:) w]') ;
	    fclose(fid) ;

	 endif

      endif

      ## output min and max
      if PAR.dbg
	 w(w == wlim) = NaN ;
	 xq = quantile(w(:), [0 0.1 0.5 0.9 1]) ;
	 xm = nanmean(w(:)) ;
	 fid = fopen(fullfile(odir, [sfx v, ".stat.csv"]), "at") ;
	 fprintf(fid, ["M:\t%7.1f | Q:\t%7.1f\t%7.1f\t%7.1f\t%7.1f\t%7.1f\n"], xm, xq) ;
	 fclose(fid) ;
	 printf("out: --> %s\n", fullfile(odir, [sfx v, ".stat.csv"])) ;
      endif   

   endfor
   
endfunction
