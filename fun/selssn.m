function varargout = selssn (ssn="all", varargin)

   ## usage: varargout = selssn (ssn, varargin)
   ## 
   ## select seasons from ids

   if ischar(ssn)
      
      switch ssn
	 case "all"
	    ssn = 1:12 ;
	 case "win"
	    ssn = [10:12 1:3] ;
	 case "sum"
	    ssn = 4:9 ;
	 case "mam"
	    ssn = 3:5 ;
	 case "jja"
	    ssn = 6:8 ;
	 case "son"
	    ssn = 9:11 ;
	 case "djf"
	    ssn = [12 1 2] ;
	 otherwise
	    ssn = 1:12 ;
      endswitch

   endif

   for i = 1:nargout
      id = varargin{i} ;
      I = any(id(:,2) == ssn, 2) ;
      varargout{i} = I ;
   endfor

endfunction
