function y = nsqueeze (x, d, j)

   ## usage:  y = nsqueeze (x, d, j)
   ##
   ## pick j-th value of dimension d from x

   idx.type = "()" ;
   idx.subs = repmat({:}, 1, ndims(x)) ;
   idx.subs{d} = j ;

   y = squeeze(subsref(x, idx)) ;

endfunction
