## usage: [v vlat] = orderlat (w, dlat, wlat)
##
## order w/ latitudes increasing
function [v vlat] = orderlat (w, dlat, wlat)
   if any(diff(wlat) > 0)
      if ~all(diff(wlat) > 0)
	 error("xds:xds", "orderlat: check latitudes") ;
      endif
      v = w ;
      vlat = wlat ;
      return ;
   else
      if isrow(wlat)
	 vlat = fliplr(wlat) ;
      else
	 vlat = flipud(wlat) ;
      endif
      v = flip(w, dlat) ;
   endif
   
endfunction
