function s = read_stat (file)

   ## usage:  s = read_stat (file)
   ##
   ## read station data

   ## repair dos files
   if isempty(stat(file))
      error("read_stat: %s not found", file) ;
   endif
   if ~system(sprintf("file %s | grep -q CRLF", file))
      file = dos2unix(file) ;
   endif
   
   fid = fopen(file, "rt") ;
   hdr = strsplit(shdr=fgetl(fid), ",") ;
   nhdr = 0 ;
   while strncmp(shdr, "Y,M,D,H,M,S,", ++nhdr)
   endwhile
   nhdr = (nhdr-1) / 2 ;
   fclose(fid) ;
   
   HD = false ; nHD = 1 ;
   for j=1:columns(hdr)
      if !isnumeric(hdr{j}), HD = true ; endif 
   endfor

   if HD
      nHD = 2 ;
      s.ids = strtrim(hdr(nhdr+1:end)) ;
   endif

   c = dlmread(file, ",")(2:end,:) ;
   
   if strcmp(hdr{4}, "Yr")
      s.id = c(:,1:3) ;
      s.id_ref = c(:,4:6) ;
      s.x = c(:,7:end) ;
   else
      s.id = c(:,1:nhdr) ;
      s.x = c(:,nhdr+1:end) ;
   endif
   s.x = lim2nan(s.x) ;

   [~,v,~] = fileparts(file) ;
   s.vars = repmat({v}, 1, columns(s.x)) ;
   
endfunction
