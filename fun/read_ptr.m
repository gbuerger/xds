function ptr = read_ptr (gdir)

   ## usage:  ptr = read_ptr (gdir)
   ##
   ## read predictor fields

   pkg load statistics
   global isANA PAR

   plist = glob({fullfile(gdir, "*.nc"), fullfile(gdir, "*/*.nc")})' ;
   if exist(pfile = fullfile(gdir, "ptr.lst"), "file")
      printf("read_ptr: <-- %s\n", pfile) ;
      plist = {plist{:}, expand_plist(pfile, [], ~true){:}} ;
   endif
   if isempty(plist)
      error("read_ptr: no matching files in %s\n", fullfile(gdir, "ptr.lst")) ;
   endif

   if PAR.waitbar, hw = waitbar(0, "read\\_ptr") ; endif
   ptr = [] ; f = tempname ;
   for j = 1:(npl=length(plist))

      if PAR.waitbar, waitbar(j/npl, hw) ; endif
      ncfile = plist{j} ;

      [file lfile] = nc2ob(ncfile, gdir) ;
      pmkdir(fileparts(file)) ;
      if exist(lfile, "file") == 2
	 ncfile = lfile ;
      endif
      
      if isnewer(file, ncfile)
	 printf("read_ptr: <-- %s\n", file) ;
	 load(file)
##	 if regexp(file, "sfc")
##	    w = to_daily(w) ;
##	    printf("read_ptr: --> %s\n", file) ;
##	    save(file, "w") ;
##	 endif
      else
	 printf("read_ptr: <-- %s\n", ncfile) ;
	 if isempty(stat(ncfile))
	    nbytes = 0 ; # url
	 else
	    nbytes = round(stat(ncfile).size) ; # local
	 endif
	 if false && ((k = ceil(nbytes/(0.5*PAR.mem))) > 1)
 	    if system("which ncks", 1)
	       error("ptr: need nco package. Exiting.\n") ;
	    endif 
	    if PAR.dbg, printf("read_ptr: splitting %s into about %d pieces.\n", ncfile, k+1) ; endif
	    w = nco_GCM(ncfile, k) ;
	 else
	    w = read_GCM(ncfile) ;
	    if isempty(w)
	       continue ;
	    endif
	 endif
	 printf("read_ptr: --> %s\n", file) ;
	 save(file, "w") ;
      endif

      ptr = join_ptr(ptr, w) ;

   endfor
   if PAR.waitbar, close(hw) ; endif

endfunction
