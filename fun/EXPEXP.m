function y = EXPEXP (x, p)

   ## usage:  y = EXPEXP (x, p)
   ##
   ## inverse of probit fit function

   y = p(3) * log(log((x-p(1))/p(2)) + e) ;

endfunction
