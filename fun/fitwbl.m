## usage: p = fitwbl (x, F)
##
## fit Weibull parameters using leased squares
function p = fitwbl (x, F)
   u = log(x) ;
   v = log(-log(1-F)) ;

   P = polyfit(u, v, 1) ;

   p(1) = exp(-P(2)/P(1)) ;
   p(2) = P(1) ;
   
endfunction
