function res = cat_idx (nv, idx)

   ## usage:  res = cat_idx (nv, idx)
   ##
   ## concatenate netcdf arrays

   for j=1:numel(idx)

      if strcmp(J = idx{j}, ":") 
   	 widx{j}{1} = J ;
   	 continue
      endif ;
      kJ = find(diff(J) < 0) ;

      if isempty(kJ)
   	 widx{j}{1} = [num2str(J(1)) ":" num2str(J(end))] ;
      else
   	 widx{j}{1} = [num2str(J(1)) ":" num2str(J(kJ))] ;
   	 widx{j}{2} = [num2str(J(kJ+1)) ":" num2str(J(end))] ;
      endif

   endfor

   for j = 1:numel(idx)
      n(j) = numel(widx{j}) ;
   endfor

   res = [] ; cI = cell(numel(idx),1) ; I0 = zeros(1, numel(idx)) ;
   for j = 1:prod(n)
      [cI{:}] = ind2sub(n, j) ; I = cell2mat(cI)' ;
      J = "" ;
      for i = 1:numel(idx)
   	 J = [J widx{i}{I(i)} ","] ;
      endfor
      J = J(1:end-1) ;
      eval(["w = nv(" J ") ;"]) ;

      i0 = find(I != I0)(1) ; I0 = I ;
      res = cat(i0, res, w) ;
   endfor
 
endfunction
