function y = EXPEXP_inv (x, p)

   ## usage:  y = EXPEXP_inv (x, p)
   ##
   ## inverse of probit fit function

   y = p(1) + p(2) * exp(exp(x/p(3)) - e) ;
   y = max(0, y) ;

endfunction
