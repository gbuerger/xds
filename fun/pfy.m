function res = pfy (z, q)

   ## usage:  res = pfy (z, q)
   ##
   ## purify truncated normal

   global PAR

   ##persistent ipfy = 0 ;

   ##if (++ipfy >= PAR.pfy(1))
   ##   res = z ;
   ##   warning("xds:xds", "pfy> skipped execution") ;
   ##   return ;
   ##endif

   res = z ;
   n = rows(z) ;
   C = nancov(z) ;
   z0 = repmat(norminv(q), n, 1) ;
   
   if (sum((z <= z0)(:)) / numel(z) < PAR.pfy(2)), return ; endif 

   z0 = norminv(q) ; nz = length(z0) ;
   mu = zeros(nz, 1) ; A = eye(nz) ;

   if nz < 2
      z = rmvnrnd(mu, C, n, A, z0') ;
   else
      [Xs, bounce_count] = HMC_exact(-A, z0', C, mu, ~true, n, z0'-abs(z0'/2)) ;
      z = Xs' ;
   endif
   
   ##[z flag] = gen1Z(C, z0) ;

   ##if !flag
   ##   z = gen2Z(C, z0) ;
   ##   if PAR.dbg, printf("pfy: using gen2Z.\n") ; endif
   ##endif

   I = all(res <= z0, 2) ;   
   res(I,:) = z(I,:) ;

endfunction


function [Xs, bounce_count] = HMC_exact(F, g, M, mu_r, cov, L, initial_X)

% Implementation of the algorithm described in http://arxiv.org/abs/1208.4118
% Author: Ari Pakman



% Returns samples from a d-dimensional Gaussian with m constraints given by  F*X+g >0 
% If cov == true
% then M is the covariance and the mean is mu = mu_r 
% if cov== false 
% then M is the precision matrix and the log-density is -1/2 X'*M*X + r'*X

  % Input
  % F:          m x d matrix
  % g:          m x 1 vector 
  % M           d x d matrix, must be symmmetric and definite positive
  % mu_r        d x 1 vector. 
  % cov:        see explanation above 
  % L:          number of samples desired
  % initial_X   d x 1 vector. Must satisfy the constraint.


		% Output
		% Xs:      d x L matrix, each column is a sample
		% bounce_count:  number of times the particle bounced 

   %% go to a whitened frame

   m = size(g,1);
   if size(F,1) ~= m
      display('error');
      return
   endif


   if cov 
      mu= mu_r;
      g = g + F*mu;
      R = chol(M);
      F = F*R';
      initial_X= initial_X -mu;
      initial_X = R'\initial_X;
      
   else
      r=mu_r;
      R=chol(M);      % M = R'*R    
      mu = R\(R'\r);
      g = g+F*mu;
      F = F/R;               % this is the time-consuming step in this code section
      initial_X= initial_X -mu;
      initial_X = R*initial_X;    
   endif



   
   d = size(initial_X,1);
   Xs=NaN;
   bounce_count =0;
   nearzero= 10000*eps;


				% Verify that initial_X is feasible
   c= F*initial_X +g;
   if any(c<0)
      display('error: inconsistent initial condition');
      return
   endif


   
				% Unsparcify the linear constraints
   g=full(g);
   F2 = sum(F.*F,2);  % squared norm of the rows of F, needed for reflecting the velocity
   F=full(F);         % if we don't unsparcify  qj= F(j,:)*V/F2(j) becomes very slow.
   Ft = F';
   

   %% Sampling loop


   last_X= initial_X;
   Xs=zeros(d,L);
   Xs(:,1)=initial_X;

   i=2;
   while (i <= L)
      i;
      stop=0;   
      j=0;
      V0= normrnd(0,1, d,1);   % initial velocity
      X = last_X;

      T=pi/2;                  % total time the particle will move
      tt=0;                    % records how much time the particle already moved 

      while (1) 
         a = V0; 
         a= real(a);
         b = X;

         fa = F*a;
         fb = F*b;
         
         U = sqrt(fa.^2 + fb.^2);
         phi = atan2(-fa,fb);           % -pi < phi < +pi    



         pn = abs(g./U)<=1; % these are the walls that may be hit 


			 % find the first time constraint becomes zero

         if any(pn) 
            inds = find(pn);

            phn= phi(pn);

            t1=-phn + acos(-g(pn)./U(pn));  % time at which coordinates hit the walls 
% this expression always gives the correct result because U*cos(phi + t) + g >= 0.


			    % if there was a previous reflection (j>0)
% and there is a potential reflection at the sample plane                                    
% make sure that a new reflection at j is not found because of numerical error
            if j>0    
               if pn(j)==1    
                  
                  cs = cumsum(pn);
                  indj=cs(j);
                  tt1 = t1(indj);
                  if abs(tt1) < nearzero || abs(tt1-2*pi)< nearzero
                     t1(indj)=Inf;
                  endif
               endif
            endif


            [mt, m_ind] = min(t1);

				% find the reflection plane 
            j = inds(m_ind);      % j is an index in the full vector of dim-m, not in the restriced vector determined by pn.

         else  %if pn(i) =0 for all i
            mt =T;
         endif %if pn(i)

         tt=tt+mt;
				%        fprintf(num2str(tt/T));

         if tt>=T
            mt= mt-(tt-T);
            stop=1;

         endif

				% move the particle a time mt

         X = a*sin(mt) + b*cos(mt);
         V = a*cos(mt) - b*sin(mt);

         if stop                    
            break;
         endif

				% compute reflected velocity

         qj=  F(j,:)*V/F2(j);   
         V0 = V -2*qj*Ft(:,j);
         bounce_count = bounce_count+ 1;

         

      endwhile % while(1)

 % at this point we have a sampled value X, but due to possible
 % numerical instabilities we check that the candidate X satisfies the
 % constraints before accepting it.
      
      if all(F*X +g > 0)
         Xs(:,i)=X;
         last_X = X;
         i= i+1;
	 
      else
	 disp('hmc reject')
         
      endif

   endwhile %while (i <= L)

			      % transform back to the unwhitened frame
   if cov
      Xs = R'*Xs  + repmat(mu, 1,L);   
   else
      Xs = R\Xs  + repmat(mu, 1,L);   
   endif




endfunction


function [X, rho, nar, ngibbs] = rmvnrnd(mu,sigma,N,A,b,rhoThr)
   %RMVNRND Draw from the  truncated multivariate normal distribution.
   %   X = rmvnrnd(MU,SIG,N,A,B) returns in N-by-P matrix X a
   %   random sample drawn from the P-dimensional multivariate normal
   %   distribution with mean MU and covariance SIG truncated to a
%   region bounded by the hyperplanes defined by the inequalities Ax<=B. If
%   A and B are ommitted then the sample is drawn from the unconstrained
%   MVN distribution.
%
%   [X,RHO,NAR,NGIBBS]  = rmvnrnd(MU,SIG,N,A,B) returns the
%   acceptance rate RHO of the accept-reject portion of the algorithm
%   (see below), the number NAR of returned samples generated by
%   the accept-reject algorithm, and the number NGIBBS returned by
%   the Gibbs sampler portion of the algorithm.
%
%   rmvnrnd(MU,SIG,N,A,B,RHOTHR) sets the minimum acceptable
%   acceptance rate for the accept-reject portion of the algorithm
%   to RHOTHR. The default is the empirically identified value
%   2.9e-4.
%
%   ALGORITHM
%   The function begins by drawing samples from the untruncated MVN
%   distribution and rejecting those which fail to satisfy the
%   constraints. If, after a number of iterations with escalating
%   sample sizes, the acceptance rate is less than RHOTHR it
%   switches to a Gibbs sampler. 
%
% ACKNOWLEDGEMENT
%   This makes use of TruncatedGaussian by Bruno Luong (File ID:
%   #23832) to generate draws from the one dimensional truncated normal.
%
%   REFERENCES
%   Robert, C.P, "Simulation of truncated normal variables",
%   Statistics and Computing, pp. 121-125 (1995).




    % Copyright 2011 Tim J. Benham, School of Mathematics and Physics,
    %                University of Queensland.

				%
				% Constant parameters
				%
   defaultRhoThr = 2.9e-4;                   % min. acceptance rate to
                                % accept accept-reject sampling.
				%
				% Process input arguments.
				%
   if nargin<6 || rhoThr<0,rhoThr = defaultRhoThr;endif

				% If no constraints default to mvnrnd
   if nargin<5
      rho = 1; nar = N; ngibbs = 0;
      X = mvnrnd(mu, sigma, N);
      return
   endif
   
   A = A'; b = b';
   mu = mu';

   p = length(mu);                         % dimension
   m = size(A,2);                          % number of constraints
   if m==0
      A = zeros(p,1);
   endif

				% initialize return arguments
   X = zeros(N,p);                         % returned random variates
   nar = 0;                                % no. of X generated by a-r
   ngibbs = 0;
   rho = 1; 

   if rhoThr<1
				% Approach 1: accept-reject method
      n = 0; % no. accepted
      maxSample = 1e6;
      trials = 0; passes = 0;
      s = N;
      while n<N && ( rho>rhoThr || s<maxSample)
         R  = mvnrnd(mu,sigma,s);
         R = R(sum(R*A<=repmat(b,size(R,1),1),2)==size(A,2),:);
         if size(R,1)>0
            X((n+1):min(N,(n+size(R,1))),:) = R(1:min(N-n,size(R,1)),:);
            nar = nar + min(N,(n+size(R,1))) - n;
         endif
         n = n + size(R,1); trials = trials + s;
         rho = n/trials;
         if rho>0
            s = min([maxSample,ceil((N-n)/rho),10*s]);
         else
            s = min([maxSample,10*s]);
         endif
         passes = passes + 1;
      endwhile
   endif

		       %
		       % Approach 2: Gibbs sampler of Christian Robert
		       %
   if nar < N
				% choose starting point
      if nar > 0
         x = X(nar,:);
         discard = 0;
      else
         x = mu;
         discard = 1;
      endif
				% set up inverse Sigma
      SigmaInv = inv(sigma);
      n = nar;
      while n<N
				% choose p new components
         for i = 1:p
    % Sigmai_i is the (p − 1) vector derived from the i-th column of Σ
    % by removing the i-th row term.
            Sigmai_i = sigma([1:(i-1) (i+1):p],i);
             % Sigma_i_iInv is the inverse of the (p−1)×(p−1) matrix
             % derived from Σ = (σij ) by eliminating its i-th row and
             % its i-th column 
            Sigma_i_iInv = SigmaInv([1:(i-1) (i+1):p],[1:(i-1) (i+1):p]) - ...
			   SigmaInv([1:(i-1) (i+1):p],i)*SigmaInv([1:(i-1) (i+1):p],i)' ...
			   / SigmaInv(i,i);
             % x_i is the (p-1) vector of components not being updated
             % at this iteration. /// mu_i
            x_i = x([1:(i-1) (i+1):p]);
            mu_i = mu([1:(i-1) (i+1):p]);
       % mui is E(xi|x_i)
       %        mui = mu(i) + Sigmai_i' * Sigma_i_iInv * (x_i - mu_i);
            mui = mu(i) + Sigmai_i' * Sigma_i_iInv * (x_i' - mu_i');
            s2i = sigma(i,i) - Sigmai_i'*Sigma_i_iInv*Sigmai_i;
            % Find points where the line with the (p-1) components x_i
            % fixed intersects the bounding polytope.
            % A_i is the (p-1) x m matrix derived from A by removing
            % the i-th row.
            A_i = A([1:(i-1) (i+1):p],:);
				% Ai is the i-th row of A
            Ai = A(i,:);
            c = (b-x_i*A_i)./Ai;
            lb = max(c(Ai<0));
            if isempty(lb), lb=-Inf; endif
            ub = min(c(Ai>0));
            if isempty(ub), ub=Inf; endif
            
            %% test feasibility of using TruncatedGaussian
            % lbsig = (lb-mui)/sqrt(s2i);
            % ubsig = (ub-mui)/sqrt(s2i);
            % if lbsig > 10 || ubsig < -10
       %     fprintf('YOWZA! %.2f %.2f %.2f %.2f\n', lbsig, ubsig, ...
       %             mui, sqrt(s2i));
       % endfor
            %%
            
		  % now draw from the 1-d normal truncated to [lb, ub]
            x(i) = mui+TruncatedGaussian(-sqrt(s2i),[lb ub]-mui);
         endfor
         if discard <= 0
            n = n + 1;
            X(n,:) = x; 
            ngibbs = ngibbs+1;
         endif
         discard = discard-1;
      endwhile
   endif

endfunction


function [X meaneffective sigmaeffective] = TruncatedGaussian(sigma, range, varargin)
		     % function X = TruncatedGaussian(sigma, range)
		     %          X = TruncatedGaussian(sigma, range, n)
		     %
% Purpose: generate a pseudo-random vector X of size n, X are drawn from
% the truncated Gaussian distribution in a RANGE braket; and satisfies
% std(X)=sigma.
% RANGE is of the form [left,right] defining the braket where X belongs.
% For a scalar input RANGE, the braket is [-RANGE,RANGE].
%
% X = TruncatedGaussian(..., 'double') or
% X = TruncatedGaussian(..., 'single') return an array X of of the
%    specified class.
%
% If input SIGMA is negative, X will be forced to have the same "shape" of
% distribution function than the unbounded Gaussian with standard deviation
% -SIGMA: N(0,-SIGMA). It is similar to calling RANDN and throw away values
% ouside RANGE. In this case, the standard deviation of the truncated
% Gaussian will be different than -SIGMA. The *effective* mean and
% the effective standard deviation can be obtained by calling:
%   [X meaneffective sigmaeffective] = TruncatedGaussian(...)
%
% Example:
% 
% sigma=2;
% range=[-3 5]
% 
% [X meaneff sigmaeff] = TruncatedGaussian(sigma, range, [1 1e6]);
% 
% stdX=std(X);
% fprintf('mean(X)=%g, estimated=%g\n',meaneff, mean(X))
% fprintf('sigma=%g, effective=%g, estimated=%g\n', sigma, sigmaeff, stdX)
% hist(X,64)
%
% Author: Bruno Luong <brunoluong@yahoo.com>
% Last update: 19/April/2009
%              12-Aug-2010, use asymptotic formula for unbalanced
%                           range to avoid round-off error issue

	% We keep track this variables so as to avoid calling fzero if
% TruncatedGaussian is called succesively with the same sigma and range
   persistent PREVSIGMA PREVRANGE PREVSIGMAC

				% shape preserving?
   shapeflag = (sigma<0);

				% Force inputs to be double class
   range = double(range);
   if isscalar(range)
				% make sure it's positive
      range=abs(range);
      range=[-range range];
   else
      range=sort(range); % right order
   endif
   sigma = abs(double(sigma));

   n=varargin;

   if shapeflag
     % Prevent the same pdf as with the normal distribution N(0,sigma)
      sigmac = sigma;
   else
      if diff(range)^2<12*sigma^2 % This imposes a limit of sigma wrt range
         warning('TruncatedGaussian:RangeSigmaIncompatible', ...
                 'TruncatedGaussian: range and sigma are incompatible\n');
         sigmac = Inf;
      elseif isequal([sigma range], [PREVSIGMA PREVRANGE]) % See line #80
         sigmac = PREVSIGMAC; % Do not need to call fzero
      else
         % Search for "sigmac" such that the truncated Gaussian having
         % sigmac in the formula of its pdf gives a standard deviation
         % equal to sigma
         [sigmac res flag] = fzero(@scz,sigma,[],...
                                   sigma^2,range(1),range(2)); %#ok
         sigmac = abs(sigmac); % Force it to be positive
         if flag<0 % Someting is wrong
            error('TruncatedGaussian:fzerofailled', ...
                  'Could not estimate sigmac\n');
         endif
				% Backup the solution
         [PREVSIGMA PREVRANGE PREVSIGMAC] = deal(sigma,range,sigmac);
      endif
   endif

				% Compute effective standard deviation
   meaneffective=meantrunc(range(1), range(2), sigmac);
   sigmaeffective=stdtrunc(range(1), range(2), sigmac);

				% Inverse of the cdf functions
   if isinf(sigmac)
  % Uniform distribution to maximize the standard deviation within the
  % range. It is like a Gaussian with infinity standard deviation
      if any(strcmpi(n,'single'))
         range = single(range);
      endif
      cdfinv = @(y) range(1)+y*diff(range);
   else
      c = sqrt(2)*sigmac;

      rn = range/c;
      asymthreshold = 4;
      
      if any(strcmpi(n,'single'))
				% cdfinv will be single class
         c = single(c);
				%e = single(e);
      endif
      
				% Unbalanced range
      if prod(sign(rn))>0 && all(abs(rn)>=asymthreshold)
   % Use asymptotic expansion
   % based on a Sergei Winitzi's paper "A handly approximation for the
   % error function and its inverse", Feb 6, 2008.
         c = c*sign(rn(1));
         rn = abs(rn);
         left = min(rn);
         right = max(rn);
         
         a = 0.147;

         x2 = left*left;
         ax2 = a*x2;
         e1 = (4/pi+ax2) ./ (1+ax2);
         e1 = exp(-x2.*e1); % e1 < 3.0539e-008 for asymthreshold = 4

         x2 = right*right;
         ax2 = a*x2;
         e2 = (4/pi+ax2) ./ (1+ax2);
         e2 = exp(-x2.*e2); % e2 < 3.0539e-008 for asymthreshold = 4
         
      % Taylor series of erf(right)-erf(left) ~= sqrt(1-e2)-sqrt(1-e1)
         de =  -0.5*(e2-e1) -0.125*(e2-e1)*(e2+e1);
         
               % Taylor series of erf1 := erf(left)-1 ~= sqrt(1-e1)-1 
         erf1 = (-0.5*e1 - 0.125*e1^2);
         cdfinv = @(y) c*asymcdfinv(y, erf1, de, a);
         
      else
         e = erf(range/c);

         cdfinv = @(y) c*erfinv(e(1)+diff(e)*y);
      endif
   endif

				% Generate random variable
   X = cdfinv(rand(n{:}));
 % Clip to prevent some nasty numerical issues with of erfinv function
 % when argument gets close to +/-1
   X = max(min(X,range(2)),range(1));

   return

endfunction % TruncatedGaussian


%%
function x = asymcdfinv(y, erf1, de, a)
 % z = erf(left) + de*y = 1 + erf1 + de*y, input argument of erfinv(.)
   f = erf1 + de*y; % = z - 1; thus z = 1+f
		    % 1 - z^2 = -2f-f^2
   l = log(-f.*(2 + f)); % log(-2f-f^2) = log(1-z.^2);
   b = 2/(pi*a) + l/2;
   x = sqrt(-b + sqrt(b.^2-l/a));
endfunction % asymcdfinv

function m=meantrunc(lower, upper, s)
	      % Compute the mean of a trunctated gaussian distribution
   if isinf(s)
      m = (upper+lower)/2;
   else
      a = (lower/sqrt(2))./s;
      b = (upper/sqrt(2))./s;
      corr = sqrt(2/pi)*(-exp(-b.^2)+exp(-a.^2))./(erf(b)-erf(a));
      m = s.*corr;
   endif
endfunction % vartrunc

function v=vartrunc(lower, upper, s)
	  % Compute the variance of a trunctated gaussian distribution
   if isinf(s)
      v = (upper-lower)^2/12;
   else
      a = (lower/sqrt(2))./s;
      b = (upper/sqrt(2))./s;
      if isinf(a)
         ea=0;
      else
         ea = a.*exp(-a.^2);
      endif
      if isinf(b)
         eb = 0;
      else
         eb = b.*exp(-b.^2);
      endif
      corr = 1 - (2/sqrt(pi))*(eb-ea)./(erf(b)-erf(a));
      v = s.^2.*corr;
   endif
endfunction % vartrunc

function stdt=stdtrunc(lower, upper, s)
	    % Standard deviation of a trunctated gaussian distribution
   arg = vartrunc(lower, upper, s)-meantrunc(lower, upper, s).^2;
				%arg = max(arg,0);
   stdt = sqrt(arg);
endfunction % stdtrunc

function res=scz(sc, targetsigma2, lower, upper)
     % Gateway for fzero, aim the standard deviation to a target value
   res = vartrunc(lower, upper, sc) - targetsigma2 - ...
	 meantrunc(lower, upper, sc).^2;
endfunction % scz

				% End of file TruncatedGaussian.m




function [res flag] = gen1Z (C, z0)

   ## usage:  [res flag] = gen1Z (C, z0)
   ##
   ## generate rows(z0) C randoms

   global PAR

   [nr nc] = size(z0) ;
   N = ceil(PAR.mem / (2*nc)) ;
   if (nc < PAR.pfy(4))
      z = mnormal_rnd(N, C) ;
   else
      if PAR.dbg, printf("pfy: gen1Z: Using truncated normals.\n") ; endif
      z = mnormal_rnd(N, C, z0(1,:)) ;
   endif 
   z0 = [z0 ; repmat(z0(1,:), N-nr, 1)] ;

   I = all(z <= z0, 2) ;
   res = z(I,:) ;
   iter = 0 ;
	# fid = fopen([tempdir "pfy_" num2str(getpid) ".log"], "wt") ;
   while (++iter < PAR.pfy(3) && rows(res) < nr)
      if (nc < PAR.pfy(4))
	 z = mnormal_rnd(N, C) ;
      else
	 z = mnormal_rnd(N, C, z0(1,:)) ;
      endif 
      I = all(z <= z0, 2) ;
      res = [res ; z(I,:)] ;
	    # fprintf(fid, "%d  %10.2f%%\n", iter, 100*rows(res)/nr) ;
	    # fflush(fid) ;
   endwhile 
				# fclose(fid) ;

   if (flag = (rows(res) >= nr))
      res = res(1:nr,:) ;
   endif

endfunction


function res = gen2Z (C, z0)

   ## usage:  res = gen2Z (C, z0)
   ##
   ## generate n C randoms (weak)

   [nr nc] = size(z0) ;
   z = mnormal_rnd(nr, C) ;

   res = z ;
   I = (res <= z0) ;
   res(I) = z(I) ;
   while any(!I(:))
      z = mnormal_rnd(nr, C) ;
      II = !I & (z <= z0) ;
      I = I | II ;
      res(II) = z(II) ;
   endwhile 

endfunction
