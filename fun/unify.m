## usage: [J1 J2 K1 K2] = unify (s1, s2)
##
## map s1 and s2 so that s1(J1)=s2(K2) is in s2 and s2(J2)=s1(K1) is in s1
function [J1 J2 K1 K2] = unify (s1, s2)

   [~, K1, K2] = intersect(s1, s2) ;

   K1 = sort(K1) ;
   K2 = sort(K2) ;

   [~, KS1] = sort(s1(K1)) ;
   [~, KS2] = sort(s2(K2)) ;

   KI1(KS1) = 1:length(KS1) ;
   KI2(KS2) = 1:length(KS2) ;

   J1 = K1(KS1)(KI2) ;
   J2 = K2(KS2)(KI1) ;

endfunction
