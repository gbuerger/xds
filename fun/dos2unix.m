function ofile = dos2unix (ifile)

   ## usage:  ofile = dos2unix (ifile)
   ##
   ## convert dos to unix

   ofile = ifile ;

   s = fscanf(fid=fopen(ifile, "rt"), "%c") ;
   fclose(fid) ;

   if (L = !strcmp(s(end), "\n")), s = [s "\n"] ; endif

   if !isempty(I = strfind(s, "\r")) || L
      fprintf(fid=fopen(ofile, "wt"), "%s", regexprep(s, "\r", "")) ;
      fclose(fid) ;
   endif 

endfunction
