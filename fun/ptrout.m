function ptrout(s, odir)

   ## usage: ptrout(s, odir)
   ## 
   ## output ptr directly

   pkg load statistics
   
   global PAR PRJ GVAR ADIR SDIR CDIR BATCH Lavg per lim isANA isCUR

   t0 = max(cellfun(@(f) stat(f).mtime, glob({sprintf("%s/%s/*.csv", SDIR, PAR.pdd) sprintf("%s/%s/*/*.csv", SDIR, PAR.pdd)}))) ;
   t1 = stat(sprintf("%s/%s.ob", SDIR, PAR.ptr)).mtime ;
   t = min(cellfun(@(f) stat(f).mtime, glob({sprintf("%s/%s.ptrout/*.csv", SDIR, PAR.pdd) sprintf("%s/%s.ptrout/*/*.csv", SDIR, PAR.pdd)}))) ;
   if t > max(t0, t1) return ; endif

   if ~isempty(f = dbstack)
      fname = f(1).name ;
   else
      fname = "ptrout" ;
   endif

   idx.type = "()" ;
   Lptr = ~true ; wLptr = false ;
   mth = {"J" "F" "M" "A" "M" "J" "J" "A" "S" "O" "N" "D"} ;
   P0 = 1000 ; T0 = 273.16 ;
   LEG = {"OBS", "ANA", "CUR", "FUT"} ;
   per = [2010 1 1 ; 2010 12 31] ;
   cur.per = PAR.cper(:,1)' ; fut.per = [2070 2100] ;
   fut.per = [-Inf Inf] ;
   Lavg = ~true ;
   
   ldata = fullfile("data", PRJ, PAR.pdd) ;
   gdata = fullfile("data", PRJ, "ptr") ;

   METAFILE = fullfile(ldata, "meta.txt") ;
   [ids, vars, names, lons, lats, alts, undefs, J] = meta(METAFILE) ;
   
   VAR = sunique(vars) ; VARNAME = sunique(names) ;

   for jv=1:length(VAR)

      if !any(J{jv}) continue ; endif

      if isempty(regexp(s.name, "precip")) | isempty(regexp(names{J{jv}(1)}, "precip")) continue ; endif

      K = convhull(lons(J{jv}), lats(J{jv})) ;
      K = J{jv}(K) ;
      G = [lons(K) lats(K)] ;
      q = 1.0 ;
      G0 = mean(G) ; Ge = G0 + q*(G - G0) ;

      [LON LAT] = meshgrid(s.lon, s.lat) ;
      [IN ON] = inpolygon(LON(:), LAT(:), Ge(:,1), Ge(:,2)) ;
      IN = (IN | ON) ;
      LON(~IN) = nan ; LAT(~IN) = nan ;

      if 0
	 clf ; hold all ;
	 plot(G(:,1), G(:,2), "+", "color", 0.7*[1 1 1]) ;
	 plot(Ge(:,1), Ge(:,2), "+", "color", 0.7*[1 1 1]) ;
	 plot(LON(:), LAT(:), "+", "color", [1 0 0]) ;
      endif

      v.id = s.id ;
      I = repmat({":"}, 1, ndims(s.x)) ; I(2) = IN ;
      v.x = s.x(I{:}) ;
      v.x = P0 * squeeze(nanmean(v.x, 2)) ;
      v.x = max(v.x, 0) ;
      v.name = "P" ;

      ne = size(v.x, 2) ;
      v.ids = arrayfun(@(i) sprintf("e%02d", i), 1:ne, "UniformOutput", false) ;
      v.vars = repmat({v.name}, 1, ne) ;
      v.udf = repmat(lim, 1, ne) ;

      if isANA | isCUR
	 mwrite_stat(odir, v) ;
      else
	 if isfield(s, "id_ref")
	    nm = length(unique(s.id_ref(:,2))) ;
	 else
	    nm = 12 ;
	 endif
	 v.id = reshape(v.id, [], nm, columns(v.id)) ;
	 v.x = reshape(v.x, [], nm, columns(v.x)) ;
	 for m = 1:nm
	    w = v ;
	    w.id = squeeze(v.id(:,m,1:3)) ;
	    w.x = squeeze(v.x(:,m,:)) ;
	    mwrite_stat(odir, w, sprintf("%02d.", m)) ;
	 endfor
      endif

   endfor
   
endfunction
