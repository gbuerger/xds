function y = nan2lim(x, l)

   global lim PAR
   
   if (nargin < 2)
      if ( isempty(lim) )
	 error("lim undefined.\n") ;
      endif
      l = lim ;
   endif

   N = size(x) ;

   if isscalar(l)
      l = repmat(l, N) ;
   else
      l = repmat(l, [N(1) 1 N(3:end)]) ;
   endif

   y = x ;
   I = find(isnan(x(:))) ;
   y(I) = l(I) ;

endfunction
