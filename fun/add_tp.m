## usage: s = add_tp (r)
##
## replace cp and lsp with tp
function s = add_tp (r)

   s = r ;
   if isfield(s, "cp") && isfield(s, "lsp")
      s.tp = s.cp ;
      s.tp.x = s.cp.x + s.lsp.x ;
      s = rmfield(s, "cp") ;
      s = rmfield(s, "lsp") ;
   endif
   
endfunction
