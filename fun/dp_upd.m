function dp_upd (u)

   ## usage: dp_upd (u)
   ## 
   ## display structure update

   persistent v ;

   if !isstruct(u), return ; endif

   if isempty(v) ;

      for [uu uk] = u
	 printf("%s.%s: ",inputname(1), uk) ;
	 disp(uu) ;
      endfor

   else

      for [uu uk] = u
	 for [vv vk] = v
	    if !strcmp(vk, uk) || isstruct(vv) || ishandle(vv), continue ; endif # FIXME: somewhat ugly
	    if !isequal(vv, uu)
	       printf("%s.%s: ",inputname(1), uk) ;
	       disp(uu) ;
	    endif
	 endfor
      endfor

   endif

   v = u ;

endfunction
