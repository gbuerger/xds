## usage: varargout = NaN_ (fun, varargin)
##
## evaluate fun(x(~nan), y(~nan))
function varargout = NaN_ (fun, varargin)

   global PAR lim

   if ~isfield(PAR, "idx") PAR.idx.type = "()" ; endif
   if ~isfield(PAR, "rec") PAR.rec = 1 ; endif
   if ~isfield(PAR, "udf") PAR.udf = lim ; endif
   if ~isfield(PAR, "par_opt") PAR.par_opt = {"UniformOutput" false} ; endif
   if isa(fun, "function_handle")
      fun = func2str(fun) ;
   endif

   if nargin < 2
      error("xds:xds", "NaN_: too few arguments") ;
   endif

   i = 1 ; xargs = {} ;
   xargs{i} = varargin{i} ;
   while ++i < nargin
      if size(varargin{i}, 1) == size(xargs{1}, 1)
	 xargs = {xargs{:}, varargin{i}} ;
      else
	 break ;
      endif
   endwhile
   nargs = i - 1 ;
   sargs = varargin(nargs+1:end) ;

   I = and(parfun(@(c) isfinite(c), xargs, PAR.par_opt{1:2}){:}) ;
   I = all(all(I, 2), 3) ;
   
   PAR.idx.subs = repmat({":"}, 1, ndims(xargs{1})) ;
   PAR.idx.subs{PAR.rec} = I ;
   for i = 1:nargs
      xargs{i} = subsref(xargs{i}, PAR.idx) ;
   endfor

   if ~any(I)
      if ~PAR.udf
	 warning("xds:xds", "no values for function %s", fun) ;
      endif
      if strcmp(fun, "leasqr")
         varargout{1} = nan(3,1) ;
      else
         varargout = num2cell(arrayfun(@(c) nan, 1:nargout)) ;
      endif 
   else
      if strcmp(fun, "leasqr")
	 xargs = parfun(@(c) c', xargs, PAR.par_opt{1:2}) ;
	 [~,varargout{1}] = feval(fun, xargs{:}, sargs{:}) ;
      else
	 varargout = cell(1, nargout) ;
	 [varargout{:}] = feval(fun, xargs{:}, sargs{:}) ;
      endif
   endif

endfunction
