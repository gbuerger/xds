function [s ptr] = dsc (X, clim, E)

   ## usage:  [s ptr] = dsc (X, clim, E)
   ##
   ## downscale "file", using climatology clim and model E

   global PAR PRSV isANA isCUR CDIR ;

   N = size(X.x) ;
   if length(N) > 2
      for j=1:N(3)
	 C{j} = X ;
	 C{j}.x = X.x(:,:,j) ;
      endfor
      [w ptr] = parfun(@(c) dsc(c, clim, E), C, PAR.par_opt{:}) ;
      w = cell2mat(w) ;
      ptr = cell2mat(ptr) ;
      s = w(1) ;
      s.z = cat(3, w.z) ;
      s.x = cat(3, w.x) ;
      return
   endif

   ## downscale X using (seasonal) E
   [s ptr] = do_dsc(X, E) ;

   ## normalize wrt. base climate
   if ~isempty(PAR.pddadj) && (~isANA || PAR.anaadj)
      pfile = fullfile(CDIR, PAR.pdd, "adj.ob") ;
      if isCUR
	 I = sdate(s.id, PAR.cper(1,:), PAR.cper(2,:)) ;
	 if ~isempty(PAR.anc)
	    PRSV = strcmp(PAR.pddadj, "preserve") ;
	    adj = feval(PAR.adjfun, s.z(I,:)) ;
	    PRSV = false ;
	 else
	    [~, adj.m, adj.s] = anom(s.z(I,:)) ;
	 endif
	 printf("dsc: --> %s\n", pfile) ;
	 save(pfile, "adj") ;
      endif
      printf("dsc: <-- %s\n", pfile) ;
      load(pfile) ;
      if isempty(PAR.anc)
	 s.z = anom(s.z, adj.m, adj.s) ;
      else
	 s.z = feval(PAR.adjfun, s.z, adj) ;
      endif
   endif

   s = rescale(s, "z") ;

   s = pre_pare(s, "inv") ;

endfunction


function [s ptr] = do_dsc (X, E)

   ## usage: [s ptr] = do_dsc (X, E)
   ## 
   ## 

   global PAR BATCH

   PAR.idx.subs = repmat({":"}, 1, ndims(X.x)) ;
   
   s.id = X.id ;
   if isfield(X, "id_ref"), s.id_ref = X.id_ref ; endif
   s.cal = X.cal ;
   N = size(X.x) ; N1 = [N(1) N(3:end)] ;
   s.z = nan([N1, size(E, 2)]) ;

   if isfield(PAR, "ptridx") && ~isempty(PAR.ptridx)
      idx = PAR.ptridx ;
   else
      idx = 1:length(X.ptr) ;
   endif
   
   ptr = zeros(length(X.ptr),columns(s.z)) ;
   for is = 1:length(PAR.ssn)
      PAR.idx.subs{PAR.rec} = selssn(PAR.ssn{is}, X.id) ;
      x = subsref(X.x, PAR.idx) ;
      w = mult(x, E(:,:,is), 2, 1) ;

      ## predictor analysis
      for j = 1:length(X.ptr)
	 if ~ismember(j, idx)
	    ptr(j,:) = ptr(j,:) + nan(1, columns(s.z)) ;
	 else
	    J = X.J == j ;
	    ptr(j,:) = ptr(j,:) + mean(mult(x(:,J), E(J,:,is), 2, 1)) ./ mean(w) ;
	 endif
      endfor
      
      clear x ;
      s.z(PAR.idx.subs{:}) = w ;
      clear w ;
      ##s.z(PAR.idx.subs{:}) = mult(subsref(X.x, PAR.idx), E(:,:,is), 2, 1) ;

   endfor
   ptr = ptr / length(PAR.ssn) ;
   p = 1:length(N) ; p = [1 p(end) setdiff(p, [1 p(end)])] ;
   s.z = permute(s.z, p) ;
   
endfunction
