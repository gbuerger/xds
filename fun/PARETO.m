function y = PARETO (x, p)

   ## usage:  y = PARETO (x, p)
   ##
   ## generalized Pareto

   y = 1 - (1 + p(3)*(x-p(1))/p(2)).^(-1/p(3)) ;
   y = norminv(y) ;

endfunction
