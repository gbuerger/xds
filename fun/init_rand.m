## usage: init_rand (rfile = sprintf("%s/rand_init.ob", pwd))
##
##
function init_rand (rfile = sprintf("%s/rand_init.ob", pwd))

   if ~exist(rfile, "file")
      v = rand("state") ;
      save(rfile, "v") ;
   else
      load(rfile) ;
      rand("state", v) ;
   endif

endfunction
