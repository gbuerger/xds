function res = eliminate_dups (C)

   ## usage: res = eliminate_dups (C)
   ## 
   ## eliminate duplicates

   [Cs I] = sort(C) ;

   res(k = 1) = true ;
   for i = 2:length(Cs)
      if strcmp(Cs{i}, Cs{i-1}), continue ; endif
      res(++k) = i ;
   endfor

   res = I(res) ;

endfunction
