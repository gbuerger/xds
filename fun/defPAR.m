function defPAR (a, b)

   ## usage:  defPAR (a, b)
   ##
   ## set default parameters

   global PAR

   if !ischar(a)
      error("defPAR: need string input\n") ;
   endif

   if !isfield(PAR, a)
      eval(["PAR." a " = b ;"]) ;
   endif

endfunction