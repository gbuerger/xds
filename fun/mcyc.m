function varargout = mcyc (r, parm)

   ## usage:  u = mcyc (r, mode, parm)
   ##
   ## estimate and remove multiplicative annual cycle

   pkg load statistics
   persistent nprint = 0
   global PAR PRJ BATCH

   u = r ;
   N = size(u.x) ;
   j0 = length(N)-1 ;
   K = [1 N(end-j0+1:end)] ;

   if !isfield(r, "cal"), r.cal = "gregorian" ; endif 
   t = date2toy(u.id, r.cal) ;

   if (nargin < 2)
      nprint++ ;

      Cs = num2cell(std(u.x)) ;
      Cx = mat2cell(u.x, N(1), ones(1,N(2))) ;
      parm = cellfun(@(x,s) fmins(@(p) sumsq(x./PAR.mcyc(t,p) - 1), [mean(t) mean(t) s]), Cx, Cs, PAR.par_opt{1:2})

      parm = cell2mat(parm') ;

      if (nargout < 2)
	 varargout{1} = parm ;
	 return ;
      endif
   endif

   u.a = u.x ./ PAR.mcyc(t,parm) ;
   
   if (nargin > 2)
      varargout{1} = u ;
   else
      varargout{1} = parm ;
      varargout{2} = u ;
   endif 
      
endfunction
