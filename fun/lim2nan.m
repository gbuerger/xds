function y = lim2nan (x, l)

   ## usage:  y = lim2nan (x, l)
   ##
   ## convert limits to nans

   persistent iswarned = false
   global lim ;

   if nargin < 2
      if isempty(lim)
	 if ~iswarned
	    warning("lim undefined, setting lim = -999.\n") ;
	    iswarned = true ;
	 endif
	 lim = -999 ;
      endif
      l = lim ;
   endif

   [N, x] = twodim(x) ;
   [nr nc] = size(x) ;

   if isscalar(l)
      l = repmat(l, 1, nc) ;
   endif 
   l = repmat(l, nr, 1) ;

   y = x ;
   ind = find(x==l) ;
   y(ind) = nan ;

   y = reshape(y, N) ;

endfunction
