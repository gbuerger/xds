## usage: val = keyval (s, key)
##
## return value of key for (key,val)-structure array s
function [val j] = keyval (s, key)

   if ~isstruct(s) || isempty(j = find(strcmp({s.Name}, key)))
      val = false ;
   else
      if isfield(s(j), "Value")
	 val = s(j).Value ;
      else
	 val = s(j).Name ;
      endif
   endif
endfunction
