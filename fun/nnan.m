function y = nnan (x)

   ## usage:  y = nnan (x)
   ##
   ## return defined (!isnan) values of x

   y = x(!isnan(x)) ;

endfunction
