function [y dlist] = expand_plist (s, tp, mode)

   ## usage:  [y dlist] = expand_plist (s, tp, mode)
   ##
   ## 

   if nargin < 3, mode = false ; endif

   dlist = [] ;
   c = "|" ; comment = "#" ;

   if iscell(s)
      y = s ;
      return ;
   endif
   
   s = fscanf(fid=fopen(s, "r"), "%c") ; fclose(fid) ;
   if s(1) == "@"
      [y dlist] = feval(s(2:end)) ;
      return ;
   endif
   s = strsplit(s, "\n") ;
   s = s(!strncmp(s, comment, 1) & !strcmp(strtrim(s), "")) ;
   if mode
      y = s ;
      return
   endif
   
   plist = cellfun(@(u) split_rows(u), s, "UniformOutput", false) ;
   rp = columns(plist) ;

   y = {} ; Y = {} ;
   for r = 1:rp

      ##waitbar(r/rp) ;
      f = strtrim(plist{r}) ; I = strfind(f{1}, c) ;
      cp = columns(f) ;
      if cp > 1
	 clear II
	 for j = 1:cp-1
	    J = I(2*j-1):I(2*j) ;
	    eval(["II(" num2str(j) ",:) = num2cell(" strtrim(f{j+1}) ");"]) ;
	 endfor
	 for i = 1:columns(II)
	    s = strrep(sprintf(f{1}, II{:,i}), c, "") ;
	    y = {y{:}, s} ;
	    Y(r,i) = {s} ;
	 endfor
      elseif isempty(f)
	 continue
      else
	 y = union(y, f) ;
	 Y(r,1) = {f} ;
      endif

   endfor
   
   if nargin > 1 && tp
      y = Y(:)' ;
      I = cellfun(@(s) ~isempty(s), y) ;
      y = y(I) ;
   endif

endfunction


## usage: v = split_rows (u)
##
##
function v = split_rows (u)
   v = strsplit(u, " ") ;
   v = v(!strcmp(strtrim(v), "")) ;
endfunction
