## usage: [J JJ] = pick_meta (mids, mvars, ids, v)
##
##
function [J JJ] = pick_meta (mids, mvars, ids, v)

   JJ = cellfun(@(x) isempty(regexp(x, "^#.*")), mids) ;

   n = length(ids) ;
   J = nan(1,n) ;

   I = find(strcmp(mvars, v)') ;

   J = arrayfun(@(c) find(strcmp(c, mids(I))), ids, "UniformOutput", false) ;
   
   for j = 1:n
      if ~any(wJ = strcmp(ids{j}, mids(I)))
	 error("xds:xds", "no metadata for station %s", ids{j}) ;
      endif
      J(j) = find(wJ) ;
   endfor
   J = cell2mat(J) ;
   J = J(~isnan(J)) ;

   J = I(J) ;
   JJ = JJ(J) ;

endfunction
