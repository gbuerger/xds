function [res s1] = cost (p, X, varargin)

   ## usage:  res = cost (p, X, varargin)
   ##
   ## cost function of problem defined by PAR.cdffit

   global PAR

   x = X(:,1) ; y = X(:,2) ;
   F = PAR.cdffit ;

   if iscell(F)
      FD = feval(F{1}, p, x, varargin{:}) ;
      FI = feval(F{2}, p, y, varargin{:}) ;
   elseif (exist([F "cdf"]) == 2 && exist([F "inv"]) == 2)
      FD = prb(F, x, p, varargin{:}) ;
      FI = prb_inv(F, y, p, varargin{:}) ;
   else
      FD = feval(F, x, p, varargin{:}) ;
      FI = feval([F "_inv"], y, p, varargin{:}) ;
   endif
   if !(isreal(FD) & isreal(FI))
      res = Inf ; return ;
   endif

   if (all(!isfinite(y - FD)) || all(!isfinite(x - FI)))
      res = inf ;
      return ;
   endif

   I = isfinite(x) & isfinite(y) & isfinite(FD) & isfinite(FI) ;
   x = x(I) ; y = y(I) ; FD = FD(I) ; FI = FI(I) ;
   
   s1 = rms_(y - FD) ;
   xm = mean(x) ;
   ym = mean(y) ;
   res1 = rms_(y - FD) / rms_(y-ym) ;
   res2 = rms_(x - FI) / rms_(x-xm) ;

   w = PAR.prbwgt ;
   res = w(1)*res1 + w(2)*res2 ;
   res = min(res,Inf) ;

endfunction
