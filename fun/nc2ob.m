## usage: [obfile lfile] = nc2ob (ncf)
##
## transform ncfile to ob
function [obfile lfile] = nc2ob (ncf, ndir)

   fs = strsplit(ncf, "/") ;
   fname = strsplit(fs{end}, ".") ;
   if strncmp(fs{1}, "http", 4)
      j = strfind(fs{end}, ".")(end) ;
      fs{end} = [fs{end}(1:j) "ob"] ;
      obfile = fullfile(ndir, fs{2:end}) ;
   else
      obfile = [ncf(1:end-3) ".ob"] ;
   endif

   if exist(lfile = [obfile(1:end-3) ".nc"]) ~= 2
      lfile = [obfile(1:end-3) "." fname{end}] ;
   endif

endfunction
