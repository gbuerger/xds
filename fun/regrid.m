function v = regrid (u, lon, lat)

   ## usage:  v = regrid (u, lon, lat)
   ##
   ## regrid u.x with u.lon, u.lat, using vectors lon, lat

   f = dbstack ;

   global PAR
   
   v = u ;
   N = size(u.x) ;

   if v.stdgrd
      nlon = length(u.lon) ; nlat = length(u.lat) ;
      N = [N(1) nlon nlat N(3:end)] ;
      u.x = reshape(u.x, N) ;
      PAR.idx.subs = repmat({":"}, 1, length(N)) ;
      [u.lon plon] = sort(u.lon) ;
      [u.lat plat] = sort(u.lat) ;
      PAR.idx.subs{2} = plon ; PAR.idx.subs{3} = plat ; 
      u.x = subsref(u.x, PAR.idx) ;
      I(1:3) = {ones(1, N(1)), nlon, nlat} ;
      for i=4:length(N)
	 I{i} = ones(1, N(i)) ;
      endfor
      c = squeeze(mat2cell(u.x, I{:})) ;
   else
       [nx ny] = size(u.lon) ; u.x = reshape(u.x, [], nx, ny) ;
       c = mat2cell(u.x, ones(1, N(1)), nx, ny) ;
   endif

   if diff(u.lon)(1) > diff(lon)(1) | diff(u.lat)(1) > diff(lat)(1)
      printf("%s> interpolating to smaller grid\n", f(1).name) ;
      y = parfun(@(x) interp2(u.lon, u.lat, squeeze(x)', lon, lat', PAR.regrid{:})'(:)', c, PAR.par_opt{:}) ;
      v.g = cell2mat(y) ;
   else
      printf("%s> resampling to larger grid\n", f(1).name) ;
      y = resample_grid(u.lon, u.lat, u.x, lon, lat, u.isP) ;
      v.g = reshape(y, [N(1) length(lon)*length(lat) N(4:end)]) ;
   endif
   
endfunction
