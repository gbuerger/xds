function y = affin (x, q, d)

   ## usage:  y = affin (x, q, d)
   ##
   ## affin strech by q of vector x (along dim d)

   if nargin < 3, d = 1 ; endif

   if ndims(x) < 3
      xm = mean(x) ;
   else
      xm = mean(x, d) ;
   endif

   xd = x - xm ;
   y = xm + q .* xd ;

endfunction