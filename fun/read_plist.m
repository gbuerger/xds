## usage: [plist dlist] = read_plist (gdir)
##
## read list of ptr files (netcdf) from gdir
function [plist dlist] = read_plist (gdir)

   global PAR
   
   dlist = {} ;
   if exist(pfile = fullfile(gdir, "ptr.lst"), "file") == 2
      printf("read_plist: <-- %s\n", pfile) ;
      [C dlist] = expand_plist(pfile, [], ~true) ;
   else
      C = glob({fullfile(gdir, "*.nc"), fullfile(gdir, "*/*.nc"), fullfile(gdir, "*/*/*.nc")})' ;
      dlist = unique(cellfun(@(f) fileparts(f), C, "UniformOutput", false)) ;
      i = 0 ; clear C ;
      for d = dlist
	 i++ ; d = d{:} ;
	 C{i} = glob({fullfile(d, "*.nc"), fullfile(d, "*/*.nc"), fullfile(d, "*/*/*.nc")}) ;
      endfor
   endif

   if isempty(C)
      error("read_plist: no matching files in %s\n", fullfile(gdir, "ptr.lst")) ;
   endif

   if ~isempty(dlist)
      plist = C ;
   elseif ~PAR.appdec || ~strncmp(C{1}, "http", 4)
	 
      plist = {C} ;
      dlist = {""} ;

   else

      CM = cell2mat(cellfun(@(c) strsplit(c, "_"), C', "UniformOutput", false)) ;
      Y = cellfun(@(c) sscanf(c(end-10:end-8), "%d"), CM(:,6)) ;
      Yu = unique(Y) ;
      
      for i=1:length(Yu)
	 iJ = find(Y == Yu(i)) ;
	 dlist{i} = sprintf("%d0s", Yu(i)) ;
	 plist{i} = C(iJ) ;
      endfor

   endif
		    
endfunction
