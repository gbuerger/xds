function res = ncisdim (nc, nv)

   ## usage:  res = ncisdim (nv)
   ##
   ## netcdf variable nv of nc is dim

   res = false ;
   for nd = nc.Dimensions
      res = res || strcmp(nd.Name, nv) ;
   endfor

endfunction
