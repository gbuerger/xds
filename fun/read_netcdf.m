function [nc lDAP] = read_netcdf (ncfile)

   ## usage:  [nc lDAP] = read_netcdf (ncfile)
   ##
   ## read netcdf file, from OPeNDAP or fileserver

   pkg load netcdf
   if exist(cert = fullfile(pwd, "cert.sh"))
      system(["sh " cert]) ;
   endif

   try
      ncinfo(ncfile) ;
   catch
      warning("xds:xds", "could not read: %s", ncfile) ;
   end_try_catch

   f = dbstack ;
   global PAR

   if isempty(strfind(ncfile, "://"))

      lDAP = false ;
      if stat(ncfile).size == 0
	 warning("xds:xds", "%s: empty file: %s", f(1).name, ncfile) ;
	 unlink(ncfile) ;
	 nc = [] ;
      else
	 nc = netcdf(ncfile, "r") ;
      endif

   else

      ##try
      lDAP = true ;
      ##f = tmpnam ;
      ##wget_args = [" --certificate=" PAR.credsdir "/credentials.pem --private-key=" PAR.credsdir "/credentials.pem \
      ##--ca-directory=" PAR.credsdir "/certificates -qO " f] ;
      ##[st out] = system(cmd = ["wget " wget_args " " [ncfile ".dds"]]) ;
      ##unlink(f) ;
      for j=1:10		# try hard
	 [st out] = system(["ncdump -h " ncfile]) ;
	 if st == 0
	    nc = netcdf(ncfile, "r") ;
	    break ;
	 else
	    warning("xds:xds", "read_netcdf: trouble reading %s", ncfile) ;
	    if exist(cert = fullfile(pwd, "cert.sh"))
	       system(["sh " cert]) ;
	    endif
	    sleep(5) ;
	 endif
      endfor
      if st ~= 0
	 warning("xds:xds", "read_netcdf: not read: %s", ncfile) ;
	 lDAP = false ; nc = NaN ;
	 return ;
      endif
      ##catch
      ##	 lDAP = false ;
      ##	 [f1 f2 f3] = fileparts(ncfile) ;
      ##	 [info, err, msg] = stat(f = fullfile("data", PAR.sim{1}, [f2 f3])) ;
      ##	 if err
      ##	    wget_args = [" --certificate=" PAR.credsdir "/credentials.pem --private-key=" PAR.credsdir "/credentials.pem \
      ##	 --ca-directory=" PAR.credsdir "/certificates -qO " f] ;
      ##	    ncinfo(ncfile)
      ##	    [err out] = system(cmd = ["wget " wget_args " " ncfile]) ;
      ##	 endif
      ##	 if err
      ##	    error("app:read_netcdf: file not found: %s\nexecuting: %s\nstatus: %d: %s", ncfile, cmd, st, out) ;
      ##	 else
      ##	    nc = netcdf(f, "r") ;
      ##	 endif
      ##end_try_catch

   endif

endfunction
