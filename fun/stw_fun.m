function res = stw_fun (X, Y)

   ## usage:  Jeof = stw_fun (X, Y)
   ##
   ## stepwise estimation of best Jeof

   global PAR

   res = cell(1,7) ;

   addpath(genpath("~/matlab/stats")) ;
   opt = {"penter", PAR.penter, "premove", PAR.premove, "display", "off"} ;

   [res{:}] = stepwisefit(X.x, mean(Y.z, 2), opt{:}) ;

   rmpath(genpath("~/matlab/stats")) ;

endfunction