## usage: varargout = xyfit (x, y, p, varargin)
##
##
function varargout = xyfit (x, y, p, varargin)

   pkg load optim
   
   u = [x ; y] ; v = [y ; x] ;
   
   fun = @(x, p) Fun(x, p, varargin{:}) ;
   w = cell(1, nargout) ;

   [w{:}] = leasqr(u, v, p, fun, [], niter=100) ;
   w{2} = w{2}' ;
   
   if nargout > 2 && w{3} == 0
      error("xds:xds", "xyfit> no convergence") ;
   endif
   
   varargout = w ;
								 
endfunction


## usage: y = Fun (x, p, varargin)
##
##
function y = Fun (x, p, varargin)

   global PAR

   F = PAR.cdffit ;
   n = rows(x) ;
   y = nan(size(x)) ;
   
   if iscell(F)
      y(1:n/2) = feval(F{1}, p, x(1:n/2), varargin{:}) ;
      y(n/2+1:n) = feval(F{2}, p, x(n/2+1:n), varargin{:}) ;
   elseif (exist([F "cdf"]) == 2 && exist([F "inv"]) == 2)
      y(1:n/2) = prb(F, x(1:n/2), p, varargin{:}) ;
      y(n/2+1:n) = prb_inv(F, x(n/2+1:n), p, varargin{:}) ;
   else
      y(1:n/2) = feval(F, x(1:n/2), p, varargin{:}) ;
      y(n/2+1:n) = feval([F "_inv"], x(n/2+1:n), p, varargin{:}) ;
   endif

endfunction
