function v = to_grid (u, un, iptr)

   ## usage: v = to_grid (u, un, iptr)
   ## 
   ## regrid to common resolution for predictor iptr
   ## UGLY

   f = dbstack ;
   global PAR
   tol = 1e-2 ;
   PAR.idx.subs = repmat({":"}, 1, ndims(u.x)) ;

   v = u ;

   Jlon = selJ(v.lon, PAR.lon, 1e-2) ;
   Jlat = selJ(v.lat, PAR.lat, 1e-2) ;

   if isfield(PAR, "nlon") && isfield(PAR, "nlat") && isfield(PAR, "gausslat")
      [lon lat] = target_ll(PAR.nlon(iptr), PAR.nlat(iptr), PAR.gausslat(iptr)) ;
   else
      lon = v.lon(Jlon) ;
      lat = v.lat(Jlat) ;
   endif

   if chklonlat(unique(v.lon(Jlon)), unique(lon), unique(v.lat(Jlat)), unique(lat), tol)
      if v.stdgrd
	 [wlat wlon] = meshgrid(v.lat, v.lon) ;
	 Jptr = selJ(wlon, PAR.lon, tol) & selJ(wlat, PAR.lat, tol) ;
	 v.lon = v.lon(any(Jptr, 2)) ;
	 v.lat = v.lat(any(Jptr, 1)) ;
      else
	 Jptr = Jlon & Jlat ;
	 v.lon = v.lon(Jlon) ;
	 v.lat = v.lat(Jlat) ;
      endif
      PAR.idx.subs{2} = Jptr ;
      v.x = subsref(v.x, PAR.idx) ;
   else
      if v.stdgrd
	 qlon = mean(abs(diff(lon))) / mean(abs(diff(v.lon))) ;
	 if qlon > 0 && qlon < PAR.qint
	    warning("xds:xds", "%s: interpolating %s to finer grid: qlon=%f (PAR.qint=%f)", f(1).name, un, qlon, PAR.qint) ;
	    printf("diff lon: %10.2f %10.2f\n", mean(abs(diff(lon))), mean(abs(diff(v.lon)))) ;
##	    exit(2) ;
	 endif
	 qlat = mean(abs(diff(lat))) / mean(abs(diff(v.lat))) ;
	 if qlat > 0 && qlat < PAR.qint
	    warning("xds:xds", "%s: interpolating %s to finer grid: qlat=%f (PAR.qint=%f)", f(1).name, un, qlat, PAR.qint) ;
	    printf("diff lat: %10.2f %10.2f\n", mean(abs(diff(lat))), mean(abs(diff(v.lat)))) ;
##	    exit(2) ;
	 endif
      endif
      printf("app: regridding %s using %dx%d\n", un, length(lon), length(lat)) ; 
      v = regrid(v, lon, lat) ;
      v.lon = lon' ; v.lat = lat' ;
      v.x = v.g ; v = rmfield(v, "g") ;
   endif

endfunction
