function [ids, vars, names, lons, lats, alts, undefs, JJ] = meta(metafile)

   ## usage:  [ids, vars, names, lons, lats, alts, undefs, JJ] = meta (metafile)
   ##
   ## read metadata

   global MC

   [~,out] = system(sprintf("file %s", metafile)) ;
   if isempty(strfind(tolower(out), "ascii"))
      metafile = dos2unix(metafile) ;
   endif
   
   META = {"ids" "vars" "names" "lons" "lats" "alts" "undefs"} ;

   nc = columns(META) ; MC = cell(1,nc) ;

   [MC{:}] = textread(metafile, "%s%s%s%s%s%s%s", "delimiter", ",", "whitespace", "") ;

   for j=1:nc
      eval([META{j} " = MC{j}(2:end) ;"]) ;
   endfor

   L = false ;
   pkg load octproj ; PAR.proj = "+init=epsg:31466" ;
   for i = 1:length(lons)
      lons{i} = str2double(lons{i}) ;
      lats{i} = str2double(lats{i}) ;
      alts{i} = str2double(alts{i}) ;
      undefs{i} = str2double(undefs{i}) ;
      if lons{i} ~= undefs{i} & abs(lons{i}) > 360 & lats{i} ~= undefs{i} & abs(lats{i}) > 180
	 L = true ;
	 [lons{i} lats{i}] = op_inv(lons{i}, lats{i}, PAR.proj) ;
	 lons{i} *= 360 / (2*pi) ; lats{i} *= 360 / (2*pi) ;
      endif
   endfor
   lons = cell2mat(lons) ;
   lats = cell2mat(lats) ;
   alts = cell2mat(alts) ;
   undefs = cell2mat(undefs) ;
   
   if L
      warning("xds:xds", "meta> converted GK to LL") ;
   endif
   
   J = cellfun(@(x) isempty(regexp(x, "^#.*")), ids) ;
   ids = strtrim(regexprep(ids, "^#", "")) ;
   
   V = sunique(vars) ;
   JJ = cellfun(@(v) find(strcmp(vars, v) & J), V, "UniformOutput", false)' ;

endfunction
