function h = disp_dfd (s, n = 2, vis = "off")

   ## usage: h = disp_dfd (s, n, vis)
   ## 
   ## display defined data

   global BATCH

   [nr nc] = size(s.x) ;

   if ~isfield(s, "ids")
      s.ids = arrayfun(@(j) num2str(j), 1 : columns(s.x), "UniformOutput", false) ;
   endif
   
   Idfd = !isnan(s.x) ;

   h = figure("position", [0.682   0.666   0.250   0.50], "visible", vis) ;

   if strcmp(graphics_toolkit, "gnuplot")

      subplot(n, 1, 1) ;
      t = pltid(s.id)' ;
      imagesc(t([1 end]), [1 nc], Idfd') ;
##      fs = round(get(gca, "fontsize") *  10/nc);
##      set(gca, "fontsize", fs) ;
      set(gca, "xminortick", "on", "ytick", 1:nc, "yticklabel", s.ids) ;
      xlim = get(gca, "xlim") ;

      if n > 1
	 subplot(n, 1, n) ;
	 plot(t, sum(Idfd, 2)) ;
	 set(gca, "xlim", xlim) ;
      endif

   else

      subplot(n, 1, 1) ;
      t = dateax(s.id)' ;
      imagesc(t([1 end]), [1 nc], Idfd') ;
      datetick("x") ;
##      fs = round(get(gca, "fontsize") * 10/nc);
##      set(gca, "fontsize", fs) ;
      if nc < 30
	 set(gca, "xminortick", "on", "ytick", 1:nc, "yticklabel", strtrunc(s.ids, 4), "fontsize", 10) ;
      endif
      
      xlim = get(gca, "xlim") ;
      if n > 1
	 subplot(n, 1, n) ;
	 plot(t, sum(Idfd, 2), "k") ;
	 datetick("x") ;
	 set(gca, "xlim", xlim) ;
      endif

   endif

   colormap([1 1 1 ; 0 0 0]) ;
##   colormap([0.7 0.7 0.7; 0 0 1])

endfunction
