#!/usr/bin/env python
from ecmwfapi import ECMWFDataServer
server = ECMWFDataServer()
server.retrieve({
    "class": "s2",
    "dataset": "s2s",
    "date": "2018-01-01",
    "expver": "prod",
    "levelist": "700/850/1000",
    "number": "1/to/50",
    "levtype": "pl",
    "model": "glob",
    "origin": "ecmf",
    "param": "129/130/131/132/133",
    "step": "0/to/1080/by/24",
    "stream": "enfo",
    "time": "00:00:00",
    "type": "pf",
    "repres": "ll",
    "grid": "1/1",
    "area": "40/40/20/60",
    "target": "pl",})

server.retrieve({
    "class": "s2",
    "dataset": "s2s",
    "date": "2018-02-01",
    "expver": "prod",
    "number": "1/to/50",
    "levtype": "sfc",
    "model": "glob",
    "origin": "ecmf",
    "param": "228",
    "step": "0/to/1080/by/24",
    "stream": "enfo",
    "time": "00:00:00",
    "type": "pf",
    "repres": "ll",
    "grid": "1/1",
    "area": "40/40/20/60",
    "target": "sfc",})
