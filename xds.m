## main driving script for XDS

clear -a ;
a = argv ;
j = find(cell2mat(cellfun(@(c) ~isempty(findstr(c, ".cfg")), a, "UniformOutput", false))) ;
if ~isempty(j)
   diary([a{j}(1:end-4) ".log"]) ;
else
   diary xds.log ;
endif
diary on

printf("+++ XDS started at %s +++\n", datestr(now, 31)) ;
restoredefaultpath ;
addpath([pwd "/fun"]) ;
addpath([pwd "/util"]) ;

if strcmp(program_invocation_name, "xds.m")
   A = argv ;
elseif ~exist("A", "var")
   A = {} ;
endif

Lptr = Lpdd = Lcal = Lout = Lplt = Lsdly = false ;
for i = 1:numel(A)
   if exist(A{i}, "file")
      A{i} = ["CFG = \"" A{i} "\";"] ;
   elseif regexp(A{i}, "^[01]{6}$")
      [Lptr Lpdd Lcal Lout Lplt Lsdly] = deal(cellfun(@(x) x == "1", num2cell(A{i}), "UniformOutput", false){:}) ;
      A = A(1) ;
   endif
endfor

## calibration
init(A{:}, "SIM = \"\";") ;      # initialization
ptr(Lptr)  ;	                 # process atmospheric predictor fields
pdd(Lpdd)  ;                     # process predictands (stations)
cal(Lcal)  ;                     # calibration
out(Lout)  ;                     # downscaled analyses

## application
init(A{:}) ;                  # initialization
app(Lptr, Lout, Lplt, Lsdly) ;

printf("+++ XDS ended at %s +++\n", datestr(now, 31)) ;
diary off
