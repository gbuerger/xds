function out (L)

   ## usage: out (L)
   ## 
   ## do downscaling and write simulated local data

   global BATCH PAR PRJ isANA ADIR SDIR isCUR
   persistent nprint = 0
   nprint++ ;

   if PAR.ptrout return ; endif

   if nargin < 1, L = ~true ; endif
   
   if false && !isCUR
      wsim = PAR.sim ;
      init(["SIM = \"" PAR.sim{2} "\";"]) ;
      out ;
      PAR.sim = wsim ; clear wsim ;
      init(["SIM = \"" PAR.sim{1} " " PAR.sim{2} "\";"]) ;
   endif

   ldata = fullfile("data", PRJ, PAR.pdd) ;
   pmkdir(odir = fullfile(SDIR, PAR.pdd)) ;

   afile = fullfile(ADIR, [strrep(PAR.ana, "/", "_") ".pc.ob"]) ;
   vfile = fullfile(SDIR, [strrep(PAR.ana, "/", "_") ".pc.ob"]) ;
   Pfile = sprintf("%s/PAR.txt", odir) ;

   if exist(Pfile, "file") == 2
      [~, s] = system(sprintf("grep output %s/PAR.txt", odir)) ;
      sameout = length(strsplit(s)) < 4 || strcmp(strsplit(s){4}, PAR.output) ;
   else
      sameout = true ;
   endif
   if sameout && isnewer(Pfile, PAR.mfile, afile, vfile) && (~L | PAR.aout) return ; endif

   if ~isempty(f = dbstack)
      fname = f(1).name ;
      printf("\n\t###   %s   ###\n\n", fname) ;
   else
      fname = "out" ;
   endif

   printf("out: <-- %s\n", cfile = fullfile(ldata, "clim.ob")) ;
   load(cfile) ;
   printf("out: <-- %s\n", PAR.mfile) ;
   load(PAR.mfile) ;

   ## do the downscaling
   if strcmp(PAR.mod, "rgr")
      printf("out: <-- %s\n", vfile) ;
      load(vfile) ;
      xds = dsc(PC, clim, R) ;
   elseif strcmp(PAR.mod, "rsc") # debugging probit
      printf("out: <-- %s\n", ofile = sprintf("%s/%s.ob", ldata, PAR.pdd)) ;
      load(ofile) ;
      w = rescale(pdd, "z") ; pdd.rx = w.x ;
      xds = pdd ; xds.x = pdd.rx ;
   else
      printf("out: <-- %s\n", vfile) ;
      load(vfile) ;
      [xds ptr] = dsc(PC, clim, E) ;
   endif

   ## check simulated distribution
   if PAR.dbg
      nv = length(clim.vars) ;
      nPan = min(4, ceil(sqrt(nv))) ;
      if BATCH
	 figure("visible", "off") ;
      endif
      j = 0 ;
      while j++ < nv/(nPan*nPan)
	 pfile = fullfile(fileparts(PAR.mfile), ["pdf_" num2str(nprint) ".png"]) ;
	 for jPan=1:nPan*nPan
	    subplot(nPan, nPan, jPan) ;
	    [q s] = qqplot(nnan(xds.z(:,j)), "norm") ;
	    vm = max(abs([get(gca, "Xlim") get(gca, "Ylim")])) ;	 
	    plot([-vm vm], [-vm vm], "--", q, s, "k") ; axis square
	 endfor
	 printf("out: --> %s\n", pfile) ;
	 print(pfile) ;
	 clf ;
      endwhile
      ## predictor influence
      if PAR.pcr
	 xlbl = arrayfun(@(j) sprintf("PC%d", j), 1:rows(ptr), "UniformOutput", false) ;
      else
	 xlbl = strrep(PC.ptr, "_", "\\_") ;
      endif
      for j = 1:columns(ptr)
	 I = ~isnan(ptr(:,j)) ;
	 bar(ptr(I,j))
	 title(ttl = sprintf("%s %s", xds.vars{j}, xds.ids{j})) ;
	 set(gca,'xticklabel', xlbl(I)) ;
	 rotateticklabel(gca, 90) ;
##	 xlabel predictor
	 ylabel influence ;
	 pfile = sprintf("%s/ptr.%s.png", odir, ttl) ;
	 printf("out: --> %s\n", pfile) ;
	 print(pfile) ;
	 clf ;
      endfor
      close ;
   endif

   ## output
   xds.id = xds.id(:,1:3) ;
   switch PAR.output
      case "csv"
	 mwrite_stat(odir, xds) ;
      case "netcdf"
	 ncwrite_stat(odir, xds, PAR.ptr, PAR.ncdir) ;
      otherwise
	 save(sprintf("%s/pdd.ob", odir), "xds") ;
   endswitch

   [In Ix] = ext(xds.x) ;
   PAR.idx.subs = num2cell(In) ;
   xn = subsref(xds.x, PAR.idx) ;
   zn = subsref(xds.x, PAR.idx) ;
   PAR.idx.subs = num2cell(Ix) ;
   xx = subsref(xds.x, PAR.idx) ;
   zx = subsref(xds.z, PAR.idx) ;
   n = ndims(xds.x) ;
   fmt = [repmat("%d ", 1, n) ": %5g %5g\n"] ;
   fid = fopen(efile = fullfile(odir, "ext.csv"), "at") ;
   fprintf(fid, ["min: " fmt "\tmax: " fmt "\n"], In, zn, xn, Ix, zx, xx) ;
   fclose(fid) ;
   printf("out: --> %s\n", efile) ;

   pfile = fullfile(odir, "PAR.txt") ;
   fprintf(fid = fopen(pfile, "wt"), "%s", disp(PAR)) ; fclose(fid) ;
   printf("out: --> %s\n", pfile) ;
   
endfunction
