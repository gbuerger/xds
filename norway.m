rmpath ~/oct ;
CFG = "norway.cfg" ;
args = "\"00011\" \"BLD = 0 ;\"" ;

C = {"2.32", "2.279", "20.2", "76.5", "83.2", "133.7", "152.4", "163.5", "191.2", "311.460"} ;

M = strsplit("...
#HADCM3Q3/RCA,...
#HADCM3Q0/CLM,...
#ECHAM5/RCA,...
#BCM/HIRHAM5,...
#BCM/RCA,...
#HADCM3Q3/HadRM3Q3,...
#ECHAM5/HIRHAM5,...
#ECHAM5/REGCM3,...
#ECHAM5/RACMO,...
ECHAM5/REMO,...
#HADCM3Q0/HadRM3Q0,...
#HADCM3Q16/HadRM3Q16,...
#HADCM3Q16/RCA,...
#ARPEGE/HIRHAM5,...
#ARPEGE/RM5.1...
", ",", 1) ;

J = cellfun(@(x) !any(strcmp(x(1), {"#" "%"})), M) ; M = M(J) ;


for c = C(1)

   for m = M

      rcm = strsplit(m{:}, "/"){2} ;
      ANA = fullfile("ERA40", rcm) ;
      SIM = {fullfile(m{:}, "fut") fullfile(m{:}, "cur")} ;

      fid = fopen("tmp.cfg", "wt") ;
      fputs(fid, ["PAR.pdd = \"" c{:} "\" ;\n"]) ;
      fputs(fid, ["PAR.ana = \"" ANA "\" ;\n"]) ;
      fputs(fid, ["PAR.sim = {\"" SIM{1} "\" \"" SIM{2} "\"} ;\n"]) ;
      fclose(fid) ;
      system(["cat " CFG " >> tmp.cfg"]) ;

      disp(["executing: octave -Hq xds.m tmp.cfg " args]) ;
      st = system(["octave -Hq xds.m tmp.cfg " args]) ;
      if st
	 error("norway: %s, %s", c{:}, m{:}) ;
      endif

      printf("norway: finished %s %s\n", c{:}, m{:}) ;

   endfor

   rename("xds.log", [c{:} ".log"]) ;

endfor
