function cal (L)

   ## usage: cal (L)
   ## 
   ## calibrate model

   global PAR ADIR PRJ clim

   ldata = fullfile("data", PRJ, PAR.pdd) ;

   ofile = sprintf("%s/%s.ob", ldata, PAR.pdd) ;
   vfile = fullfile(ADIR, [strrep(PAR.ana, "/", "_") ".pc.ob"]) ;
   if (isnewer(PAR.mfile, ofile, vfile) && !L) || PAR.ptrout, return ; endif

   if ~isempty(f = dbstack)
      fname = f(1).name ;
      printf("\n\t###   %s   ###\n\n", fname) ;
   else
      fname = "cal" ;
   endif
   
   ws = sprintf("    Calibration of XDS using %s    ", PAR.sim{1}) ; nws = length(ws) ;
   wt1 = "#"(ones(1,7)) ; wt2 = "#"(ones(1,nws)) ;
   printf(["\n\t" wt1 wt2 wt1 "\n"]) ;
   printf(["\t" wt1 ws wt1 "\n"]) ;
   printf(["\t" wt1 wt2 wt1 "\n\n"]) ;

   ## get clim and pdd
   load(cfile = fullfile(ldata, "clim.ob")) ;
   printf("cal: <-- %s\n", cfile) ;
   load(ofile) ;
   printf("cal: <-- %s\n", ofile) ;

   ## get pc of ptr, define X and Y
   load(vfile) ;
   printf("cal: <-- %s\n", vfile) ;

   X = selper(PC, PAR.cper(1,:), PAR.cper(2,:)) ;
   Y = selper(pdd, PAR.cper(1,:), PAR.cper(2,:)) ;
   [X.nr X.nc] = size(X.x) ;
   X.CI = sdate(X.id, PAR.xdscal(1,:), PAR.xdscal(2,:)) ;
   if isfield(PAR, "xdsval")
      X.VI = sdate(X.id, PAR.xdsval(1,:), PAR.xdsval(2,:)) ;
   else
      X.VI = !X.CI ;
   endif
   if strcmp(PAR.optptr, "manual") && (!any(X.CI) || !any(X.VI))
      error("cal: too few values for cal/val") ;
   endif

   if isfield(PAR, "ptridx") && !isempty(PAR.ptridx)
      Jptr = false ;
      for j=1:length(PAR.ptridx)
	 Jptr = Jptr | X.J == PAR.ptridx(j) ;
      endfor
   else
      Jptr = true(1, X.nc) ;
   endif

   if PAR.pcr
      X.x(:,!Jptr) = nan ;
      ## [eof.E eof.PC eof.ev] = pca(X.x, PAR.trunc) ;
      [eof.E eof.PC eof.ev] = pca(X.x) ;
      X.x(:,!Jptr) = 0 ;
      eof.E(isnan(eof.E)) = 0 ;
      X.x(:,Jptr) = X.x * eof.E ;
      [X.nr X.nc] = size(X.x) ; Jptr = true(1, X.nc) ;
      printf("%s: --> %s\n", fname, efile = strrep(PAR.mfile, "mod", "pcr")) ; 
      save(efile, "eof") ;
   endif

   ## select predictors
   if sum(Jptr) < Y.nc
      error("xds:xds", "cal> too few predictors: %d < %d", sum(Jptr), Y.nc) ;
   else
      printf("%s: Using %d potential predictors (PCs).\n", fname, sum(Jptr)) ;
   endif
   
   Jeof = ptrsel(X, Y, Jptr) ;

   [~, fout] = fileparts(PAR.ana) ;
   skl(Jeof, Y, X, sprintf("%s/%s.skl.txt", fileparts(PAR.mfile), fout)) ;

   ## best model \\
   X = selper(PC, PAR.cper(1,:), PAR.cper(2,:)) ;
   if PAR.pcr
      X.x = X.x * eof.E ;
      [X.nr X.nc] = size(X.x) ;   
   endif
   [E R] = do_cal(Y, X, Jeof) ;
   if PAR.pcr
      E = mult(eof.E, E, 2, 1) ;
      R = mult(eof.E, R, 2, 1) ;
   endif
   printf("cal: --> %s\n", PAR.mfile) ; 
   save(PAR.mfile, "E", "R", "Jeof") 
   ## best model //

endfunction


function res = skl (Jeof, Y, X, fout)

   ## usage:  res = skl (Jeof, Y, X, fout)
   ##
   ## validate vars rel. to number of EOFs

   [st idx] = dbstack() ;
   pkg load statistics
   global PAR ADIR clim
   
   wX = X ; wY = Y ; wX.x(!X.CI,:) = nan ; wY.z(!X.CI,:) = nan ;
   [E R] = do_cal(wY, wX, Jeof) ;

   ## validation
   y = nan(rows(X.x), size(E, 2)) ;
   for is = 1:length(PAR.ssn)
      I = selssn(PAR.ssn{is}, X.id) ;
      y(I,:) = nanmult(X.x(I,:), E(:,:,is)) ;
   endfor

   if strcmp(PAR.optptr, "manual")
      ncpu = PAR.ncpu ; # ugly
      PAR.ncpu = 1 ;
      PAR.dbg = false ;
      pp = feval(PAR.adjfun, y) ;
      y = feval(PAR.adjfun, y, pp) ;
      PAR.ncpu = ncpu ;
   endif

   VAR = unique(Y.vars) ; cres = vres = [] ; dVAR = {} ;
   J0 = ind2log(PAR.validx, Y.nc) ;

   for v = VAR
      J = strcmp(Y.vars, v{:}) & J0 ;
      if !any(J), continue ; endif
      dVAR = union(dVAR, v) ;
      w = cell(1, 4) ;
      o = nanmean(y(X.CI,J), 2) ;
      f = nanmean(Y.z(X.CI,J), 2) ;
      [w{:}] = NaN_(@re, o, f) ;
      cres = [cres w{PAR.valscore}] ;
      if length(st) < 2
	 printf("cal>skl: --> %s\n", fullfile(ADIR, [v{:} ".cal.csv"]))
	 csvwrite(fullfile(ADIR, [v{:} ".cal.csv"]), [o f]) ;
      endif
      o = nanmean(y(X.VI,J), 2) ;
      f = nanmean(Y.z(X.VI,J), 2) ;
      [w{:}] = NaN_(@re, o, f) ;
      vres = [vres w{PAR.valscore}] ;
      if length(st) < 2
	 printf("cal>skl: --> %s\n", fullfile(ADIR, [v{:} ".val.csv"]))
	 csvwrite(fullfile(ADIR, [v{:} ".val.csv"]), [o f]) ;
      endif
   endfor
   res = [cres ; vres] ;
   if nargin > 3
      fid = fopen(fout, "wt") ;
      if ~isempty(f = dbstack)
	 fname = f(1).name ;
      else
	 fname = "cal" ;
      endif
      score = {"EV" "correlation" "mean bias" "std bias"} ;
      fprintf(stdout, "%s: calibration/validation %s skill ([%%]):\n", fname, score{PAR.valscore}) ;
      fprintf(fid, "%s: calibration/validation %s skill ([%%]):\n", fname, score{PAR.valscore}) ;
      displaytable(100*res', {"cal" "val"}, 5, {".0f"}, dVAR, stdout, " ") ;
      displaytable(100*res', {"cal" "val"}, 5, {".0f"}, dVAR, fid, " ") ;
      fclose(fid) ;
      printf("cal>skl: --> %s\n", fout)
   endif

   if !PAR.kstest, return ; endif 

   wrn = warning ;
   warning("off") ;

   cres = vres = [] ;
   for v = VAR
      J = strcmp(Y.vars, v{:}) & J0 ;
      if !any(J), continue ; endif
      wy = zscore(y(X.CI,J)) ;
      for j = 1:columns(wy)
	 ks(j) = kolmogorov_smirnov_test(wy(:,j), "norm", 0, 1) ;
      endfor
      cres = [cres mean(ks)] ;
      wy = zscore(y(X.VI,J)) ;
      for j = 1:columns(wy)
	 ks(j) = kolmogorov_smirnov_test(wy(:,j), "norm", 0, 1) ;
      endfor
      vres = [vres mean(ks)] ;
   endfor
   res = [res ; [cres ; vres]] ;

   warning(wrn) ;

endfunction


function neof = neof_fig (NEOF, res, VAR)

   ## usage:  neof = neof_fig (NEOF, res, VAR)
   ##
   ## estimate neof from figures

   global PAR

   if (PAR.valscore == 1)
      ylbl = "EV [%]" ;
   else
      ylbl = "rho" ;
   endif

   nc = 2 + 2*PAR.kstest ; nV = length(VAR) ;
   set(0, "defaulttextfontsize", 8, "defaultaxesfontsize", 8) ;

   for j = 1:nV
      v = VAR{j} ;
      subplot(nc,nV,j) ;
      plot(NEOF', squeeze(res(1,j,:))) ; set(gca, "title", ["cal(" v ")"], "xlabel", "neof", "ylabel", ylbl) ;
      subplot(nc,nV,nV+j) ;
      plot(NEOF', squeeze(res(2,j,:))) ; set(gca, "title", ["val(" v ")"], "xlabel", "neof", "ylabel", ylbl) ;
      if PAR.kstest
	 subplot(nc,nV,2*nV+j) ;
	 plot(NEOF', squeeze(res(3,j,:))) ; title(["KS(cal " v ")"]) ;
	 subplot(nc,nV,3*nV+j) ;
	 plot(NEOF', squeeze(res(4,j,:))) ; title(["KS(val " v ")"]) ;
      endif
   endfor
   if PAR.dbg, print([fileparts(PAR.mfile) "/cal.png"]) ; endif

   if !strcmp(getenv("TERM"), "dumb")
      printf("cal: How many predictors (shown on x-axis)?\n") ;
      printf("cal: Suggested: 3-10 x number of local variables,\n") ;
      printf("cal: use neof with %d <= NEOF <= %d\n", NEOF([1 end])) ;
      printf("cal: use \"<var>\" (with quotes!) for optimum var\n") ;

      in = input("cal: NEOF = ") ;
   else
      in = NEOF(end) ;
      sleep(3) ;
   endif
   close ;

   if isnumeric(in) && in >= NEOF(1) && in <= NEOF(end)
      neof = in ;
   elseif ischar(in)
      jv = find(strcmp(VAR, in)) ;      
      [w j] = max(res(2,jv,:)) ;
      neof = NEOF(j) ;
   else
      error("cal: NEOF invalid, exiting.\n")
   endif 

endfunction


function Jeof = ptrsel (X, Y, Jptr)

   ## usage: Jeof = ptrsel (X, Y, Jptr)
   ## 
   ## predictor selection

   global PAR PRJ

   if ~isempty(f = dbstack)
      fname = f(1).name ;
   else
      fname = "cal" ;
   endif


   VAR = unique(Y.vars) ;
   Xj = Xjfun(X.J) ;

   Jeof = false(1,X.nc) ;
##   I = !any(isnan([X.x Y.z]), 2) ;
   I = sum(~isnan([X.x Y.z]), 2) ./ (X.nc + Y.nc) > PAR.npdd ;

   switch PAR.optptr

      case "mtl"
	 b_opt = zeros(1, X.nc) ;
	 [x, ~, ~] = anom(X.x(I,Jptr)) ;
	 [y, ~, ~] = anom(Y.z(I,:)) ;
	 Jeof(Jptr) = mtl(x, y) ;

      case "multi"
	 b_opt = zeros(1, X.nc) ;
	 [x, ~, ~] = anom(X.x(I,Jptr)) ;
	 [y, ~, ~] = anom(Y.z(I,:)) ;
	 jit_enable(0) ; tic ;
	 Jeof(Jptr) = local_loop(x, y) ;
	 toc
	 jit_enable(1) ; tic ;
	 Jeof(Jptr) = local_loop(x, y) ;
	 toc
	 
      case {"cv", "mean"}
	 for v = VAR
	    b_opt = zeros(1, X.nc) ;
	    Jv = strcmp(Y.vars, v) ;
	    [x, ~, ~] = anom(X.x(:,Jptr)) ;
	    [y, ~, ~] = anom(nanmean(Y.z(:,Jv), 2)) ;
	    I = !any(isnan([x y]), 2) ;
	    x = x(I,:) ; y = y(I,:) ;
	    [s_opt, b_opt(Jptr)] = crossvalidate(@lars, PAR.kfold, steps=100, x, y) ;
	    if PAR.dbg
	       k_opt = ceil(s_opt*X.nc) ;
	       plot((1:k_opt)'/X.nc, b_opt(1:k_opt)) ;
	       print(fullfile("data", PRJ, PAR.pdd, "ptrsel.png")) ;
	       close ;
	    endif
	    Jeof = Jeof | b_opt != 0 ;
	 endfor

	 ## y = nansum([yP, yT, yP.*yT], 2) ;
	 ## [s_opt, b_opt(Jptr)] = crossvalidate(@lars, PAR.kfold, steps=100, zscore(X.x(I,Jptr)), y, PAR.reg) ;
	 ## Jeof = b_opt != 0 ;

      case "num"
	 for jv = 1:length(VAR)
	    v = VAR{jv} ;
	    b_opt = zeros(1, X.nc) ;
	    Jv = strcmp(Y.vars, v) ;
	    x = zscore(X.x(I,Jptr)) ;
	    y = zscore(mean(Y.z(I,Jv), 2)) ;
	    if isnumeric(PAR.reg)
	       nptr = ceil(PAR.reg * length(Jv)) ;
	       w = lars(x, y, [], -nptr) ;
	    else
	       w = lars(x, y) ;
	    endif
	    b_opt(Jptr) = w(end,:) ;
	    Jeof = Jeof | b_opt != 0 ;
	 endfor

	 ## y = nansum([yP, yT, yP.*yT], 2) ;
	 ## [s_opt, b_opt(Jptr)] = crossvalidate(@lars, PAR.kfold, steps=100, zscore(X.x(I,Jptr)), y, PAR.reg) ;
	 ## Jeof = b_opt != 0 ;

      case "manual"
	 if !exist("NODISP", "var") || !ischar(NODISP), NODISP = "" ; endif 

	 NEOF = Y.nc:sum(Jptr) ;
	 if isempty(NEOF)
	    error("cal: too few predictors. %d < %d\n", sum(Jptr), Y.nc)
	 endif

	 pkg load nan
	 [X.PJ C] = ptrSort(X.x(:,Jptr), X.J(Jptr), Y, PAR.ptrsel) ;
	 pkg unload nan
	 Xp = (1:X.nc)(Jptr)(X.PJ) ;
	 if PAR.dbg
	    ptrlist = X.ptr(X.J(Xp)) ;
	    printf("\n   #:\tP ptr     \tno. \tcorr\t||  #:\tT ptr     \tno. \tcorr\n\n") ;
	    for j=1:length(ptrlist)/2
	       jP = 2*j-1 ; jT = 2*j ;
	       Pstr = sprintf("%-10s", ptrlist{jP}) ; Tstr = sprintf("%-10s", ptrlist{jT}) ;
	       XjP = Xj(Xp(jP)) ; XjT = Xj(Xp(jT)) ; Pc = abs(C(jP)) ; Tc = abs(C(jT)) ; 
	       printf("%4d:\t%s\t%3d\t%3.2f\t||%4d:\t%s\t%3d\t%3.2f\n", jP, Pstr, XjP, Pc, jT, Tstr, XjT, Tc) ;
	    endfor 
	 endif

	 JEOF = cell(1, length(NEOF)) ; JEOF = cellfun(@(j) Xp(1:j), num2cell(NEOF), "UniformOutput", false) ;
	 res = parfun(@(J) skl(J, Y, X), JEOF, PAR.par_opt{1:2}) ;
	 res = reshape(cell2mat(res), 2 + 2*PAR.kstest, [], length(NEOF)) ;

	 neof = neof_fig(NEOF, res, VAR) ;
	 Jeof = find2ind(Xp(1:neof), X.nc) ;

      case "stw"
	 if ~PAR.bld & exist(file = [ldata "/ptr_stw.ob"])
	    load(file) ;
	 else
	    wX = X ; wX.x = X.x(:,Jptr) ;
	    stw = stw_fun(X, Y) ;
	    save(file, "stw") ;
	 endif
	 Jeof = stw{4} ;

      case "none"
	   Jeof = Jptr ;

      otherwise
	 warning("xds:xds", "cal: no ptr selection specified, using all predictors") ;
	 Jeof = true(1, X.nc) & Jptr ;

   endswitch

   if sum(Jeof) < Y.nc
      warning("xds:xds", "%s:\t%d predictors selected, but at least %d (= number of predictands) are needed.\n", fname, sum(Jeof), Y.nc) ;
   endif

   J = find(Jeof) ;
   printf("\n%s(%s):\tNumber of predictors:\t%d\n", fname, PAR.optptr, sum(Jeof)) ;
   printf("%s:\tNumber of predictands:\t%d\n\n", fname, Y.nc) ;
   printf("%s: --> %s\n", fname, PAR.plist) ; 
   fid = fopen(PAR.plist, "w") ;
   if PAR.pcr
      for j=1:sum(Jeof)
	 fprintf(fid, "%d: EOF %d\n", j, J(j)) ;
      endfor
   else
      for j=1:sum(Jeof)
	 fprintf(fid, "%d -> %d:\t%s\t%d\n", j, J(j), X.ptr(X.J)(Jeof){j}, Xj(Jeof)(j)) ;
      endfor
   endif
   uJ = unique(X.J(J)) ;
   fprintf(fid, ["\n" repmat("%d ", 1, length(uJ)) "\n"], uJ) ;
   fclose(fid) ;

endfunction


function [E, R] = do_cal (Y, X, Jeof)

   ## usage: [E, R] = do_cal (Y, X, Jeof)
   ## 
   ## calibrate models using Jeof predictors

   f = dbstack ;
   global PAR
   
   nJ = sum(Jeof) ;
   if ltrunc = (isnumeric(PAR.ltrunc) && Y.nc > nJ)
      warning("xds:xds", "%s: we use local EOFs (%d > %d)", f(1).name, Y.nc, nJ) ;
      vars = sunique(Y.vars) ; nv = length(vars) ;
      PAR.ltrunc /= sum(PAR.ltrunc) ;
      PAR.ltrunc = floor(PAR.ltrunc * nJ) ;
   endif
   
   for is = 1:length(PAR.ssn)
      [Iy Ix] = selssn(PAR.ssn{is}, Y.id, X.id) ;
      ## adjust local fields if too large
      if ltrunc
	 y = [] ; Ey = {} ;
	 for jv = 1:nv
	    J = strcmp(Y.vars, vars{jv}) ;
	    [wEy wPCy] = pca(Y.z(Iy,J), -PAR.ltrunc(jv)) ;
	    Ey = [Ey wEy'] ;
	    y = [y wPCy] ;
	 endfor
	 if nv == 1
	    yc = nanmult(Ey, Y.cov(:,:,is), Ey') ;
	 else
	    yc = nancov(y) ;
	 endif
      else
	 y = Y.z(Iy,:) ;
	 yc = Y.cov(:,:,is) ;
      endif
      
      ## XDS
      wE = xrgr(y, X.x(Ix,Jeof), "xds", yc) ;
      wR = xrgr(y, X.x(Ix,Jeof), "rgr", yc) ;

      if ltrunc
	 Ey = blkdiag(Ey{:}) ;
	 wE = wE * Ey ;
	 wR = wR * Ey ;
      endif
      E(:,:,is) = R(:,:,is) = zeros(X.nc, Y.nc) ; E(Jeof,:,is) = wE ; R(Jeof,:,is) = wR ;
   endfor

endfunction


## usage: Jx = local_loop (x, y)
##
## find optimum LARS smoothing
function Jx = local_loop (x, y)

   global PAR
   
   C = num2cell(y, 1) ;
   beta = parfun(@(c) lars(x, c), C, PAR.par_opt{:}) ;

   Nx = columns(x) ; Ny = columns(y) ; Jx = false(1,Nx) ;
   for iy = 1:Ny

      b = beta{iy} ;
      ix = 0 ;
      while ~any(I = (b(++ix,:) ~= 0 & ~Jx))
      endwhile
      [~,j] = max(abs(b(ix,I))) ;
      Jx(find(I)(j)) = true ;

   endfor
   
endfunction
