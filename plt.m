function plt (L=true)

   ## usage: plt (L)
   ## 
   ## plot results

   global PAR PRJ GVAR ADIR SDIR CDIR BATCH Lavg per DRATE
   persistent fig
   
   idx.type = "()" ;
   Lptr = ~true ; wLptr = false ;
   mth = {"J" "F" "M" "A" "M" "J" "J" "A" "S" "O" "N" "D"} ;
   P0 = 86400 ; T0 = 273.16 ;
   LEG = {"OBS", "ANA", "CUR", "FUT"} ;
   if PAR.xdscal(1,1) <= PAR.plty & PAR.plty <= PAR.xdscal(2,1)
      warning("xds:xds", "plt> plot year %d within calibration period [%d %d]", PAR.plty, PAR.xdscal(:,1)) ;
   endif
   per = [PAR.plty 1 1 ; PAR.plty 12 31] ;
   cur.per = PAR.cper(:,1)' ; fut.per = [2070 2100] ;
   fut.per = [-Inf Inf] ;
   Lavg = true ;
   sfx = "svg" ;
   
   if ~isempty(f = dbstack)
      fname = f(1).name ;
      printf("\n\t###   %s   ###\n\n", fname) ;
   else
      fname = "plt" ;
   endif

   ddir = PAR.pdd ;
   if PAR.ptrout ddir = [ddir ".ptrout"] ; endif
   
   pmkdir(pdir = fullfile(SDIR, ddir, "plt")) ;
   if ~isnewer(fullfile(SDIR, PAR.pdd, "/PAR.txt"), pdir) || ~L return ; endif
   
##   delete(sprintf("%s/*.png", pdir)) ;
   METAFILE = sprintf("data/%s/%s/meta.txt", PRJ, PAR.pdd) ;

   annarg = {} ; Sfun = {"mean" "max"} ;
   # annarg = {"f"} ; sfun = @(x,p) P_stat(x,p) ;

   ## plot parameters
   set(0, "defaultaxesfontname", "Liberation Sans", "defaultaxesfontsize", 12) ;
   set(0, "defaultaxescolororder", [0 0 0; jet(7)]) ;
   set(0, "defaulttextfontname", "Linux Biolinum", "defaulttextfontsize", 12, "defaultlinelinewidth", 2) ;
   set(0, "defaultfigureunits", "normalized") ;
   set(0, "defaultfigurepaperunits", "normalized") ;
   set(0, "defaultfigureposition", [0.9 1 0.2 0.2]) ;
   lw = 1*[2 1 0.5 0.5] ; ls = {"-" "-" "-" "-"} ;
   if 0 & (1 | BATCH)
      vis = "off" ;
   else
      vis = "on" ;
   endif

   pg = [2 2] ; q = 0.1 ; legpos = "northeastoutside" ;
   cmap = [0 0 0; 0 0 1; rainbow(16)([10 2],:)] ;

   [ids, vars, names, lons, lats, alts, undefs, J] = meta(METAFILE) ;
   VAR = sunique(vars) ; VARNAME = sunique(names) ;

   if Lptr
      printf("plt: <-- %s\n", file = sprintf("data/%s/ptr/%s/ptr.ob", PRJ, PAR.ana)) ;
      load(file) ;
      if isempty(GVAR)
	 GVAR = [fieldnames(ptr) fieldnames(ptr)]' ;
      endif
      p_ana.ptr = SelFld(ptr, GVAR(1,:)) ;
      printf("plt: <-- %s\n", file = fullfile(CDIR, "ptr.ob")) ;
      load(file) ; cur.ptr = SelFld(ptr, GVAR(2,:)) ; ;
      printf("plt: <-- %s\n", file = fullfile(SDIR, "ptr.ob")) ;
      load(file) ; fut.ptr = SelFld(ptr, GVAR(2,:)) ;
      clear ptr ;
   endif

   for jv=1:length(VAR)

      if !any(J{jv}) continue ; endif
      v = vars{J{jv}(1)} ; vn = names{J{jv}(1)} ; vv = strrep(v, ".", "_") ;

      if PAR.ptrout && isempty(regexp(vn, "precip")) continue ; endif

      if (exist("V", "var") && !any(strcmp(v, V))) continue ; endif
      vu = punit(v) ;

      printf("plt: <-- %s\n", ifile = sprintf("data/%s/%s/%s.csv", PRJ, PAR.pdd, v)) ;
      obs = read_stat(ifile) ;

      ## select ids from meta
      [Jm Js Km Ks] = unify(ids(J{jv})', obs.ids) ;

      obs.ids = ids(J{jv})(Jm)' ;
      obs.lons = lons(J{jv})(Jm)' ;
      obs.lats = lats(J{jv})(Jm)' ;
      obs.alts = alts(J{jv})(Jm)' ;
      obs.udf = undefs(J{jv})(Jm)' ;
      obs.x = obs.x(:,Ks) ; obs.x = lim2nan(obs.x, obs.udf) ;
      TP.obs.(v).id = obs.id ; TP.obs.(v).x = obs.x ; TP.obs.(v).ll = [obs.lons ; obs.lats] ;

      printf("plt: <-- %s\n", ifile = fullfile(ADIR, ddir, [vv ".csv"])) ;
      ana = read_stat(ifile) ;
      ana.x = lim2nan(ana.x, obs.udf) ;
      TP.ana.(v).id = ana.id ; TP.ana.(v).x = ana.x ; TP.ana.(v).ll = TP.obs.(v).ll ;

      if ~isempty(ifile = glob([fullfile(SDIR, ddir) "/plt/" vv "*.csv"]))
	 ifile = ifile{1} ;
	 printf("plt: <-- %s\n", ifile) ;
	 sim1 = read_stat(ifile) ;
	 sim1.x = lim2nan(sim1.x, obs.udf) ;
      elseif ~isempty(F = glob(sprintf("%s/%s/[0-9][0-9]/%s.*-%s.csv", SDIR, ddir, vv, PAR.pltiss))')
	 sim1 = [] ;
	 printf("plt: <-- %s\n", F{1}) ;
	 printf("plt: ...\n", F{end}) ;
	 for ifile = F
	    s = read_stat(ifile{:}) ;
	    sim1 = join(sim1, s) ;
	 endfor
	 sim1.x = reshape(sim1.x, rows(sim1.id), [], length(F)) ;
	 for j = 1:length(F)
	    sim1.x(:,:,j) = lim2nan(sim1.x(:,:,j), obs.udf) ;
	 endfor
	 printf("plt: <-- %s\n", F{end}) ;
      elseif PAR.ptrout && ~isempty(F = glob(sprintf("%s/%s/*%s*.csv", SDIR, ddir, vv))')
	 sim1 = [] ;
	 printf("plt: <-- %s\n", F{1}) ;
	 printf("plt: ...\n", F{end}) ;
	 for ifile = F
	    s = read_stat(ifile{:}) ;
	    sim1 = join(sim1, s) ;
	 endfor
	 sim1.x = reshape(sim1.x, rows(sim1.id), [], length(F)) ;
	 for j = 1:length(F)
	    sim1.x(:,:,j) = lim2nan(sim1.x(:,:,j), obs.udf(1)) ;
	 endfor
	 printf("plt: <-- %s\n", F{end}) ;
      else
	 warning("xds:xds", "no files found") ;
	 return ;
	 ##	 ifile = glob([fullfile(CDIR, ddir) "/" vv "*.csv"]){1} ;
      endif
      TP.sim1.(v).id = sim1.id ; TP.sim1.(v).x = sim1.x ; TP.sim1.(v).ll = TP.obs.(v).ll ;

      ifile = glob([fullfile(CDIR, ddir) "/" vv "*.csv"]){1} ;
      printf("plt: <-- %s\n", ifile) ;
      sim2 = read_stat(ifile) ;
      sim2.x = lim2nan(sim2.x, obs.udf) ;
      TP.sim2.(v).id = sim2.id ; TP.sim2.(v).x = sim2.x ; TP.sim2.(v).ll = TP.obs.(v).ll ;

      if PAR.mskobs
	 idx.subs = repmat({":"}, 1, columns(obs.x)) ;
	 [obs.I ana.I] = common(obs.id, ana.id) ;
	 I = isnan(obs.x(obs.I,:)) ;
	 w = ana.x(ana.I,:) ; w(I) = NaN ; ana.x(ana.I,:) = w ;
	 [obs.I sim2.I] = common(obs.id, sim2.id) ;
	 I = isnan(obs.x(obs.I,:)) ;
	 w = sim2.x(sim2.I,:) ; w(I) = NaN ; sim2.x(sim2.I,:) = w ;
      endif

      if Lptr
	 [p_ana.Y wLptr] = ptr_loc(p_ana.ptr, vn, lons, lats) ;
	 if wLptr
	    printf("plt: pairing %s with %s\n", vn, p_ana.Y.name) ;
	    printf("plt: using:\n") ;
	    printf(["\t" repmat("%d ", 1, length(p_ana.Y.K)) "\n"], p_ana.Y.K) ;
	    printf(["\t" repmat("%d ", 1, length(p_ana.Y.lon)) "\n"], p_ana.Y.lon) ;
	    printf(["\t" repmat("%d ", 1, length(p_ana.Y.lat)) "\n"], p_ana.Y.lat) ;
	    ## ana temperature
	    if (isT = ~isempty(regexp(vn, "temp"))) && nanmean(p_ana.Y.x) > 100
	       p_ana.Y.x = p_ana.Y.x - T0 ;
	    endif
	    ## ana precip
	    if isP = ~isempty(regexp(vn, "precip"))
	       if strcmp(PAR.ana, "ERAi") || regexp(PAR.ana, "/ei")
		  p_ana.Y.x = p_ana.Y.x * 1000 ;
	       else
		  p_ana.Y.x = p_ana.Y.x * P0 ;
	       endif
	    endif
	    TP.p_ana.(v).id = p_ana.Y.id ; TP.p_ana.(v).x = p_ana.Y.x ; TP.p_ana.(v).ll = [p_ana.Y.lon' ; p_ana.Y.lat'] ;
	    cur.Y = ptr_loc(cur.ptr, vn, lons, lats) ;
	    printf("plt: pairing %s with %s\n", vn, cur.Y.name) ;
	    if isT && nanmean(cur.Y.x) > 100
	       cur.Y.x = cur.Y.x - T0 ;
	    endif
	    if isP
	       cur.Y.x = cur.Y.x * P0 ;
	    endif
	    TP.cur.(v).id = cur.Y.id ; TP.cur.(v).x = cur.Y.x ; TP.cur.(v).ll = [cur.Y.lon' ; cur.Y.lat'] ;
	    fut.Y = ptr_loc(fut.ptr, vn, lons, lats) ;
	    printf("plt: pairing %s with %s\n", vn, fut.Y.name) ;
	    if isT && nanmean(fut.Y.x) > 100
	       fut.Y.x = fut.Y.x - T0 ;
	    endif
	    if isP
	       fut.Y.x = fut.Y.x * P0 ;
	    endif
	    TP.fut.(v).id = fut.Y.id ; TP.fut.(v).x = fut.Y.x ; TP.fut.(v).ll = [fut.Y.lon' ; fut.Y.lat'] ;
	 endif
      endif
      ## continue ;

      if Lavg
	 obs.x = nanmean(obs.x, 2) ;
	 ana.x = nanmean(ana.x, 2) ;
	 sim1.x = nanmean(sim1.x, 2) ;
	 sim2.x = nanmean(sim2.x, 2) ;
      endif

      for k=1:length(Sfun)
	 sfun = Sfun{k} ;
	 [obs.(sfun).id obs.(sfun).x] = annstat(obs.id, obs.x, sfun, annarg{:}) ;
	 [ana.(sfun).id ana.(sfun).x] = annstat(ana.id, ana.x, sfun, annarg{:}) ;
	 wDRATE = DRATE ;
	 DRATE = 0.45 ;
	 [sim1.(sfun).id sim1.(sfun).x] = annstat(sim1.id, sim1.x, sfun, annarg{:}) ;
	 DRATE = wDRATE ;
	 [sim2.(sfun).id sim2.(sfun).x] = annstat(sim2.id, sim2.x, sfun, annarg{:}) ;
	 if wLptr
	    [p_ana.(sfun).id p_ana.(sfun).x] = annstat(p_ana.Y.id, p_ana.Y.x, sfun, annarg{:}) ;
	    [cur.(sfun).id cur.(sfun).x] = annstat(cur.Y.id, cur.Y.x, sfun, annarg{:}) ;
	    [fut.(sfun).id fut.(sfun).x] = annstat(fut.Y.id, fut.Y.x, sfun, annarg{:}) ;
	 endif
      endfor

      for j=1:columns(obs.x)

	 if Lavg
	    st = "areal mean" ;
	 else
	    st = num2str(obs.ids{j}) ;
	    st = strrep(st, " ", "_") ;
	 endif
	 wst = strrep(st, "_", "\\_") ;
	 wst = strrep(wst, " ", "-") ;

	 if isempty(fig)
	    fig = figure("visible", vis, "position", [0.7 0.6 0.3 0.4], "paperpositionmode", "auto") ;
	    cla ;
	    print(sprintf("/tmp/foo.%s", sfx)) ;
	 endif
	 
	 ## annual timeseries
	 yfile = sprintf("%s/%s.%s.y.%s", pdir, v, wst, sfx) ;

	 for k=1:2
	    sfun = Sfun{k} ;
	    subplot(2, 1, k) ; hold on

	    obs.h = plot(obs.(sfun).id(:,1), obs.(sfun).x(:,j), "linewidth", lw(1), "linestyle", ls{1}, "color", cmap(1,:)) ;

	    if wLptr
##	       p_ana.l = sprintf("%s (%.1f,%.1f)", PAR.ana, p_ana.Y.lon(j), p_ana.Y.lat(j)) ;
##	       cur.l = sprintf("%s (%.1f,%.1f)", PAR.sim{2}, cur.Y.lon(j), cur.Y.lat(j)) ;
##	       fut.l = sprintf("%s (%.1f,%.1f)", PAR.sim{1}, fut.Y.lon(j), fut.Y.lat(j)) ;
	       p_ana.l = sprintf("ANA(%.1f,%.1f)", p_ana.Y.lon(j), p_ana.Y.lat(j)) ;
	       cur.l = sprintf("CUR(%.1f,%.1f)", cur.Y.lon(j), cur.Y.lat(j)) ;
	       fut.l = sprintf("FUT(%.1f,%.1f)", fut.Y.lon(j), fut.Y.lat(j)) ;
	       ##hptr = copyobj(gca, gcf) ; delete(get(hptr, "children")) ; delete(hl) ;
	       ##axes(hptr) ; set(hptr, "color", "none") ;

	       jp = 1 ;
	       ax1 = gca ;
	       if 1 # no axes opacity yet
		  ax2 = ax1 ;
	       else
		  ax2 = copyobj(ax1, gcf) ;
	       endif
##	       set(ax2, "yaxislocation", "right") ;

	       jp++ ;
	       ana.h = plot(ax1, ana.(sfun).id(:,1), ana.(sfun).x(:,j), "linewidth", lw(jp), "linestyle", ls{jp}, "color", cmap(jp,:)) ;
	       p_ana.h = plot(ax2, p_ana.(sfun).id(:,1), p_ana.(sfun).x(:,j), "linewidth", lw(jp)/2, "color", cmap(jp,:), "linestyle", "--") ;

	       jp++ ;
	       sim2.h = plot(ax1, sim2.(sfun).id(:,1), sim2.(sfun).x(:,j), "linewidth", lw(jp), "linestyle", ls{jp}, "color", cmap(jp,:)) ;
	       cur.h = plot(ax2, cur.(sfun).id(:,1), cur.(sfun).x(:,j), "linewidth", lw(jp)/2, "color", cmap(jp,:), "linestyle", "--") ;
	       jp++ ;
	       sim1.h = plot(ax1, sim1.(sfun).id(:,1), sim1.(sfun).x(:,j), "linewidth", lw(jp), "linestyle", ls{jp}, "color", cmap(jp,:)) ;
	       fut.h = plot(ax2, fut.(sfun).id(:,1), fut.(sfun).x(:,j), "linewidth", lw(jp)/2, "color", cmap(jp,:), "linestyle", "--") ;

	       [hl, hl_obj, hplot, labels] = legend([obs.h ana.h sim2.h sim1.h], {"OBS" "ANA" "CUR" "FUT"}, "location", legpos) ;
	       legend("boxoff") ;

	    else

	       plot(ana.(sfun).id(:,1), ana.(sfun).x(:,j), "linewidth", lw(2), "linestyle", ls{2}, "color", cmap(2,:)) ;
	       ##axis([1900 2100]) ;
	       plot(sim2.(sfun).id(:,1), sim2.(sfun).x(:,j), "linewidth", lw(3), "linestyle", ls{3}, "color", cmap(3,:)) ;
	       if ndims(sim1.(sfun).x) > 2
		  [lx ux] = bounds(sim1.(sfun).x(:,j,:)) ;
		  plot(sim1.(sfun).id(:,[1 1]), [lx ux], "-", "color", cmap(4,:), "markerfacecolor", cmap(4,:), "markeredgecolor", cmap(4,:)) ;
	       else
		  plot(sim1.(sfun).id(:,1), sim1.(sfun).x(:,j), "linewidth", lw(4), "linestyle", ls{4}, "color", cmap(4,:)) ;
	       endif
	       legend(LEG, "location", legpos) ;
	       legend("boxoff") ;
	       legend("right") ;
	    endif
	    set(gca, "ygrid", "on", "gridalpha", 0.6) ;
	    xlabel("year") ;
	    vs = strrep(v, "TN", "T_n") ;
	    vs = strrep(vs, "Tn", "T_n") ;
	    vs = strrep(vs, "TX", "T_x") ;
	    vs = strrep(vs, "Tx", "T_x") ;
	    ylabel(sprintf("annual %s %s  %s", sfun, vs, vu)) ;
	    title([vs ", " wst]) ;
	 endfor

	 printf("plt: --> %s\n", yfile) ;
	 print(yfile) ; pause(1) ;
	 printf("plt: --> %s\n", yplot=strrep(yfile, ["." sfx], ".og")) ;
	 hgsave(yplot) ;
	 clf ;

	 ## monthly means and boxplots
	 bpfile = sprintf("%s/%s.%s.bp.%s", pdir, v, wst, sfx) ;
	 box.xm{1} = box.xm{2} = box.xm{3} = box.xm{4} = {} ;
	 for m = 1:12
	    obs.I = obs.id(:,2) == m ;
	    ana.I = ana.id(:,2) == m ;
	    sim1.I = sim1.id(:,2) == m ;
	    sim2.I = sim2.id(:,2) == m ;
	    if IM(m) = and(any(obs.I),any(ana.I),any(sim1.I),any(sim2.I))
	       box.xm{1}{m} = obs.x(obs.I,j) ;
	       box.xm{2}{m} = ana.x(ana.I,j) ;
	       box.xm{3}{m} = sim2.x(sim2.I,j) ;
	       box.xm{4}{m} = sim1.x(sim1.I,j) ;
	    else
	       box.xm{1}{m} = NaN * obs.x(obs.I,j) ;
	       box.xm{2}{m} = NaN * ana.x(ana.I,j) ;
	       box.xm{3}{m} = NaN * sim2.x(sim2.I,j) ;
	       box.xm{4}{m} = NaN * sim1.x(sim1.I,j) ;
	    endif
	 endfor
	 hg = aboxplot(box.xm, "labels", {mth(IM){:}}, "colormap", cmap) ;
	 legend(hg, LEG, "location", legpos) ;
	 legend("boxoff") ;
	 legend("right") ;
	 xlabel("month") ;
	 vs = strrep(v, "TN", "T_n") ;
	 vs = strrep(vs, "Tn", "T_n") ;
	 vs = strrep(vs, "TX", "T_x") ;
	 vs = strrep(vs, "Tx", "T_x") ;
	 ylabel(sprintf("%s  %s", vs, vu)) ;
	 title([vs ", " wst]) ;
	 printf("plt: --> %s\n", bpfile) ;
	 print(bpfile, "-r600", "-portrait") ; pause(3) ;
	 printf("plt: --> %s\n", bpplot=strrep(bpfile, ["." sfx], ".og")) ;
	 hgsave(bpplot) ;
	 clf ;

	 if wLptr
	    mfile = sprintf("%s/%s_glb.%s.m.%s", pdir, v, wst, sfx) ;
	    box.mm = [] ;
	    for m=1:12
	       p_ana.Y.I = p_ana.Y.id(:,2) == m & cur.per(1) <= p_ana.Y.id(:,1) & p_ana.Y.id(:,1) <= cur.per(2) ;
	       fut.Y.I = fut.Y.id(:,2) == m & fut.per(1) <= fut.Y.id(:,1) & fut.Y.id(:,1) <= fut.per(2) ;
	       box.mm(m,1) = nanmean(p_ana.Y.x(p_ana.Y.I,j)) ;
	       cur.Y.I = cur.Y.id(:,2) == m & cur.per(1) <= cur.Y.id(:,1) & cur.Y.id(:,1) <= cur.per(2) ;
	       box.mm(m,2) = nanmean(cur.Y.x(cur.Y.I,j)) ;
	       box.mm(m,3) = nanmean(fut.Y.x(fut.Y.I,j)) ;
	    endfor
	    hg = bar(box.mm) ;
	    colormap(cmap(2:end,:))
	    set(gca,'xticklabel', mth) 
	    legend(hg, LEG(2:end), "location", legpos) ;
	    legend("boxoff") ;
	    legend("right") ;
	    xlabel("month") ;
	    vs = strrep(v, "TN", "T_n") ;
	    vs = strrep(vs, "Tn", "T_n") ;
	    vs = strrep(vs, "TX", "T_x") ;
	    vs = strrep(vs, "Tx", "T_x") ;
	    ylabel(sprintf("%s  %s", vs, vu)) ;
	    title([vs "\\_glb, " wst]) ;
	    printf("plt: --> %s\n", mfile) ;
	    print(mfile, "-r600", "-portrait") ;
	    printf("plt: --> %s\n", mplot=strrep(mfile, ["." sfx], ".og")) ;
	    hgsave(mplot) ;
	    clf ;
	 endif
	 
	 mfile = sprintf("%s/%s.%s.m.%s", pdir, v, wst, sfx) ;
	 box.mm = [] ;
	 for m=1:12
	    obs.I = obs.id(:,2) == m & cur.per(1) <= obs.id(:,1) & obs.id(:,1) <= cur.per(2) ;
	    ana.I = ana.id(:,2) == m & cur.per(1) <= ana.id(:,1) & ana.id(:,1) <= cur.per(2) ;
	    sim1.I = sim1.id(:,2) == m & fut.per(1) <= sim1.id(:,1) & sim1.id(:,1) <= fut.per(2) ;
	    box.mm(m,1) = nanmean(obs.x(obs.I,j)) ;
	    box.mm(m,2) = nanmean(ana.x(ana.I,j)) ;
	    sim2.I = sim2.id(:,2) == m & cur.per(1) <= sim2.id(:,1) & sim2.id(:,1) <= cur.per(2) ;
	    box.mm(m,3) = nanmean(sim2.x(sim2.I,j)) ;
	    box.mm(m,4) = nanmean(sim1.x(sim1.I,j)) ;
	 endfor
	 hg = bar(box.mm) ;
	 colormap(cmap)
	 set(gca,'xticklabel', mth) 
	 legend(hg, LEG, "location", legpos) ;
	 legend("boxoff") ;
	 legend("right") ;
	 xlabel("month") ;
	 vs = strrep(v, "TN", "T_n") ;
	 vs = strrep(vs, "Tn", "T_n") ;
	 vs = strrep(vs, "TX", "T_x") ;
	 vs = strrep(vs, "Tx", "T_x") ;
	 ylabel(sprintf("%s  %s", vs, vu)) ;
	 title([vs ", " wst]) ;
	 printf("plt: --> %s\n", mfile) ;
	 print(mfile, "-r600", "-portrait") ; pause(5) ;
	 printf("plt: --> %s\n", mplot=strrep(mfile, ["." sfx], ".og")) ;
	 hgsave(mplot) ;
	 clf ;

	 dfile = sprintf("%s/%s.%s.d.%s", pdir, v, wst, sfx) ;
	 plt_dly(gcf, obs, ana, j, v, vn, lw, cmap) ;
	 printf("plt: --> %s\n", dfile) ;
	 print(dfile, "-r600", "-portrait") ; pause(5) ;
	 printf("plt: --> %s\n", dplot=strrep(dfile, ["." sfx], ".og")) ;
	 hgsave(dplot) ;
	 ##pause(10) ;
	 clf ;

	 if Lavg
	    break ;
	 endif
	 
      endfor

   endfor
   
   return ;

   if !isfield(TP.obs, "T") || !isfield(TP.obs, "P"), continue ; endif

   ## T-P kernel regression
   nb = 10 ;
   [obs.T obs.Pf obs.Pi] = TP_kreg(TP.obs, nb) ;
   [ana.T ana.Pf ana.Pi] = TP_kreg(TP.ana, nb) ;
   [sim2.T sim2.Pf sim2.Pi] = TP_kreg(TP.sim2, nb) ;
   [sim1.T sim1.Pf sim1.Pi] = TP_kreg(TP.sim1, nb) ;
   if wLptr
      [p_ana.T p_ana.Pf p_ana.Pi] = TP_kreg(TP.p_ana, nb) ;
      [cur.T cur.Pf cur.Pi] = TP_kreg(TP.cur, nb) ;
      [fut.T fut.Pf fut.Pi] = TP_kreg(TP.fut, nb) ;
   endif

   for j=1:columns(obs.Pf)

      st = num2str(obs.ids(j){:}) ; 
      st = strrep(st, " ", "_") ; wst = strrep(st, "_", "\\_") ;

      TPfile = sprintf("%s/%s.%s.TP.%s", pdir, v, st, sfx) ;
      set(fig, "visible", vis) ;

      subaxis(2, 1, 1) ;
      line(obs.T{j}, obs.Pf{j}, "linewidth", lw(1), "color", cmap(1,:)) ;
      line(ana.T{j}, ana.Pf{j}, "linewidth", lw(2), "color", cmap(2,:)) ;
      line(sim2.T{j}, sim2.Pf{j}, "linewidth", lw(3), "color", cmap(3,:)) ;
      line(sim1.T{j}, sim1.Pf{j}, "linewidth", lw(4), "color", cmap(4,:)) ;
      title([v ", " wst]) ;
      if wLptr
	 line(p_ana.T{j}, p_ana.Pf{j}, "linewidth", lw(2)/2, "color", cmap(2,:), "linestyle", "--") ;
	 line(cur.T{j}, cur.Pf{j}, "linewidth", lw(2)/2, "color", cmap(3,:), "linestyle", "--") ;
	 line(fut.T{j}, fut.Pf{j}, "linewidth", lw(2)/2, "color", cmap(4,:), "linestyle", "--") ;
	 [hleg, hleg_obj, hplot, labels] = legend(LEG, "location", legpos) ;
	 legend("boxoff") ;
	 legend("right") ;
      else
	 legend(LEG, "location", legpos) ;
	 legend("boxoff") ;
	 legend("right") ;
      endif
      ylabel(["Pf"]) ;

      subaxis(2, 1, 2) ;
      line(obs.T{j}, obs.Pi{j}, "linewidth", lw(1), "color", cmap(1,:)) ;
      line(ana.T{j}, ana.Pi{j}, "linewidth", lw(2), "color", cmap(2,:)) ;
      line(sim2.T{j}, sim2.Pi{j}, "linewidth", lw(3), "color", cmap(3,:)) ;
      line(sim1.T{j}, sim1.Pi{j}, "linewidth", lw(4), "color", cmap(4,:)) ;
      if wLptr
	 line(p_ana.T{j}, p_ana.Pi{j}, "linewidth", lw(2)/2, "color", cmap(2,:), "linestyle", "--") ;
	 line(cur.T{j}, cur.Pi{j}, "linewidth", lw(2)/2, "color", cmap(3,:), "linestyle", "--") ;
	 line(fut.T{j}, fut.Pi{j}, "linewidth", lw(2)/2, "color", cmap(4,:), "linestyle", "--") ;
	 [hleg, hleg_obj, hplot, labels] = legend(LEG, "location", legpos) ;
	 legend("boxoff") ;
	 legend("right") ;
      else
	 legend(LEG, "location", legpos) ;
	 legend("boxoff") ;
	 legend("right") ;
      endif
      xlabel("T [{\\deg}C]") ;
      ylabel(["Pi [mm/d]"]) ;
      ## orient landscape
      ## set(gca, "xgrid", "on") ;
      set(gcf, "paperposition", [0.05 0.05 0.95 0.95])
      printf("plt: --> %s\n", TPfile) ;
      print(TPfile, "-r600") ; pause(5) ;
      clf ;

   endfor

endfunction


function [y rv] = ptr_loc (ptr, vn, lon, lat)
 
   ## usage: [y rv] = ptr_loc (ptr, vn, lon, lat)
   ##
   ## nearest gridpoint for vn
   ## UGLY

   y = [] ;
   vn = tolower(vn) ;
   if !isempty(strfind(vn, "temp")), vn = "temp" ; endif
   if !isempty(strfind(vn, "precip")), vn = "precip" ; endif

   for [v k] = ptr
      name = tolower(v.name) ;
      if !isempty(strfind(name, "temp")), name = "temp" ; endif
      if !isempty(strfind(name, "wind")), name = "wind" ; endif
      if !isempty(strfind(name, "sun")), name = "sun" ; endif
      if !isempty(strfind(name, "precip")), name = "precip" ; endif
      if strcmp(name, vn)
	 clear y ;
	 y.name = name ;
	 break ;
      endif
   endfor

   if !(rv = isfield(y, "name"))
      return ;
   endif

   if isfield(v, "lon")
      xlon = v.lon ;
   else
      xlon = lon ;
   endif
   if isfield(v, "lat")
      xlat = v.lat ;
   else
      xlat = lat ;
   endif

   if isvector(xlon) && isvector(xlat)
      [xlon xlat] = meshgrid(xlon, xlat) ;
   endif

   y.id = v.id ; y.name = k ;
   y.K = findgp([lon(:)' ; lat(:)'], [xlon(:)' ; xlat(:)']) ;
   y.lon = xlon(:)(y.K) ; y.lat = xlat(:)(y.K) ; y.x = v.x(:,y.K) ;

endfunction


function plt_dly (h, obs, ana, j, v, vn, lw, cmap)

   ## usage: plt_dly (h, obs, ana, j, v, vn, lw, cmap)
   ## 
   ## plot daily values

   global BATCH PAR Lavg per

   vis = get(h, "Visible") ;
   figure(h, "Visible", vis) ;

   if Lavg
      st = "areal mean" ;
   else
      st = num2str(obs.ids{j}) ;
      st = strrep(st, " ", "_") ;
   endif
   wst = strrep(st, "_", "\\_") ;
   wst = strrep(wst, " ", "-") ;

   vu = punit(v) ;

   obs.I = find(sdate(obs.id, per)) ;
   ana.I = find(sdate(ana.id, per)) ;
   if sum(obs.I & ana.I) < 300
      obs.I = find(sdate(obs.id, PAR.cper)) ;
      ana.I = find(sdate(ana.id, PAR.cper)) ;
      y0 = ceil(nanmean(obs.id(obs.I,:)([1 end],1))) ;
      obs.I = find(sdate(obs.id, y0)) ;
      ana.I = find(sdate(ana.id, y0)) ;
   endif
   
   dfun = @dateax ;

   oI = obs.I(1:floor(length(obs.I)/2)) ; aI = ana.I(1:floor(length(ana.I)/2)) ;
   line(dfun(obs.id(oI,:)), obs.x(oI,j), "color", cmap(1,:), "linewidth", lw(1)) ;
   ## line(dfun(ana.id(aI,:)), ana.x(aI,j), "color", cmap(2,:), "linewidth", lw(2)) ;
   oI = obs.I(ceil(length(obs.I)/2):end) ; aI = ana.I(ceil(length(ana.I)/2):end) ;
   line(dfun(obs.id(oI,:)), obs.x(oI,j), "color", cmap(1,:), "linewidth", lw(1)) ;
   ## line(dfun(ana.id(aI,:)), ana.x(aI,j), "color", cmap(2,:), "linewidth", lw(2)) ;
   ax = axis ; clf ; set(gcf, "Visible", vis) ;

   ##subaxis(2, 1, 1) ;
   subplot(2, 1, 1) ;
   oI = obs.I(1:floor(length(obs.I)/2)) ; aI = ana.I(1:floor(length(ana.I)/2)) ;
   if 0 & any(~isnan(obs.x(oI,j))) axis(ax) ; endif
   line(dfun(obs.id(oI,:)), obs.x(oI,j), "color", cmap(1,:), "linewidth", lw(1)) ;
   line(dfun(ana.id(aI,:)), ana.x(aI,j), "color", cmap(2,:), "linewidth", lw(2)) ;
   datetick("mmm yyyy") ;
   ANA = sprintf("%s/XDS", toupper(strsplit(PAR.ana, "/"){end})) ;
   legend({"OBS" ANA}, "location", "southoutside") ;
   legend("boxoff") ; legend("right") ;
   title([vn ", " wst]) ;
   set(gca, "ygrid", "on", "gridalpha", 0.6) ;
   vs = strrep(v, "TN", "T_n") ;
   vs = strrep(vs, "Tn", "T_n") ;
   vs = strrep(vs, "TX", "T_x") ;
   vs = strrep(vs, "Tx", "T_x") ;
   ylabel(sprintf("%s  %s", vs, vu)) ;

   ##subaxis(2, 1, 2) ;
   subplot(2, 1, 2) ;
   oI = obs.I(ceil(length(obs.I)/2):end) ; aI = ana.I(ceil(length(ana.I)/2):end) ;
   if 0 & any(~isnan(obs.x(oI,j))) axis(ax) ; endif
   line(dfun(obs.id(oI,:)), obs.x(oI,j), "color", cmap(1,:), "linewidth", lw(1)) ;
   line(dfun(ana.id(aI,:)), ana.x(aI,j), "color", cmap(2,:), "linewidth", lw(2)) ;
   datetick("mmm yyyy") ;
##   legend({"OBS" ANA}, "location", "northwest") ;
##   legend("boxoff") ; legend("right") ;
   set(gca, "ygrid", "on", "gridalpha", 0.6) ;
   vs = strrep(v, "TN", "T_n") ;
   vs = strrep(vs, "Tn", "T_n") ;
   vs = strrep(vs, "TX", "T_x") ;
   vs = strrep(vs, "Tx", "T_x") ;
   ylabel(sprintf("%s  %s", vs, vu)) ;

endfunction


function vu = punit (v)

   ## usage: vu = punit (v)
   ## 
   ## physical units

   switch v
      case {"T" "TN" "Tn" "TX" "Tx" "Tavg" "TAVG" "Tmean" "TMEAN"}
	 vu = " [{\\deg}C]" ;
      case {"P" "pr"}
	 vu = "[mm/d]" ;
      otherwise
	 vu = "" ;
   endswitch

endfunction


function [Tq Pf Pi] = TP_kreg (s, nb)

   ## usage: [Tq Pf Pi] = TP_kreg (s, nb)
   ## 
   ## T-P kernel regression

   binn = "T" ;

   [IT IP] = common(s.T.id, s.P.id) ;
   s.T.id = s.T.id(IT,:) ; s.T.x = s.T.x(IT,:) ;
   s.P.id = s.P.id(IP,:) ; s.P.x = s.P.x(IP,:) ;

   ## I = ssn(s.T.id, "cold") ;
   ## I = ssn(s.T.id, "warm") ;
   I = ssn(s.T.id, "all") ;

   for j = 1:columns(s.P.ll)

      s.P.T(j) = findll(s.P.ll(:,j), s.T.ll) ;
      X.id = s.P.id(I,:) ;
      X.x = [s.T.x(I,s.P.T(j)) s.P.x(I,j)] ;

      if 0
	 scatter(X.x(:,1), X.x(:,2), 5, "black", "filled") ;
	 xlabel("T [{\\deg}C]") ; ylabel(["P [mm]"])
	 orient landscape
	 set(gca, "xgrid", "on") ;
	 set(gcf, "paperposition", [0.05 0.05 0.95 0.95])
	 print(sprintf("%s/T-P_scatter.%s", cloud, sfx)) ;
      endif

      ## pre-select data
      II = all(!isnan(X.x), 2) ;
      x = X.x(II,:) ;
      Iw = x(:,2) > 0 ;	# wet days

      ## quantiles
      w = quantile(x(:,1), linspace(0, 1, nb+1))' ;
      Tq{j} = nanmean([w(1:end-1)', w(2:end)'], 2) ;
      ## Tq{j} = linspace(min(x(:,1)), max(x(:,1)), nb+1)' ;

      ## frequencies
      IT = arrayfun(@(u,v) u < x(:,1) & x(:,1) < v, w(1:end-1), w(2:end), "UniformOutput", false) ;
      Pf{j} = cell2mat(cellfun(@(i) sum(Iw&i)/sum(i), IT, "UniformOutput", false))' ;

      ## intensity (kernel regression)
      n = rows(x) ;
      Pi{j} = kernel_regression(Tq{j}, x(Iw,2), x(Iw,1), n^(-1/5), "kernel_normal", true, false) ;
      ## Pi{j} = qntl(x(Iw,:), binn, 0.5, nb+1) ;

   endfor

endfunction
