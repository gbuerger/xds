## usage: nc2csv (ncdir)
##
## read and write (csv) netcdf files
function nc2csv (ncdir)

   pkg load netcdf ;

   G = dlmread("~/owncloud/SHIVA/Mahanadi.txt", ",")(:,1:2) ;
   G0 = mean(G) ; Ge = G0 + (G - G0) ;

   t0 = datenum(1900,1,1) ;

   for ncf = glob([ncdir "/*/*.nc"])'

      ncf = ncf{:} ;
      
      id = datevec(t0 + double(ncread(ncf, "time"))/24)(:,1:3) ;

      lon = double(ncread(ncf, "longitude")) ;
      lat = double(ncread(ncf, "latitude")) ;
      [LAT LON] = meshgrid(lat, lon) ;
      [IN ON] = inpolygon(LON(:), LAT(:), Ge(:,1), Ge(:,2)) ;
      IN = (IN | ON) ;
      ids = strsplit(sprintf("%.2f_%.2f\n", [LON(IN) LAT(IN)]'), "\n")(1:end-1) ;
      
      nc = ncinfo(ncf) ;
      for nv = [nc.Variables]

	 if length([nv.Dimensions.Length]) < 3, continue ; endif

	 x = ncread(ncf, nv.Name) ;
	 idx.type = "()" ;
	 idx.subs = repmat({":"}, 1, ndims(x)) ;
	 if strcmp(nv.Dimensions(3).Name, "level")
	    idx.subs{3} = size(x,3) ;
	    x = squeeze(subsref(x, idx)) ;
	 endif
	 N = num2cell(size(x)) ;
	 V = nv.Name ;
	 VN = keyval(nv.Attributes, "long_name") ;
	 x = reshape(x, N{1}*N{2}, N{3:end}) ;
	 idx.subs = repmat({":"}, 1, ndims(x)) ;
	 idx.subs{1} = find(IN) ;
	 x = subsref(x, idx) ;
	 p = num2cell(1:ndims(x)) ;
	 x = permute(x, [p{end}, p{1:end-1}]) ;
	 
	 ffmt = ",%.1f" ;
	 x(abs(x) < eps) = 0 ;
	 if regexpi(VN, "temperature")
	    x = x - 273.15 ;
	 endif
	 if regexpi(VN, "precip")
	    x = 1000 * x ;
	 endif
	 if regexpi(VN, "humid")
	    ffmt = ",%.3g" ;
	 endif

	 eval([nv.Name ".id = id ;"]) ;
	 eval([nv.Name ".ids = ids ;"]) ;
	 eval([nv.Name ".x = x ;"]) ;

	 N = size(x) ;
	 if length(N) < 3
	    fid = fopen(ofile = strrep(ncf, ".nc", ["." V ".csv"]), "wt") ;
	    fmt = ["Y,M,D" repmat(",%s", 1, N(2)) "\n"] ;
	    eval(["fprintf(fid, fmt, " V ".ids{:}) ;"]) ;
	    fmt = ["%d,%d,%d" repmat(ffmt, 1, N(2)) "\n"] ;
	    eval(["fprintf(fid, fmt, [" V ".id nan2lim(" V ".x)]') ;"]) ;
	    fclose(fid) ;
	 else
	    for j=1:N(3)
	       ofile = strrep(ncf, ".nc", ["." V ".csv"]) ;
	       [f1 f2 f3] = fileparts(ofile) ;
	       pmkdir(odir = fullfile(f1, sprintf("%02d", j))) ;
	       ofile = fullfile(odir, [f2 f3]) ;
	       fid = fopen(ofile, "wt") ;
	       fmt = ["Y,M,D" repmat(",%s", 1, N(2)) "\n"] ;
	       eval(["fprintf(fid, fmt, " V ".ids{:}) ;"]) ;
	       fmt = ["%d,%d,%d" repmat(ffmt, 1, N(2)) "\n"] ;
	       eval(["fprintf(fid, fmt, [" V ".id nan2lim(" V ".x(:,:,j))]') ;"]) ;
	       fclose(fid) ;
	    endfor
	 endif

      endfor
      
   endfor

endfunction
