## usage: Y = evinv (X, mu, sigma)
##
## Gumbel inverse cdf
function Y = evinv (X, mu, sigma)
   Y = gevinv(X, 0, mu, sigma) ;
endfunction
