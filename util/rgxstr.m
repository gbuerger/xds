## usage: l = rgxstr (s, PAT)
##
## check whether s contains any of the regex's PAT
function l = rgxstr (s, PAT)
   l = any(cellfun(@(pat) ~isempty(regexp(s, pat)), PAT)) ;
endfunction
