## usage: to_xds (pdd, odir)
##
## output to xds
function to_xds (pdd, odir)

   global lim
   
   write_stat(odir, pdd) ;

   C(1,:) = {"id" "var" "varname" "lon" "lat" "alt" "undef"} ;
   for j = 1:columns(pdd.x)
      C(1+j,:) = {pdd.ids{j}, pdd.vars{j}, pdd.names{j}, pdd.lon(j), pdd.lat(j), pdd.alt(j), lim} ;
   endfor

   printf("--> %s\n", [odir, "/meta.txt"]) ;
   cell2csv([odir, "/meta.txt"], C) ;

endfunction
