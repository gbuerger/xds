## usage: bias = fbias (f, s, ref)
##
## calcualte bias between s and ref for field f
function bias = fbias (f, s, ref)

   pkg load statistics
   global l ad pz bw bcfun m D S
   
   if ismember(f, {"id" "ad" "h"})
      bias = NaN ;
      return ;
   endif

   E = ref.(f) ;
   X = s.(f) ;

   jt = ndims(E) ;
   I = repmat({":"}, 1, jt) ;
   I{jt} = ad ;
   E = E(I{:}) ;
   X = X(I{:}) ;
   
   if strcmp(l, "sfc")
      
      if LE = (sum(diff(E, 1, jt)(:) < 0) / sum(~isnan(E(:))) > 0.1)
	 ## precip cumulative
	 E = cumsum(E, jt) ;
      endif
      if LX = (sum(diff(X, 1, jt)(:) < 0) / sum(~isnan(X(:))) > 0.1)
	 ## precip cumulative
	 X = cumsum(X, jt) ;
      endif
      E = diff(E, 1, jt) ;
      X = diff(X, 1, jt) ;

      ## fixate E ~ 0
      qz = quantile(E(:), pz) ;
      E(E <= qz) = qz ;
      X(E <= qz) = qz ;

      N = size(E) ;
      for j = 1:jt-1
	 E = nanmean(E, j) ;
	 X = nanmean(X, j) ;
      endfor
      E = repmat(E, N(1:jt-1)) ;
      X = repmat(X, N(1:jt-1)) ;

   endif

   Es = ksmooth(E, jt, bw.E) ;
   Xs = ksmooth(X, jt, bw.X) ;

   bias = bcfun(Xs, Es) ;

   ## plots
   fn = datestr([1, m, 1, 0, 0, 0], 3) ;
   pfile = sprintf("%s/%s/cf.%s.%s.svg", D, S, f, fn) ;
   if exist(pfile, "file") ~= 2

      lat = ncread(ifile, "latitude") ;
      lon = ncread(ifile, "longitude") ;
      figure(1, "position", [0 0 0.3 0.5], "paperpositionmode", "auto")
      clf
      subplot(2,1,1) ; hold on ;
      col = rainbow(length(lat)) ;
      set(gca, "ColorOrder", col)
      if isempty(i = find(ad(:,1) == 365))
	 wad = ad ; we = bias ;
      else
	 wad = [ad(1:i,:) ; nan(1,columns(ad)) ; ad(i+1:end,:)] ;
	 if strcmp(l, "sfc")
	    we = cat(jt, bias(:,:,:,1:i), nan([size(bias)(1:jt-1) 1]), bias(:,:,:,i+1:end)) ;
	 else
	    we = cat(jt, bias(:,:,:,:,1:i), nan([size(bias)(1:jt-1) 1]), bias(:,:,:,:,i+1:end)) ;
	 endif
      endif
      plot(wad(:,1), squeeze(mean(mean(we, 1), 3))', "linewidth", 1)
      plot(wad(:,1), squeeze(mean(mean(mean(we, 1), 2), 3)), "k", "linewidth", 2)
      xlabel("lead  [d]") ;
      datetick(3) ;
      ##	       ylim([0 2]) ;
      if strcmp(l, "sfc")
	 ttl = "correction factor" ;
      else
	 ttl = "correction" ;
      endif
      title(sprintf("%s %s, latitudes", f, ttl)) ;
      hl = legend(cellstr(num2str(lat)), "location", "northeast") ;
      set(hl, "color", "none", "box", "off")
      subplot(2,1,2) ; hold on ;
      col = rainbow(length(lon)) ;
      set(gca, "ColorOrder", col)
      plot(wad(:,1), squeeze(mean(mean(we, 2), 3))', "linewidth", 1)
      plot(wad(:,1), squeeze(mean(mean(mean(we, 1), 2), 3)), "k", "linewidth", 2)
      xlabel("lead  [d]") ;
      datetick(3) ;
      ##	       ylim([0 2]) ;
      title(sprintf("%s %s, longitudes", f, ttl)) ;
      hl = legend(cellstr(num2str(lon-360)), "location", "northeast") ;
      set(hl, "color", "none", "box", "off")
      printf("%s/%s/cf.%s.%s.svg\n", D, S, f, fn) ;
      print(pfile) ;

   endif

endfunction
