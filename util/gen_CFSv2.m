## usage: gen_CFSv2 (lfile)
##
## generate ptr list for CFSv2
function gen_CFSv2 (lfile)

   addpath ~/oct
   
   pfx = "http://nomads.ncdc.noaa.gov/thredds/" ;
   cnd = "(kv = keyval(v, \"name\")) && rgxstr(kv, {\"q700\" \"q850\" \"q925\" \"t700\" \"t850\"  \"t1000\" \"wnd700\" \"wnd925\" \"wnd1000\" \"z700\" \"z850\" \"z1000\" \"prate\"})" ;

   F = {} ;
   fid = fopen(lfile, "wt") ;
   for y = 1982:2009
      for m = 1:12

	 ctlg = sprintf("%scatalog/modeldata/cmd_ts_9mon/%4d/%4d%02d/catalog.xml", pfx, y, y, m) ;

	 s = parseXML(ctlg) ;
	 F = rkeyval(s, "urlPath", cnd) ;
	 F = cellfun(@(c) [pfx "dodsC/" c], F, "UniformOutput", false) ;
	 cellfun(@(f) fdisp(fid, f), F) ;

	 ctlg = sprintf("%scatalog/cfsr-rfl-ts9/prate/%4d%02d/catalog.xml", pfx, y, m) ;
	 s = parseXML(ctlg) ;
	 F = rkeyval(s, "urlPath", cnd) ;
	 F = cellfun(@(c) [pfx "dodsC/" c], F, "UniformOutput", false) ;
	 cellfun(@(f) fdisp(fid, f), F) ;

	 fflush(fid) ;

      endfor
   endfor

   fclose(fid) ;
   
endfunction
