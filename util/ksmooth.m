## usage: xs = ksmooth (x, jt, varargin)
##
## kernel smoother for x relative to time dim jt
function xs = ksmooth (x, jt, varargin)

   pkg load econometrics statistics

   if nargin < 2, jt = 1 ; endif
   Nx = size(x) ;
   nd = length(Nx) ;

   CNx = arrayfun(@(j) ones(j,1), Nx, "UniformOutput", false) ;
   CNx{jt} = Nx(jt) ;
   C = mat2cell(x, CNx{:}) ;
   x = parfun(@(c) kernel_regression((1:Nx(jt))', squeeze(c), (1:Nx(jt))', varargin{:}), C, "UniformOutput", false) ;

   x = cell2mat(x) ;
   xs = reshape(x, [Nx(jt) Nx(1:jt-1) Nx(jt+1:end)]) ;
   if jt > 1
      xs = permute(xs, [2:jt-1 jt:nd 1]) ; # OMG!!
   endif
endfunction
