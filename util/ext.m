function varargout = ext (x)

## usage:  ext(x)
## display extremal values

   N = size(x) ;

   xmin = min(x(:)) ;
   xmax = max(x(:)) ;
   imin = find(x == xmin)(1) ;
   imax = find(x == xmax)(1) ;

   d0 = d = length(N) ;
   while (d > 0)
      Imin(d) = ceil(imin/prod(N(1:(d-1)))) ;
      imin = imin - (Imin(d)-1) * prod(N(1:(d-1))) ;
      Imax(d) = ceil(imax/prod(N(1:(d-1)))) ;
      imax = imax - (Imax(d)-1) * prod(N(1:(d-1))) ;
      d-- ;
   endwhile

   if nargout < 1

      fmt = [repmat("%10d", 1, d0) ": %12.4g\n"] ;
      printf(fmt, Imin, xmin) ;
      printf(fmt, Imax, xmax) ;

      In = num2cell(Imin) ; In{1} = ":" ; 
      Ix = num2cell(Imax) ; Ix{1} = ":" ;

      subplot(2, 1, 1) ;
      plot(x(Ix{:})) ;
      title("max") ;
      subplot(2, 1, 2) ;
      plot(x(In{:})) ;
      title("min") ;

   else

      varargout = num2cell([Imin Imax xmin xmax](1:nargout)) ;
      
   endif
   
endfunction
