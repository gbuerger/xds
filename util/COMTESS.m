addpath ~/xds/fun
A = argv' ;

xds = [] ; n = 0 ;
for d = A

   [S, E, TE, M, T, NM, SP] = regexp(d{:}, "[\\w-_]") ;
   d = char(M)' ;
   F = glob(fullfile(d, [v "*.csv"])) ;

   for f = F

      n++ ;
      u = read_stat(f{:}) ;
      xds = join(xds, u) ;

   endfor
endfor

if !isfield(xds, "ids"), exit ; endif

fid = fopen(ofile = [v ".csv"], "wt") ;
fmt = ["Y,M,D" repmat(",%s", 1, n) "\n"] ;
fprintf(fid, fmt, xds.ids{:}) ;
fmt = ["%d,%d,%d" repmat(",%3.1f", 1, n) "\n"] ;
fprintf(fid, fmt, [xds.id(:,1:3) xds.x]') ;
fclose(fid) ;

# csvwrite(ofile = [opfx v ".csv"], [xds.id(:,1:3) xds.x(:,J)], "precision", PAR.precision) ;
printf("out: --> %s\n", ofile) ;
