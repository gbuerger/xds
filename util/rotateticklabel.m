%% Copyright (c) 2005, Andrew Bliss
%% All rights reserved.
%% 
%% Redistribution and use in source and binary forms, with or without
%% modification, are permitted provided that the following conditions are met:
%% 
%% * Redistributions of source code must retain the above copyright notice, this
%%   list of conditions and the following disclaimer.
%% 
%% * Redistributions in binary form must reproduce the above copyright notice,
%%   this list of conditions and the following disclaimer in the documentation
%%   and/or other materials provided with the distribution
%% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
%% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
%% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
%% DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
%% FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
%% DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
%% SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
%% CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
%% OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
%% OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
function th=rotateticklabel(h,rot,demo)

%ROTATETICKLABEL rotates tick labels
%   TH=ROTATETICKLABEL(H,ROT) is the calling form where H is a handle to
%   the axis that contains the XTickLabels that are to be rotated. ROT is
%   an optional parameter that specifies the angle of rotation. The default
%   angle is 90. TH is a handle to the text objects created. For long
%   strings such as those produced by datetick, you may have to adjust the
%   position of the axes so the labels don't get cut off.
%
%   Of course, GCA can be substituted for H if desired.
%
%   TH=ROTATETICKLABEL([],[],'demo') shows a demo figure.
%
%   Known deficiencies: if tick labels are raised to a power, the power
%   will be lost after rotation.
%
%   See also datetick.

%   Written Oct 14, 2005 by Andy Bliss
%   Copyright 2005 by Andy Bliss

%DEMO:
   if nargin==3
      x=[now-.7 now-.3 now];
      y=[20 35 15];
      figure
      plot(x,y,'.-')
      datetick('x',0,'keepticks')
      h=gca;
      set(h,'position',[0.13 0.35 0.775 0.55])
      rot=90;
   endif

%set the default rotation if user doesn't specify
   if nargin==1
      rot=90;
   endif

%make sure the rotation is in the range 0:360 (brute force method)
   while rot>360
      rot=rot-360;
   endwhile
   while rot<0
      rot=rot+360;
   endwhile

%get current tick labels
   a=get(h,'XTickLabel');
   fs = get(h,'fontsize'); fn = get(h,'fontname');

%erase current tick labels from figure
   set(h,'XTickLabel',[]);

%get tick label positions
   b=get(h,'xtick');
   c=get(h,'ylim');

   if strcmp(get(h, "ydir"), "normal")
      if strcmp(get(h, "xaxislocation"), "bottom")
	 j = 1 ; d = 1 ; algn = 'right' ;
      else
	 j = 2 ; d = -1 ; algn = 'left' ;
      endif
   else
      if strcmp(get(h, "xaxislocation"), "bottom")
	 j = 2 ; d = -1 ; algn = 'right' ;
      else
	 j = 1 ; d = 1 ; algn = 'left' ;
      endif
   endif

%make new tick labels
   if rot<180
      th=text(b,repmat(c(j) - d*0.02*(c(2)-c(1)),length(b),1),a,'fontname',fn,'fontsize',fs,'HorizontalAlignment',algn,'rotation',rot);
   else
      th=text(b,repmat(c(j) - d*0.10*(c(2)-c(1)),length(b),1),a,'fontname',fn,'fontsize',fs,'HorizontalAlignment',algn,'rotation',rot);
   endif

##   return ; # "normalized" buggy 
   set(th, "units", "normalized") ;
   pos0 = pos = get(h, "position") ;

   e = 0.5 * max(cell2mat(get(th, "extent"))(:,4)) ;
   pos(2) += e ;
   pos(4) -= e ;
   set(gca, "position", pos) ;

   # hlink = linkprop ([h; th], "fontsize");

endfunction
