## usage: res = zchk (z)
##
## check maximum absolute z-scores
function res = zchk (z)

   N = 1000 ;			# sample size
   
   n = rows(z) ;
   Z = randn(n, N) ;
   res = max(abs(zscore(z))) / mean(max(Z)) ;

endfunction
