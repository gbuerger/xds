## usage: ghcnd_metadata = ghcnd_meta (meta)
##
##
function ghcnd_metadata = ghcnd_meta (meta)

   n = stat(meta).size / 86 ;
   L = num2cell(1:n) ;
   C = cell(1,10) ;

   [C{:}] = parfun(@(l) rline(meta, l), L, "UniformOutput", false) ;

   ghcnd_metadata.ID = C{1} ;
   ghcnd_metadata.lat = cell2mat(C{2}') ;
   ghcnd_metadata.lon = cell2mat(C{3}') ;
   ghcnd_metadata.elevation = cell2mat(C{4}') ;
   ghcnd_metadata.state = C{5} ;
   ghcnd_metadata.name = C{6} ;
   ghcnd_metadata.gsnf = C{7} ;
   ghcnd_metadata.hcnf = C{8} ;
   ghcnd_metadata.wid = C{9} ;
   ghcnd_metadata.bline = C{10} ;
   
endfunction


## usage: [id, lat, lon, ele, state, name, gsnf, hcnf, wid, bline] = rline (fid, l)
##
##
function [id, lat, lon, ele, state, name, gsnf, hcnf, wid, bline] = rline (meta, l)
   
   fid=fopen(meta);

   nrows = 96640;
   IDq = 1:11;
   LATq = 13:20;
   LONq = 22:30;
   ELEVq = 31:37;
   STATEq = 39:40;
   NAMEq = 42:71;
   GSNFLAGq = 73:75;
   HCNFLAGq = 77:79;
   WMOIDq = 81:85;

   fseek(fid, (l-1)*86, SEEK_SET) ;

   aline = fgetl(fid);
   if ~ischar(aline)
      error("incorrect input") ;
   endif
   bline = char(aline);
   id = bline(IDq);
   lat = str2num(bline(LATq));
   lon = str2num(bline(LONq));
   ele = str2num(bline(ELEVq));
   state = bline(STATEq);
   name = deblank(bline(NAMEq));
   gsnf = bline(GSNFLAGq);
   hcnf = bline(HCNFLAGq);
   wid = bline(WMOIDq);

   fclose(fid);
   
endfunction
