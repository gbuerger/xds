## usage: [id h] = doy (d)
##
## day of year
function [id h] = doy (d)

   ## usage: res = f1 (a)
   ##
   ##
   function res = f1 (a)
      if isnan(a)
	 res = strptime(date, "%F %T") ;
	 res.yday = res.hour = NaN ;
      else
	 res = strptime(datestr(a, 31), "%F %T") ;
      endif
   endfunction


   ## usage: res = f2 (a)
   ##
   ##
   function res = f2 (a)
      if isnan(a)
	 res = strptime(date, "%F %T") ;
	 res.yday = res.hour = NaN ;
      else
	 res = strptime(datestr(datenum(a), 31), "%F %T") ;
      endif
   endfunction

   if columns(d) == 1
      w = parfun(@(a) [f1(a).yday + 1 f1(a).hour], d, "UniformOutput", false) ;
   else
      N = size(d) ;
      d = mat2cell(d, ones(N, 1), N(2)) ;
      w = parfun(@(a) [f2(a).yday + 1 f2(a).hour], d, "UniformOutput", false) ;
   endif

   N = size(w) ;
   w = cell2mat(w) ;
   w = reshape(w, [N(1), 2, N(2:end)]) ;
   I = repmat({":"}, 1, ndims(w)) ;
   I{2} = 1 ;
   id = squeeze(w(I{:})) ;
   I{2} = 2 ;
   h = squeeze(w(I{:})) ;
   return ;
   
   h = unique(w(:,2)) ;
   nh = length(h) ;
   
   id = nan(rows(w)/nh, nh) ;

   for j = 1:nh
      I = find(w(:,2) == h(j)) ;
      id(:,j) = w(I,1) ;
   endfor
   
endfunction
