## usage: [lst dlist] = list_CFSv2 ()
##
## produce listing of CFSv2 files
function [lst dlist] = list_CFSv2 ()

   fl = "https://www.ncei.noaa.gov/thredds/dodsC/cfs_refor_fl_6h_9m" ;
   hp = "https://www.ncei.noaa.gov/thredds/dodsC/cfs_refor_hp_ts_9m" ;

   V_fl = {"prate"} ;
   V_hp = {"t700" "t850" "wnd700" "wnd925" "q700" "q850" "z700" "z850"} ;

   T = datenum(1981, 1, 1):5:datenum(1981, 12, 31) ;
   i = 0 ;
   for y = 1982:2010
      dlist{++i} = sprintf("%4d", y) ;
      lst{i} = {} ;
      for t = T
	 dv = datevec(t) ;
	 for hh = {"00" "06" "12" "18"}
	    for v = V_fl
	       ncf = sprintf("%s/%s/%04d%02d/%s.%04d%02d%02d%s.time.grb2", fl, v{:}, y, dv(2), v{:}, y, dv(2), dv(3), hh{:}) ;
	       lst{i} = [lst{i} ncf] ;
	    endfor
	    for v = V_hp
	       ncf = sprintf("%s/%04d/%04d%02d/%s.%04d%02d%02d%s.time.grb2", hp, y, y, dv(2), v{:}, y, dv(2), dv(3), hh{:}) ;
	       lst{i} = [lst{i} ncf] ;
	    endfor
	 endfor
      endfor
   endfor
   
endfunction
