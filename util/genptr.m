## usage: s = genptr ()
##
## generate ptr file list
function s = genptr (ofile)

   fid = fopen(ofile, "wt") ;
   
   pfx = "http://nomads.ncdc.noaa.gov/thredds/dodsC/modeldata/cfs_reforecast_6-hourly_9mon_flxf/2003/200305/20030511/flxf2003051118.01.2003051106.grb2" ;
   pfx = "http://nomads.ncdc.noaa.gov/thredds/dodsC/modeldata/cfs_reforecast_6-hourly_9mon_flxf/" ;

   T = datenum(1982, 1, 1) ;
   T1 = datenum(1982, 1, 31) ;

   while T <= T1

      [y m d] = datevec(T) ;
      f = sprintf("%s%4d/%4d%02d/%4d%02d%02d/flxf%4d%02d%02d00.01.%4d%02d%02d00.grb2", pfx, y, y, m, y, m, d, y, m, d, y, m, d) ;

      T++ ;
      
      [~, suc] = urlread([f ".das"]) ;
      if ~suc, continue ; endif

      for lt=0:180
	 for h=[0 6 12 18]

	    [y1 m1 d1 h1] = datevec(T+lt+h/24) ;
	    f = sprintf("%s%4d/%4d%02d/%4d%02d%02d/flxf%4d%02d%02d%02d.01.%4d%02d%02d00.grb2", pfx, y, y, m, y, m, d, y1, m1, d1, h1, y, m, d) ;

	    fdisp(fid, f) ;
	    disp([y m d]) ;

	 endfor
      endfor
      
   endwhile

   fclose(fid) ;
   
endfunction
