
hT = hgload("/home/gerd/xds/data/EGLV/ptr/MPI-ESM-MR/rcp45/r1i1p1/pdd/plt/T.Bochum.ts.og") ;
set(hT, "visible", "on") ;

hP = hgload("/home/gerd/xds/data/EGLV/ptr/MPI-ESM-MR/rcp45/r1i1p1/pdd/plt/P.8721.ts.og") ;
set(hP, "visible", "on", "position", get(hT, "position")) ;

set(findall("type", "axes"), "fontsize", 14) ;
set(findall("type", "text"), "fontsize", 14) ;

hTC = findall(hT, "type", "axes")
hPC = findall(hP, "type", "axes")
set(hTC(1), "visible", "off")
set(hTC(3), "visible", "off")
set(hPC(1), "visible", "off")
set(hPC(3), "visible", "off")

delete(hTC(4))
set(hTC(2), "position", get(hPC(4), "position"))
copyobj(hPC(2), hT)

set(gcf, "paperposition", [0 0 1 0.5]) ;
print ~/owncloud/EGLV/max.png ;
print ~/owncloud/EGLV/max.svg ;
