## usage: [ad h] = getI (t)
##
## get doy index from t
function [ad h] = getI (t)

   t0 = datenum(1900,1,1) ;
   td = t/24 + t0 ;
   
   [ad h] = doy(td) ;

   ym = datevec(td(1))(1:2) ;

   if is_leap_year(ym(1)) && ym(2) > 2
      if isempty(i = find(ad(:,1) == 366))
	 I = ad(:,1) < 366 ;
      else
	 I = 1:i ;
      endif
      ad(I,:) = ad(I,:) - 1 ;
   endif

endfunction
