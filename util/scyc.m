## usage: y = scyc (idx, x, fun=@nanmean, deps=1e-5)
##
## seasonal cycle (daily)
function y = scyc (idx, x, fun=@nanmean, deps=1e-5)

   pkg load statistics
   
   idx = idx(:,1:3) ;
   t = 365 * date2toy(idx) / (2*pi) ;

   I = parfun(@(d) abs(t - d) < deps, 1:365, "UniformOutput", false) ;

   f = @(i) fun(x(i)) ;
   y = parfun(@(i) f(i), I, "UniformOutput", false)'  ;
   y = cell2mat(y) ;
   
   if nargout > 0 return ; endif

   plot(y) ;
   datetick("mmm", "keeplimits") ;
   
endfunction
