function idy = pltid (idx)

## usage:  idy = pltid(idx)
## interpolate to continuous years

   [n] = columns(idx) ;
   idy = idx(:,1) + (idx(:,2)-1)/12 + (idx(:,3)-1)/365 ;

endfunction
