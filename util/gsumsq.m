## usage: y = gsumsq (x)
##
##
function y = gsumsq (x)
   I = ~isnan(x) ;
   y = sumsq(x(I)) ;
endfunction
