## usage: selctl (d)
##
## select random ensemble members for ctl
function selctl (d = "data/SaWaM/brazil/SEAS5.bc", Y = 1981:2017)

   pkg load netcdf
   addpath ~/xds/fun
   init_rand ;
   
   m0 = 4 ; ms = circshift((1:12)', m0) ;
   ys = Y(randperm(Y(end) - Y(1) + 1)) ;
   pmkdir(d1 = [d ".ctl"]) ;

   ifile=sprintf("%s/1981/pf.01.sfc.nc", d) ;
   nc = ncinfo(ifile) ;
   j = find(strcmp({nc.Dimensions.Name}, "number")) ;	    
   N = [nc.Dimensions.Length] ;
   n = N(j) ;

   X = [] ; hw = waitbar(0) ;
   idx.type = "()" ;
   for y = Y
      yy = ys(y - Y(1) + 1) ;
      em = randi(n) ;
      waitbar((y-Y(1)+1)/(Y(end)-Y(1)+1), hw) ;
      pmkdir(sprintf("%s/%4d", d1, y)) ;
      for l = {"pl" "sfc"}
	 l = l{:} ;
	 ww = [] ;
	 for m = 1:12

	    mi = mod(m - randi(6) + 1, 12) ; # random lead time
	    if mi == 0 mi = 12 ; endif

	    ifile=sprintf("%s/%4d/pf.%02d.%s.nc", d, yy, m, l) ;
	    printf("<-- %s\n", ifile) ;
	    unlink(ofile = sprintf("%s/%4d/pf.%02d.%s.nc", d1, y, m, l)) ;
##	    netcdf_create(ofile, "netcdf4") ;

	    nc = nc1 = ncinfo(ifile) ;
	    j = find(strcmp({nc.Dimensions.Name}, "number")) ;
	    
	    N = [nc.Dimensions.Length] ;
	    JD = [1:j-1 j+1:length(N)] ;
	    JV = [1:j-1 j+1:length(nc.Variables)] ;
	    n = N(j) ;

	    id = datevec(datenum(1900,1,1) + ncread(ifile, "time")/24) ;
	    idx.subs = repmat({":"}, 1, length(N)) ;
	    idx.subs{end} = I = (id(:,2) == m) ;
	    for i = 1:length(nc.Variables)
	       if all(nc.Variables(i).Size == N)
		  nc.Variables(i).Dimensions = nc.Variables(i).Dimensions(JD) ;
		  nc.Variables(i).Dimensions(end).Length = sum(I) ;
		  nc.Variables(i).Size = [nc.Variables(i).Size(JD(1:end-1)) sum(I)] ;
	       elseif strcmp(nc.Variables(i).Name, "time")
		  nc.Variables(i).Dimensions(end).Length = sum(I) ;
		  nc.Variables(i).Size = sum(I) ;
	       endif
	    endfor
	    nc.Variables = nc.Variables(JV) ;
	    nc.Dimensions = nc.Dimensions(JD) ;
	    nc.Dimensions(end).Length = sum(I) ;
	    nc.Filename = ofile ;
	    nc.Format = "netcdf4" ;
	    printf("--> %s\n", ofile) ;
	    ncwriteschema(ofile, nc) ;
	    for i = 1:length(nc1.Variables)
	       if i == j, continue ; endif
	       nv = nc1.Variables(i) ;
	       ncn = nv.Name ;
	       x = ncread(ifile, ncn) ;
	       if regexpi(keyval(nv.Attributes, "long_name"), "precipitation")
		  jt = find(strcmp({nv.Dimensions.Name}, "time")) ;
		  Nw = N ; Nw(jt) = 1 ;
		  x = cat(jt, diff(x, 1, jt), nan(Nw)) ;
	       endif
	       if all(nv.Size == N)
		  idx.subs{j} = em ;
		  x = squeeze(subsref(x, idx)) ;
##		  if isfield(X, ncn)
##		     X.(ncn).id = cat(1, X.(ncn).id, id) ;
##		     X.(ncn).x = cat(ndims(x), X.(ncn).x, x) ;
##		  else
##		     X.(ncn).id = id ;
##		     X.(ncn).x = x ;
##		  endif
	       elseif strcmp(nv.Name, "time")
		  x = x(I) ;
		  id = datevec(datenum(1900,1,1) + double(x)/24) ;
		  id(:,1) = y ;
		  idc = mat2cell(id, rows(id), ones(1, columns(id))) ;
		  x = 24 * (datenum(idc{:}) - datenum(1900,1,1)) ;
	       endif
	       ncwrite(ofile, ncn, x) ;
	    endfor
##	    if any(mean(mean(X.t.x(:,:,1,:), 1), 2) > 288)
##	       disp([y, m]) ;
##	       break
##	    endif
	 endfor
      endfor
   endfor
   close(hw) ;

endfunction
