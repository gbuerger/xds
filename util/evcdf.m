## usage: Y = evcdf (X, mu, sigma)
##
## Gumbel cdf
function Y = evcdf (X, mu, sigma)
   Y = gevcdf(X, 0, mu, sigma) ;
endfunction
