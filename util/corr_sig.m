function [sig, r, rcrit, df, pval] = corr_sig (x, y, alpha, rcrit0)

   ## usage:  [sig, r, rcrit, df, pval] = corr_sig (x, y, alpha, rcrit0)
   ##
   ## calculate significance of x-y correlations
   ## neff: Davis, 1976; Ebisuzaki, 1997
   ## global ESS: 0 simple; 1 Ebisuzaki; 2 Dawdy/Matalas
   ## 

   global ESS LAG SIDED ;
   if isempty(SIDED) SIDED = "one" ; endif

   if (nargin < 3), alpha = 0.01 ; endif
   if strcmp(SIDED, "two") alpha = alpha / 2 ; endif
   
   if (columns(x)*columns(y) > 1)
      if (nargin < 4)
	 for i=1:columns(x)
	    for j=1:columns(y)
	       [sig(i,j), r(i,j), rcrit(i,j), df(i,j)] = ...
	       corr_sig (x(:,i), y(:,j), alpha) ;
	    endfor
	 endfor
      else
	 for i=1:columns(x)
	    for j=1:columns(y)
	       [sig(i,j), r(i,j), rcrit(i,j), df(i,j)] = ...
	       corr_sig (x(:,i), y(:,j), alpha, rcrit0) ;
	    endfor
	 endfor
      endif
      return ;
   endif

   if (any(size(x) != size(y)))
      error("corr_sig: unequal size") ;
   endif 

   I = all(~isnan([x y]), 2) ;
   x = x(I,:) ; y = y(I,:) ;
   [n m] = size(x) ;

   sig = false(1, m) ; r = rcrit = df = pval = nan(1, m) ;
   
   if var(x) * var(y) < eps
      return ;
   endif
   
   r = corr(x, y) ;

   r = min(max(r, -1), 1) ; # octave bug
   
   if (nargin < 4)
      if (ESS == 1)
	 df = Neff(x, y, LAG) - 2 ;
      elseif (ESS == 2)
	 df = ess(x, y) - 2 ;
      elseif (isnan(ESS))
	 Navl = sum(!isnan(x) & !isnan(y)) ;
	 df = Navl + Inf ;
      else
	 Navl = sum(!isnan(x) & !isnan(y)) ;
	 df = Navl - 2 ;
      endif
      for j=1:m
	 if (df(j) < 1), continue, endif
	 t = r * sqrt(df(j) / (1-r(j)^2)) ;
	 pval(j) = 1 - tcdf(t, df(j)) ;
	 tcrit = tinv(1-alpha, df(j)) ;
##	 rcrit(j) = tcrit / sqrt(tcrit^2+df(j)) ;
	 rcrit(j) = cor_t(tcrit, df(j)) ;
      endfor 
   else
      Navl = sum(!isnan(x) & !isnan(y)) ;
      df = Navl - 2 ;
      if (df(1) < 1), return, endif
      rcrit = rcrit0 ;
      pval = 1 - tcdf(t, df) ;
   endif

   sig = abs(r) > rcrit ;

endfunction
