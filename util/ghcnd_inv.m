## usage: ghcnd_inventory = ghcnd_inv (invf)
##
## read GHCN inventory
function ghcnd_inventory = ghcnd_inv (invf)

   fid = fopen(invf, "rt") ;
   C = textscan (fid, "%11s %8.4f %9.4f %4s %4d %4d") ;
   fclose(fid) ;
   
   ghcnd_inventory.ID = C{1} ;
   ghcnd_inventory.lat = C{2} ;
   ghcnd_inventory.lon = C{3} ;
   ghcnd_inventory.var = C{4} ;
   ghcnd_inventory.yb = C{5} ;
   ghcnd_inventory.ye = C{6} ;

endfunction
