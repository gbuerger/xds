
## usage: [lst dlist] = list_CFSR ()
##
## produce listing of CFSR files
function [lst dlist] = list_CFSR ()

   V = {"t700" "t850" "wnd700" "wnd850" "z700" "z850" "q700" "q850" "prate"} ;

   lst = {} ; i = 0 ;

   sroot = "https://www.ncei.noaa.gov/thredds/dodsC/cfs_reanl_ts" ;

   for y = 1979:2010
      dlist{++i} = sprintf("%4d", y) ;
      for m = 1:12
	 for v = V
	    ncf = sprintf("%s/%04d%02d/%s.l.gdas.%04d%02d.grb2", sroot, y, m, v{:}, y, m) ;
	    lst = [lst ncf] ;
	 endfor
      endfor
   endfor
   for y = 2011:2011
      dlist{++i} = sprintf("%4d", y) ;
      for m = 1:3
	 for v = V
	    ncf = sprintf("%s/%04d%02d/%s.l.gdas.%04d%02d.grb2", sroot, y, m, v{:}, y, m) ;
	    lst = [lst ncf] ;
	 endfor
      endfor
   endfor

   sroot = "http://www.ncei.noaa.gov/thredds/dodsC/cfs_v2_anl_ts" ;

   for y = 2011:2011
      dlist{++i} = sprintf("%4d", y) ;
      for m = 4:12
	 for v = V
	    ncf = sprintf("%s/%04d%02d/%s.l.gdas.%04d%02d.grib2", sroot, y, m, v{:}, y, m) ;
	    lst = [lst ncf] ;
	 endfor
      endfor
   endfor
   for y = 2012:2016
      dlist{++i} = sprintf("%4d", y) ;
      for m = 1:12
	 for v = V
	    ncf = sprintf("%s/%04d%02d/%s.l.gdas.%04d%02d.grib2", sroot, y, m, v{:}, y, m) ;
	    lst = [lst ncf] ;
	 endfor
      endfor
   endfor
   
endfunction
