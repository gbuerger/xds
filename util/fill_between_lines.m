## usage: h = fill_between_lines (X, Y1, Y2, C, varargin)
##
##
function h = fill_between_lines (X, Y1, Y2, C, varargin)
   XX = [X(:) ; flipud(X(:))] ;
   Y = [Y1(:) ; flipud(Y2(:))] ;
   I = all(~isnan([XX Y]), 2) ;
   h = fill(XX(I), Y(I), C, varargin{:} ) ;
   hold on ;
   plot(X, Y2, "color", "k", "linewidth", 2) ;
endfunction
