function [Alpha,Beta,Location]=momentwbl(x)
   % x is vector of data and the out put of function are the moment estimations
   % Alpha is scale parameter, Beta is shape parameter and location is location parameter.
   ## adapted from
   ## http://www.mathworks.com/matlabcentral/fileexchange/38185-estimating-three-parameters-of-weibull-distribution

   beta1=0.001;
   beta2=200;
   n=size(x,2);
   ss=skewness(x); 
   ee1=(ss-(gamma(3/beta1+1)-3*gamma(2/beta1+1)*gamma(1/beta1+1)+2*gamma(1/beta1+1).^3)/((gamma(2/beta1+1)-gamma(1/beta1+1).^2).^(3/2)));
   ee2=(ss-(gamma(3/beta2+1)-3*gamma(2/beta2+1)*gamma(1/beta2+1)+2*gamma(1/beta2+1).^3)/((gamma(2/beta2+1)-gamma(1/beta2+1).^2).^(3/2)));
   % ee1*ee2 should be less than 0
   ee=ee1;
   m=1;
   while (abs(ee)>.01 & m<300)
      m=m+1;
      beta=(beta1+beta2)/2;
      ee=(ss-(gamma(3/beta+1)-3*gamma(2/beta+1)*gamma(1/beta+1)+2*gamma(1/beta+1).^3)/((gamma(2/beta+1)-gamma(1/beta+1).^2).^(3/2)));
      if ee*ee1<0
         beta2=beta;
      else
         beta1=beta;
      endif
      m=m+1;
   endwhile
   Beta=beta;
   Alpha=(((1/n)*(sum((x-mean(x)).^2))/(gamma(2/Beta+1)-gamma(1/Beta+1).^2)).^(1/2*Beta)).^(1/Beta);
   Location=mean(x)-Alpha*gamma(1/Beta+1);

endfunction
