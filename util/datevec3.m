function id = datevec3 (J, kY)

   ## usage: id = datevec3 (J, kY)
   ## 
   ## transform date yyyymmddHHMMSS to [y m d H M S]

   global NCPU

   J = J(:) ;

   if iscell(J)
      id = parcellfun(NCPU, @(j) datevec3(j), J, "UniformOutput", false, "VerboseLevel", false) ;
      id = cell2mat(id) ;
      return
   endif

   if nargin < 2, kY = 4 ; endif

   n = length(num2str(J)) ;
   id(1) = sscanf(J(1:kY), "%d") ;
   J = J(kY+1:end) ;
   n = length(J) ;

   j = 1 ;
   while n > 0
      j++ ;
      id(j) = sscanf(J(1:2), "%02d") ; J = J(3:end) ;
      n -= 2 ;
   endwhile

endfunction
