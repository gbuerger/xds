function plt (L)

   ## usage: plt (L)
   ## 
   ## plot results

   if (!L), return ; endif

   f = dbstack ; printf("\n\t###   %s   ###\n\n", f(1).name) ;

   global PAR PRJ GVAR ADIR SDIR CDIR BATCH Judf

   Lptr = true ; wLptr = false ;

   mth = {"J" "F" "M" "A" "M" "J" "J" "A" "S" "O" "N" "D"} ;
   P0 = [2*1000 86400 86400] ; T0 = 273.16 ; per = [2002 1 1 ; 2002 12 31] ;

   ldata = fullfile("data", PRJ, PAR.pdd) ;
   gdata = fullfile("data", PRJ, "ptr") ;
   pmkdir(pdir = fullfile(SDIR, PAR.pdd, "plt")) ;
   METAFILE = fullfile(ldata, "meta.txt") ;

   if strcmp(PAR.sim{:}), exit ; endif

   annarg = {} ; Sfun = {"mean" "max"} ;
   # annarg = {"f"} ; sfun = @(x,p) P_stat(x,p) ;

   ## plot parameters
   if BATCH
      graphics_toolkit gnuplot ; vis = "off" ;
      lw = 5*[2 1 1 1] ;
   else
      graphics_toolkit fltk ; vis = "on" ;
      lw = 1*[2 1 1 1] ;
      ## set(0, "defaultfigureposition", [0.4 0.3 0.6 0.7])
   endif
   pg = [2 2] ; q = 0.1 ; legpos = "northwest" ;
   cmap = [0 0 0; 0 0 1; rainbow(16)([10 2],:)] ;
   set(0, "defaultaxesfontname", "helvetica", "defaultaxesfontsize", 8, "defaultaxesfontweight", "bold") ;
   set(0, "defaulttextfontname", "helvetica", "defaulttextfontsize", 9, "defaulttextfontweight", "bold") ;

   pkg load miscellaneous statistics io
   
   [ids, vars, names, lons, lats, alts, undefs, J] = meta(METAFILE) ;
   VAR = sunique(vars) ; VARNAME = sunique(names) ;

global vars lons lats undefs J

[obs.T obs.P obs.X] = do_run("obs") ;
[ana.T ana.P ana.X] = do_run("ana", ADIR) ;
[sim2.T sim2.P sim2.X] = do_run("sim2", CDIR) ;
[sim1.T sim1.P sim1.X] = do_run("sim1", SDIR) ;

TPfile = fullfile(pdir, [v "." st ".TP.png"]) ;
TPfig = figure("visible", vis) ;

## obs.h = line(obs.X(:,1), obs.X(:,2), "linestyle", "none", "marker", ".", "markersize", 2, "markerfacecolor", "black", "markeredgecolor", "black") ;
line(obs.T, obs.P, "color", "black") ;
line(ana.T, ana.P, "color", "blue") ;
line(sim2.T, sim2.P, "color", "green") ;
line(sim1.T, sim1.P, "color", "red") ;

title([upper(PAR.pdd(1)) PAR.pdd(2:end) ", " v ", " wst]) ;
xlabel("T [C]") ; ylabel(["P [mm]"])
orient landscape
set(gca, "xgrid", "on") ;
set(gcf, "paperposition", [0.05 0.05 0.95 0.95])
print(fullfile(cloud, ["T-P_" sfx "_scatter.png"])) ;


function [Tq Pf x] = do_run (run, DIR)

   ## usage: [Tq Pf x] = do_run (run, DIR)
   ## 
   ## 

   global PAR PRJ
   global vars lons lats undefs J

   VAR = sunique(vars) ;

   ldata = fullfile("data", PRJ, PAR.pdd) ;

   for jv = 1:length(VAR)

      v = VAR{jv} ;
      if strcmp(run, "obs")
	 ifile = fullfile(ldata, [v ".csv"]) ;
      else
	 ifile = fullfile(DIR, PAR.pdd, [v ".csv"]) ;
      endif
      JJ = find(strcmp(vars, v)')(J{jv}) ;
      udf = str2num(char(undefs(JJ)))' ;

      s.(v) = read_stat(ifile) ;
      if strcmp(run, "obs")
	 s.(v).x = s.(v).x(:,J{jv}) ;
      endif
      s.(v).x = lim2nan(s.(v).x, udf) ;
      s.(v).ll = [lons(JJ) ; lats(JJ)] ;

   endfor

   [IT IP] = common(s.T.id, s.P.id) ;
   s.T.id = s.T.id(IT,:) ; s.T.x = s.T.x(IT,:) ;
   s.P.id = s.P.id(IP,:) ; s.P.x = s.P.x(IP,:) ;

   ## I = ssn(s.P.id, "cold") ;
   ## I = ssn(s.P.id, "warm") ;
   I = ssn(s.P.id, "all") ;

   X.id = X.x = [] ;
   for j = 1:columns(s.P.ll)
      s.P.T(j) = findll(s.P.ll(:,j), s.T.ll) ;
      X.id = [X.id ; s.P.id(I,:)] ;
      X.x = [X.x ; [s.T.x(I,s.P.T(j)) s.P.x(I,j)]] ;
   endfor

   if 0
      scatter(X.x(:,1), X.x(:,2), 5, "black", "filled") ;
      xlabel("T [C]") ; ylabel(["P [mm]"])
      orient landscape
      set(gca, "xgrid", "on") ;
      set(gcf, "paperposition", [0.05 0.05 0.95 0.95])
      print(fullfile(cloud, ["T-P_" sfx "_scatter.png"])) ;
   endif

   ## pre-select data
   I = all(!isnan(X.x), 2) ;
   ## I = X.x(:,1) > 0 & X.x(:,2) > 0 ;	# both T and P > 0
   x = X.x(I,:) ;

   ## quantiles
   Tq = linspace(min(x(:,1)), max(x(:,1)), 20)' ;

   ## fit kernel
   n = rows(x) ;
   Pf = kernel_regression(Tq, x(:,2), x(:,1), n^(-1/5)) ;

endfunction
