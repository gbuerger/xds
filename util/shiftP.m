## usage: shiftP (ifile)
##
## shift P one day back
function shiftP (ifile)

   odir = fileparts(ifile) ;
   
   s = read_stat(ifile) ;
   s.vars = cellfun(@(c) c = "Ps", s.vars, "UniformOutput", false) ;

   s.id = datevec(datenum(s.id) - 1) ;

   n = length(s.ids) ;
   
   ofile = sprintf("%s/%s.csv", odir, s.vars{1}) ;
   printf("out: --> %s\n", ofile) ;
   fid = fopen(ofile, "wt") ;
   fmt = ["Y,M,D" repmat(",%s", 1, n) "\n"] ;
   fprintf(fid, fmt, s.ids{:}) ;
   fmt = ["%d,%d,%d" repmat([",%.1f"], 1, n) "\n"] ;
   fprintf(fid, fmt, [s.id(:,1:3) s.x]') ;
   fclose(fid) ;

   printf("out: --> %s\n", ofile) ;

endfunction
