## usage: val = rkeyval (s, key, cnd)
##
## return value of key for structure array s
function val = rkeyval (s, key, cnd)

   if nargin < 3
      cnd = "true" ;
   endif
   
   val = {} ;

   if ~isstruct(s)
      return ;
   endif

   for j = 1:length(s)
      for [v k] = s(j)
	 if eval(cnd) && (kv = keyval(v, key))
	    val = union(val, kv) ;
	 else
	    val = union(val, rkeyval(v, key, cnd)) ;
	 endif
	 
      endfor
   endfor

endfunction
