## usage: res = ndfind (I)
##
## multidimensional form of find
function res = ndfind (I)

   N = size(I) ;
   res = cell(1, length(N)) ;
   
   w = find(I)(:) ;

   [res{:}] = ind2sub(N, w) ;

##   res = num2cell(cell2mat(res)) ;

endfunction
