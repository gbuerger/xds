## usage: CFS = read_CFSv2 (pfx, ltx)
##
##
#http://nomads.ncdc.noaa.gov/thredds/dodsC/modeldata/cmd_ts_9mon/2003/200307/wnd1000.2003072012.time.grb2
http://nomads.ncdc.noaa.gov/thredds/dodsC/modeldata/cmd_ts_9mon/2003/200307/q850.2003071500.time.grb2
v = "wnd1000"
function CFS = read_CFSv2 (v, ltx)

   addpath ~/oct
   pfx = "http://nomads.ncdc.noaa.gov/thredds/"

   cnd = "(kv = keyval(v, \"name\")) && strcmp(kv, \"q850\")"
   sfx = rkeyval(s, "urlPath", cnd) ;
   s = parseXML([pfx "catalog/" sfx{1}]) ;
   ncf = [pfx "dodsC/" sfx]
   ncdisp(ncf)
   ## detailed version
      url = "http://nomads.ncdc.noaa.gov/thredds/dodsC/modeldata/cmd_ts_9mon/" ;

      vs = "Precipitation_rate" ; q = 24*60*60 ;
      y0 = 1982 ; y1 = 2010 ;
      y0 = 2000 ; y1 = 2010 ;
      LON = [80.5 86.5] ; LAT = [19 23.5] ;

      T0 = datenum(y0, 1, 1) ;
      T1 = datenum(y1, 12, 31) ;

      [y m d h] = datevec(T0) ;
      f = sprintf("%s%4d/%4d%02d/%4d%02d%02d/flxf%4d%02d%02d%02d.01.%4d%02d%02d%02d.grb2", url, y, y, m, y, m, d, y, m, d, h, y, m, d, h) ;
      lon = ncread(f, "lon") ;
      lat = ncread(f, "lat") ;
      Ilon = find(LON(1) <= lon & lon <= LON(2)) ;
      Ilat = find(LAT(1) <= lat & lat <= LAT(2)) ;

      CFS.vt = CFS.lt = CFS.x = [] ;
      for t=T0:1/4:T1

	 [y m d h] = datevec(t) ;
	 pmkdir(odir = sprintf("%s%4d%02d", pfx, y, m)) ;
	 fo = sprintf("%s/prate.%4d%02d%02d%02d.ob", odir, y, m, d, h) ;

	 if exist(fo, "file")

	    printf("<-- %s\n", fo) ;
	    load(fo) ;
	    
	 else
	    
	    f = sprintf("%s%4d/%4d%02d/%4d%02d%02d/flxf%4d%02d%02d%02d.01.%4d%02d%02d%02d.grb2", url, y, y, m, y, m, d, y, m, d, h, y, m, d, h) ;
	    [~, suc] = urlread([f ".das"]) ;
	    if ~suc, continue ; endif

	    VT = [] ;
	    while size(VT, 2) ~= ltx+1

	       X = VT = LT = [] ;
	       for lt=0:ltx

		  vt = t + lt ;
		  [y1 m1 d1 h1] = datevec(vt) ;
		  f = sprintf("%s%4d/%4d%02d/%4d%02d%02d/flxf%4d%02d%02d%02d.01.%4d%02d%02d%02d.grb2", url, y, y, m, y, m, d, y1, m1, d1, h1, y, m, d, h) ;
		  
		  ##[~, suc] = urlread([f ".das"]) ;
		  ##if ~suc, continue ; endif

		  i = 0 ;
		  while ++i < 10
		     try
			x = double(q * ncread(f, vs, [Ilon(1) Ilat(1) 1], [length(Ilon) length(Ilat) 1])) ;
			break ;
		     catch
			sleep(10) ;
		     end_try_catch
		  endwhile
		  if i == 10
		     error("read_CFSv2: no data for %4d%02d%02d%02d + %d", y, m, d, h, lt) ;
		     x = nan(length(Ilon), length(Ilat)) ;
		  endif

		  VT = cat(2, VT, vt) ;
		  LT = cat(2, LT, lt) ;
		  X = cat(3, X, x) ;

	       endfor

	    endwhile

	    save(fo, "VT", "LT", "X") ;
	    printf("--> %s\n", fo) ;
	    
	 endif

	 if isempty(X), continue ; endif
	 
	 CFS.vt = cat(1, CFS.vt, VT) ;
	 CFS.lt = cat(1, CFS.lt, LT) ;
	 CFS.x = cat(1, CFS.x, adddim(X)) ;

      endfor

endfunction
