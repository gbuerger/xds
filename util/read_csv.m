## usage: s = read_csv (file)
##
## read csv file into structure
function s = read_csv (file)

      fid = fopen(file, "rt") ;
      n = length(str = strsplit(fgetl(fid), ",")) ;
      fclose(fid) ;

      fmt = repmat(" %s", 1, n) ;
      C = cell(1,n) ;
      [C{:}] = textread(file, fmt, 1, "delimiter", ",") ;

      if strcmp(str{1}, "Yr")
	 
	 s.lbl = cellfun(@(c) strread(c{:}, "%s"), C(7:end)) ;

	 fmt = ["%d %d %d %d %d %d" repmat(" %f", 1, n-6)] ;
	 id_ref = id = cell(1,3) ; x = cell(1,n-6) ;
	 [id_ref{:} id{:} x{:}] = textread(file, fmt, "delimiter", ",") ;

	 s.id_ref = double(cell2mat(id_ref)(2:end,:)) ;

      else

	 s.lbl = cellfun(@(c) strread(c{:}, "%s"), C(4:end)) ;

	 fmt = ["%d %d %d" repmat(" %f", 1, n-3)] ;
	 id = cell(1,3) ; x = cell(1,n-3) ;
	 [id{:} x{:}] = textread(file, fmt, "delimiter", ",") ;

      endif
      
      s.id = double(cell2mat(id)(2:end,:)) ;
      s.x = cell2mat(x)(2:end,:) ;

endfunction
