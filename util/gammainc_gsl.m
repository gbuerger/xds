## usage: y = gammainc_gsl (x, a, tail="upper")
##
## wrapper for gsl
function y = gammainc_gsl (x, a, tail="upper")

   pkg load gsl

   if strncmp(tail, "up", 2)

      y = gsl_sf_gamma_inc_Q(a, x) ;

   else

      y = gsl_sf_gamma_inc_P(a, x) ;

   endif
   
endfunction
