function id = datevec2 (J, kY)

   ## usage: id = datevec2 (J, kY)
   ## 
   ## transform date yyyymmddHHMMSS to [y m d H M S]

   global NCPU

   J = J(:) ;

   if iscell(J)
      id = parfun(NCPU, @(j) datevec2(j), J, "UniformOutput", false) ;
      id = cell2mat(id) ;
      return
   elseif !isscalar(J)
      id = parfun(@(j) datevec2(j), J, "UniformOutput", false) ;
      id = cell2mat(id) ;
      return
   endif

   if nargin < 2, kY = 4 ; endif

   n = length(num2str(J)) ;
   id(1) = floor(J / 10^(n-kY)) ; J = J - id(1) * 10^(n-kY) ;
   n = length(num2str(J)) ;

   j = 1 ;
   while n > 0
      j++ ;
      id(j) = floor(J / 10^(n-2)) ; J = J - id(j) * 10^(n-2) ;
      n -= 2 ;
   endwhile

endfunction
