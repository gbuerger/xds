## usage: y = adddim (x, d)
##
##
function y = adddim (x, d)
   if nargin < 2
      d = 1 ;
   endif

   n = ndims(x) ;
   p = 1:n ;
   p = [p(1:d-1) n+1 p(d:n)] ;
   y = permute(x, p) ;

endfunction
