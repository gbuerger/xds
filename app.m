function app (L, Lout, Lplt, Lsdly)

   ## usage: app (L, Lout, Lplt, Lsdly)
   ##
   ## process predictors (analyses and GCM simulations)

   if nargin == 0, L = false ; endif

   global PAR PRJ GVAR ADIR SDIR CDIR isANA isCUR keof

   dclim = fullfile(CDIR, "clim") ;

   cfile = fullfile(CDIR, [strrep(PAR.ana, "/", "_") ".pc.ob"]) ;
   pfile = fullfile(CDIR, PAR.pdd, "adj.ob") ;

   if L || Lout || !isnewer(cfile, PAR.mfile, pfile)
      wsim = PAR.sim ;
      init(["SIM = \"" PAR.sim{2} "\";"]) ;
      ptr(L) ;
      out(Lout) ;
      PAR.sim = wsim ; clear wsim ;
      isCUR = false ;
      init(["SIM = \"" PAR.sim{1} " " PAR.sim{2} "\";"]) ;
   endif

   ## loop over app input
   [plist dlist] = read_plist(gdir = fullfile("data", PAR.sim{1})) ;
   if Lhttp = strncmp(plist{1}{1}, "http", 4)
      SDIR0 = SDIR ;   # UGLY
   endif

   prbwgt = PAR.prbwgt ; PAR.prbwgt = [1 0] ; ## UGLY
   for jp = 1:length(dlist)

      d = dlist{jp} ;
      
      if ~isempty(f = dbstack)
	 fname = f(1).name ;
	 printf("\n\t###   %s> %s   ###\n\n", fname, d) ;
      else
	 fname = "app" ;
      endif

      if Lhttp
	 pmkdir(SDIR = fullfile(SDIR0, d)) ;
      else
	 pmkdir(SDIR = strrep(d, PRJ, fullfile(PRJ, PAR.ptr, PRJ))) ;
      endif

      if ~isnewer(file = sprintf("%s/ptr.ob", SDIR), d)
	 ptr = [] ;
	 if Lhttp
	    ncl = plist{jp} ;
	 else
	    ncl = glob([d "/*.nc"])' ;
	 endif
	 for ncf = ncl
	    ncf = ncf{:} ;
	    pmkdir(fileparts(oncf = nc2ob(ncf, gdir))) ;
	    if isnewer(oncf, ncf)
	       printf("app: <-- %s\n", oncf) ;
	       load(oncf) ;
	    else
	       printf("app: <-- %s\n", ncf) ;
	       w = read_GCM(ncf) ;
	       if isempty(w)
		  warning("xds:xds", "%s> no input: %s", f(1).name, ncf)
		  continue ;
	       endif
	       w = add_tp(w) ;
	       printf("app: --> %s\n", oncf) ;
	       save(oncf, "w") ;
	    endif
	    ptr = join_ptr(ptr, w) ;
	 endfor
	 printf("app: --> %s\n", file) ;
	 save(file, "ptr") ;
      endif

      ## regrid and normalize
      zfile = fullfile(SDIR, "ptrz.ob") ;
      if ~isnewer(zfile, file, dclim) || PAR.ptrout
	 if PAR.ptrout
	    odir = sprintf("%s/%s.ptrout", SDIR, PAR.pdd) ;
	    if ~Lplt && exist(zfile, "file") == 2 && exist(odir, "dir") == 7 && ~isnewer(file, odir) continue ; endif
	 endif
	 if exist("ptr", "var") ~= 1 || PAR.ptrout
	    printf("app: <-- %s\n", file) ; 
	    clear ptr ;
	    load(file) ;
	 endif
	 ptr = SelFld(ptr, GVAR(2,:)) ;
	 if isempty(ptr)
	    printf("app: missing fields, deleting %s\n", file) ; 
	    unlink(file) ;
	    continue ;
	 endif
	 
	 for iptr = 1:columns(GVAR)
	    k = GVAR{2,iptr} ; v = ptr.(k) ;
	    v.isP = !isempty(strfind(tolower(v.name), "precip")) ;
	    v = to_grid(v, k, iptr) ;

	    if PAR.ptrout
##	       lon = v.lon ; lat = v.lat ;
##	       save(pfile = fullfile(SDIR, [k ".geo"]), "-text", "lat", "lon") ;
##	       csvwrite(pfile = fullfile(SDIR, [k ".csv"]), [v.id(:,1:3) v.x]) ;
##	       printf("app: --> %s\n", pfile) ; 
	       ptrout(v, odir) ;
	    else
	       ## transform to N(0,1)
	       cfile = fullfile(dclim, [k ".ob"]) ;
	       v = do_norm(v, cfile) ;
	       ptr.(k) = v ;
	    endif
	 endfor

	 if ~PAR.ptrout
	    printf("app: --> %s\n", zfile) ;
	    save(zfile, "ptr") ;
	 endif
	 
      endif

      ## EOF reduction \\
      ofile = fullfile(SDIR, [strrep(PAR.ana, "/", "_") ".pc.ob"]) ;
      if (~isnewer(ofile, zfile, dclim, PAR.mfile) || PAR.bld || L) && ~PAR.ptrout

	 if exist("ptr", "var") ~= 1 || ~isfield("ptr.(GVAR{2,iptr})", "z")
	    printf("app: <-- %s\n", zfile) ; 
	    clear ptr ;
	    load(zfile) ;
	 endif

	 ptr = common_def(ptr) ;

	 PC.id = PC.x = PC.J = [] ; wptr = {} ; keof = 0 ;
	 for iptr = 1:columns(GVAR)

	    key = GVAR{2,iptr} ;
	    if !isANA
	       printf("app: pairing %s with %s\n", GVAR{[2 1],iptr}) ;
	    endif
	    v = ptr.(key) ; wptr = {wptr{:}, key} ;
	    [pc v] = do_eof(v, GVAR{:,iptr}) ;
	    v = rmfield(v, "z") ;
	    ptr = rmfield(ptr, key) ;

	    PC = upd_PC(PC, v, pc, iptr) ;

	 endfor
	 clear ptr ;
	 PC.ptr = wptr ; [PC.nr PC.nc] = size(PC.x) ;

	 if isANA
	    PCw = selper(PC, PAR.cper(1,:), PAR.cper(2,:)) ;
	    G = covadj(PCw.x) ;
	    printf("app: --> %s\n", Gfile = fullfile(ADIR, "G.ob")) ; 
	    save(Gfile, "G") ;
	 elseif isCUR
	    PCw = selper(PC, PAR.sper(1,:), PAR.sper(2,:)) ;
	    G = covadj(PCw.x) ;
	    printf("app: --> %s\n", Gfile = fullfile(CDIR, "G.ob")) ; 
	    save(Gfile, "G") ;
	 endif

	 if strfind(PAR.ptradj, "mlt")
	    PC = mltadj(PC) ;
	 endif

	 save(ofile, "PC") ;
	 printf("app: --> %s\n", ofile) ;
      endif
      ## EOF reduction  //
      
      ## write output
      if ~PAR.ptrout out(Lout) ; endif
      if ~PAR.ptrout & PAR.aout && strcmp(PAR.output, "csv") aout() ; endif

      ## plots
      plt(Lplt)  ;
      
      ## sub daily
      sdly(Lsdly)  ;

   endfor
   PAR.prbwgt = prbwgt ; ## UGLY
   close all ;
      
endfunction
